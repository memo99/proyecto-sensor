from tutorial_interfaces.srv import XYZ     # CHANGE
import rclpy
from rclpy.node import Node
import numpy as np
from scipy import interpolate


class MinimalService(Node):

    def __init__(self):
        super().__init__('minimal_service')
        self.srv = self.create_service(XYZ, 'xyz', self.xyz_callback)        # CHANGE
        

    def xyz_callback(self, request, response):
        #response.sum = request.a + request.b + request.c                                                  # CHANGE
        Xi = np.arange(0, 5, 1)
        Yi = np.arange(0, 5, 1)
        X, Y = np.meshgrid(Xi, Yi)
        #Z = np.sqrt(X)


        a= [[0, 1, 2, 3, 4],
            [1, 2, 3, 4, 4],
            [2, 3, 4, 4, 4],
            [3, 4, 4, 4, 4],
            [4, 4, 4, 4, 4]]

        Z=np.asarray(a)

        resultado1 = interpolate.interp2d(X, Y, Z, kind = "cubic")

        response.z=float(resultado1(request.x,request.y))

        print(response.z)

        #response.sum=int(resultado1(request.a,request.b))

        
        self.get_logger().info('Incoming request\nx: %.2f y: %.2f' % (request.x, request.y)) # CHANGE

        return response

def main(args=None):
    rclpy.init(args=args)

    minimal_service = MinimalService()

    rclpy.spin(minimal_service)

    rclpy.shutdown()

if __name__ == '__main__':
    main()


