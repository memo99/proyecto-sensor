// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from wessling_hand_msgs:msg/SystemStatus.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "wessling_hand_msgs/msg/detail/system_status__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace wessling_hand_msgs
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

void SystemStatus_init_function(
  void * message_memory, rosidl_runtime_cpp::MessageInitialization _init)
{
  new (message_memory) wessling_hand_msgs::msg::SystemStatus(_init);
}

void SystemStatus_fini_function(void * message_memory)
{
  auto typed_message = static_cast<wessling_hand_msgs::msg::SystemStatus *>(message_memory);
  typed_message->~SystemStatus();
}

size_t size_function__SystemStatus__hand(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<wessling_hand_msgs::msg::HandStatus> *>(untyped_member);
  return member->size();
}

const void * get_const_function__SystemStatus__hand(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<wessling_hand_msgs::msg::HandStatus> *>(untyped_member);
  return &member[index];
}

void * get_function__SystemStatus__hand(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<wessling_hand_msgs::msg::HandStatus> *>(untyped_member);
  return &member[index];
}

void resize_function__SystemStatus__hand(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<wessling_hand_msgs::msg::HandStatus> *>(untyped_member);
  member->resize(size);
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember SystemStatus_message_member_array[4] = {
  {
    "hand",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<wessling_hand_msgs::msg::HandStatus>(),  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::SystemStatus, hand),  // bytes offset in struct
    nullptr,  // default value
    size_function__SystemStatus__hand,  // size() function pointer
    get_const_function__SystemStatus__hand,  // get_const(index) function pointer
    get_function__SystemStatus__hand,  // get(index) function pointer
    resize_function__SystemStatus__hand  // resize(index) function pointer
  },
  {
    "brake_status",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::SystemStatus, brake_status),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr  // resize(index) function pointer
  },
  {
    "left_hand",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::SystemStatus, left_hand),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr  // resize(index) function pointer
  },
  {
    "right_hand",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::SystemStatus, right_hand),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers SystemStatus_message_members = {
  "wessling_hand_msgs::msg",  // message namespace
  "SystemStatus",  // message name
  4,  // number of fields
  sizeof(wessling_hand_msgs::msg::SystemStatus),
  SystemStatus_message_member_array,  // message members
  SystemStatus_init_function,  // function to initialize message memory (memory has to be allocated)
  SystemStatus_fini_function  // function to terminate message instance (will not free memory)
};

static const rosidl_message_type_support_t SystemStatus_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &SystemStatus_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace wessling_hand_msgs


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<wessling_hand_msgs::msg::SystemStatus>()
{
  return &::wessling_hand_msgs::msg::rosidl_typesupport_introspection_cpp::SystemStatus_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, wessling_hand_msgs, msg, SystemStatus)() {
  return &::wessling_hand_msgs::msg::rosidl_typesupport_introspection_cpp::SystemStatus_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
