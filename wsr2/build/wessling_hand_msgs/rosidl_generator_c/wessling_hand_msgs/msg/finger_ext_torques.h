// generated from rosidl_generator_c/resource/idl.h.em
// with input from wessling_hand_msgs:msg/FingerExtTorques.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__FINGER_EXT_TORQUES_H_
#define WESSLING_HAND_MSGS__MSG__FINGER_EXT_TORQUES_H_

#include "wessling_hand_msgs/msg/detail/finger_ext_torques__struct.h"
#include "wessling_hand_msgs/msg/detail/finger_ext_torques__functions.h"
#include "wessling_hand_msgs/msg/detail/finger_ext_torques__type_support.h"

#endif  // WESSLING_HAND_MSGS__MSG__FINGER_EXT_TORQUES_H_
