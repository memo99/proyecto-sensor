// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from wessling_hand_msgs:msg/SystemStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_STATUS__STRUCT_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_STATUS__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'hand'
#include "wessling_hand_msgs/msg/detail/hand_status__struct.h"

// Struct defined in msg/SystemStatus in the package wessling_hand_msgs.
typedef struct wessling_hand_msgs__msg__SystemStatus
{
  wessling_hand_msgs__msg__HandStatus__Sequence hand;
  bool brake_status;
  bool left_hand;
  bool right_hand;
} wessling_hand_msgs__msg__SystemStatus;

// Struct for a sequence of wessling_hand_msgs__msg__SystemStatus.
typedef struct wessling_hand_msgs__msg__SystemStatus__Sequence
{
  wessling_hand_msgs__msg__SystemStatus * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} wessling_hand_msgs__msg__SystemStatus__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_STATUS__STRUCT_H_
