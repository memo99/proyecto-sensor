// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from wessling_hand_msgs:msg/SystemExtTorques.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__STRUCT_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'hand'
#include "wessling_hand_msgs/msg/detail/hand_ext_torques__struct.h"

// Struct defined in msg/SystemExtTorques in the package wessling_hand_msgs.
typedef struct wessling_hand_msgs__msg__SystemExtTorques
{
  wessling_hand_msgs__msg__HandExtTorques__Sequence hand;
  bool left_hand;
  bool right_hand;
} wessling_hand_msgs__msg__SystemExtTorques;

// Struct for a sequence of wessling_hand_msgs__msg__SystemExtTorques.
typedef struct wessling_hand_msgs__msg__SystemExtTorques__Sequence
{
  wessling_hand_msgs__msg__SystemExtTorques * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} wessling_hand_msgs__msg__SystemExtTorques__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__STRUCT_H_
