// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from wessling_hand_msgs:msg/FingerExtTorques.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/msg/detail/finger_ext_torques__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `torque`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
wessling_hand_msgs__msg__FingerExtTorques__init(wessling_hand_msgs__msg__FingerExtTorques * msg)
{
  if (!msg) {
    return false;
  }
  // torque
  if (!rosidl_runtime_c__float__Sequence__init(&msg->torque, 0)) {
    wessling_hand_msgs__msg__FingerExtTorques__fini(msg);
    return false;
  }
  return true;
}

void
wessling_hand_msgs__msg__FingerExtTorques__fini(wessling_hand_msgs__msg__FingerExtTorques * msg)
{
  if (!msg) {
    return;
  }
  // torque
  rosidl_runtime_c__float__Sequence__fini(&msg->torque);
}

wessling_hand_msgs__msg__FingerExtTorques *
wessling_hand_msgs__msg__FingerExtTorques__create()
{
  wessling_hand_msgs__msg__FingerExtTorques * msg = (wessling_hand_msgs__msg__FingerExtTorques *)malloc(sizeof(wessling_hand_msgs__msg__FingerExtTorques));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(wessling_hand_msgs__msg__FingerExtTorques));
  bool success = wessling_hand_msgs__msg__FingerExtTorques__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
wessling_hand_msgs__msg__FingerExtTorques__destroy(wessling_hand_msgs__msg__FingerExtTorques * msg)
{
  if (msg) {
    wessling_hand_msgs__msg__FingerExtTorques__fini(msg);
  }
  free(msg);
}


bool
wessling_hand_msgs__msg__FingerExtTorques__Sequence__init(wessling_hand_msgs__msg__FingerExtTorques__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  wessling_hand_msgs__msg__FingerExtTorques * data = NULL;
  if (size) {
    data = (wessling_hand_msgs__msg__FingerExtTorques *)calloc(size, sizeof(wessling_hand_msgs__msg__FingerExtTorques));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = wessling_hand_msgs__msg__FingerExtTorques__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        wessling_hand_msgs__msg__FingerExtTorques__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
wessling_hand_msgs__msg__FingerExtTorques__Sequence__fini(wessling_hand_msgs__msg__FingerExtTorques__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      wessling_hand_msgs__msg__FingerExtTorques__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

wessling_hand_msgs__msg__FingerExtTorques__Sequence *
wessling_hand_msgs__msg__FingerExtTorques__Sequence__create(size_t size)
{
  wessling_hand_msgs__msg__FingerExtTorques__Sequence * array = (wessling_hand_msgs__msg__FingerExtTorques__Sequence *)malloc(sizeof(wessling_hand_msgs__msg__FingerExtTorques__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = wessling_hand_msgs__msg__FingerExtTorques__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
wessling_hand_msgs__msg__FingerExtTorques__Sequence__destroy(wessling_hand_msgs__msg__FingerExtTorques__Sequence * array)
{
  if (array) {
    wessling_hand_msgs__msg__FingerExtTorques__Sequence__fini(array);
  }
  free(array);
}
