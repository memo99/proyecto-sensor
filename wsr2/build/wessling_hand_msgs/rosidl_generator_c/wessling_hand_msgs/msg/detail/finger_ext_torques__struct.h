// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from wessling_hand_msgs:msg/FingerExtTorques.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__STRUCT_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'torque'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in msg/FingerExtTorques in the package wessling_hand_msgs.
typedef struct wessling_hand_msgs__msg__FingerExtTorques
{
  rosidl_runtime_c__float__Sequence torque;
} wessling_hand_msgs__msg__FingerExtTorques;

// Struct for a sequence of wessling_hand_msgs__msg__FingerExtTorques.
typedef struct wessling_hand_msgs__msg__FingerExtTorques__Sequence
{
  wessling_hand_msgs__msg__FingerExtTorques * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} wessling_hand_msgs__msg__FingerExtTorques__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__STRUCT_H_
