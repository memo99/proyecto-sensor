// generated from rosidl_generator_c/resource/idl__functions.h.em
// with input from wessling_hand_msgs:msg/HandStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__FUNCTIONS_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_runtime_c/visibility_control.h"
#include "wessling_hand_msgs/msg/rosidl_generator_c__visibility_control.h"

#include "wessling_hand_msgs/msg/detail/hand_status__struct.h"

/// Initialize msg/HandStatus message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * wessling_hand_msgs__msg__HandStatus
 * )) before or use
 * wessling_hand_msgs__msg__HandStatus__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
bool
wessling_hand_msgs__msg__HandStatus__init(wessling_hand_msgs__msg__HandStatus * msg);

/// Finalize msg/HandStatus message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__HandStatus__fini(wessling_hand_msgs__msg__HandStatus * msg);

/// Create msg/HandStatus message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * wessling_hand_msgs__msg__HandStatus__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
wessling_hand_msgs__msg__HandStatus *
wessling_hand_msgs__msg__HandStatus__create();

/// Destroy msg/HandStatus message.
/**
 * It calls
 * wessling_hand_msgs__msg__HandStatus__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__HandStatus__destroy(wessling_hand_msgs__msg__HandStatus * msg);


/// Initialize array of msg/HandStatus messages.
/**
 * It allocates the memory for the number of elements and calls
 * wessling_hand_msgs__msg__HandStatus__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
bool
wessling_hand_msgs__msg__HandStatus__Sequence__init(wessling_hand_msgs__msg__HandStatus__Sequence * array, size_t size);

/// Finalize array of msg/HandStatus messages.
/**
 * It calls
 * wessling_hand_msgs__msg__HandStatus__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__HandStatus__Sequence__fini(wessling_hand_msgs__msg__HandStatus__Sequence * array);

/// Create array of msg/HandStatus messages.
/**
 * It allocates the memory for the array and calls
 * wessling_hand_msgs__msg__HandStatus__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
wessling_hand_msgs__msg__HandStatus__Sequence *
wessling_hand_msgs__msg__HandStatus__Sequence__create(size_t size);

/// Destroy array of msg/HandStatus messages.
/**
 * It calls
 * wessling_hand_msgs__msg__HandStatus__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__HandStatus__Sequence__destroy(wessling_hand_msgs__msg__HandStatus__Sequence * array);

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__FUNCTIONS_H_
