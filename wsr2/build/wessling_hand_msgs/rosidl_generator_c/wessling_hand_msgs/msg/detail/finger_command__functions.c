// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from wessling_hand_msgs:msg/FingerCommand.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/msg/detail/finger_command__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `angle`
// Member `kp`
// Member `velocity`
// Member `stiffness`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
wessling_hand_msgs__msg__FingerCommand__init(wessling_hand_msgs__msg__FingerCommand * msg)
{
  if (!msg) {
    return false;
  }
  // enable
  // angle
  if (!rosidl_runtime_c__float__Sequence__init(&msg->angle, 0)) {
    wessling_hand_msgs__msg__FingerCommand__fini(msg);
    return false;
  }
  // kp
  if (!rosidl_runtime_c__float__Sequence__init(&msg->kp, 0)) {
    wessling_hand_msgs__msg__FingerCommand__fini(msg);
    return false;
  }
  // velocity
  if (!rosidl_runtime_c__float__Sequence__init(&msg->velocity, 0)) {
    wessling_hand_msgs__msg__FingerCommand__fini(msg);
    return false;
  }
  // stiffness
  if (!rosidl_runtime_c__float__Sequence__init(&msg->stiffness, 0)) {
    wessling_hand_msgs__msg__FingerCommand__fini(msg);
    return false;
  }
  return true;
}

void
wessling_hand_msgs__msg__FingerCommand__fini(wessling_hand_msgs__msg__FingerCommand * msg)
{
  if (!msg) {
    return;
  }
  // enable
  // angle
  rosidl_runtime_c__float__Sequence__fini(&msg->angle);
  // kp
  rosidl_runtime_c__float__Sequence__fini(&msg->kp);
  // velocity
  rosidl_runtime_c__float__Sequence__fini(&msg->velocity);
  // stiffness
  rosidl_runtime_c__float__Sequence__fini(&msg->stiffness);
}

wessling_hand_msgs__msg__FingerCommand *
wessling_hand_msgs__msg__FingerCommand__create()
{
  wessling_hand_msgs__msg__FingerCommand * msg = (wessling_hand_msgs__msg__FingerCommand *)malloc(sizeof(wessling_hand_msgs__msg__FingerCommand));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(wessling_hand_msgs__msg__FingerCommand));
  bool success = wessling_hand_msgs__msg__FingerCommand__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
wessling_hand_msgs__msg__FingerCommand__destroy(wessling_hand_msgs__msg__FingerCommand * msg)
{
  if (msg) {
    wessling_hand_msgs__msg__FingerCommand__fini(msg);
  }
  free(msg);
}


bool
wessling_hand_msgs__msg__FingerCommand__Sequence__init(wessling_hand_msgs__msg__FingerCommand__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  wessling_hand_msgs__msg__FingerCommand * data = NULL;
  if (size) {
    data = (wessling_hand_msgs__msg__FingerCommand *)calloc(size, sizeof(wessling_hand_msgs__msg__FingerCommand));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = wessling_hand_msgs__msg__FingerCommand__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        wessling_hand_msgs__msg__FingerCommand__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
wessling_hand_msgs__msg__FingerCommand__Sequence__fini(wessling_hand_msgs__msg__FingerCommand__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      wessling_hand_msgs__msg__FingerCommand__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

wessling_hand_msgs__msg__FingerCommand__Sequence *
wessling_hand_msgs__msg__FingerCommand__Sequence__create(size_t size)
{
  wessling_hand_msgs__msg__FingerCommand__Sequence * array = (wessling_hand_msgs__msg__FingerCommand__Sequence *)malloc(sizeof(wessling_hand_msgs__msg__FingerCommand__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = wessling_hand_msgs__msg__FingerCommand__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
wessling_hand_msgs__msg__FingerCommand__Sequence__destroy(wessling_hand_msgs__msg__FingerCommand__Sequence * array)
{
  if (array) {
    wessling_hand_msgs__msg__FingerCommand__Sequence__fini(array);
  }
  free(array);
}
