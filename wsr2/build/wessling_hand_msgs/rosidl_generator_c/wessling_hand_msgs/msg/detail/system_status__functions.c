// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from wessling_hand_msgs:msg/SystemStatus.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/msg/detail/system_status__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `hand`
#include "wessling_hand_msgs/msg/detail/hand_status__functions.h"

bool
wessling_hand_msgs__msg__SystemStatus__init(wessling_hand_msgs__msg__SystemStatus * msg)
{
  if (!msg) {
    return false;
  }
  // hand
  if (!wessling_hand_msgs__msg__HandStatus__Sequence__init(&msg->hand, 0)) {
    wessling_hand_msgs__msg__SystemStatus__fini(msg);
    return false;
  }
  // brake_status
  // left_hand
  // right_hand
  return true;
}

void
wessling_hand_msgs__msg__SystemStatus__fini(wessling_hand_msgs__msg__SystemStatus * msg)
{
  if (!msg) {
    return;
  }
  // hand
  wessling_hand_msgs__msg__HandStatus__Sequence__fini(&msg->hand);
  // brake_status
  // left_hand
  // right_hand
}

wessling_hand_msgs__msg__SystemStatus *
wessling_hand_msgs__msg__SystemStatus__create()
{
  wessling_hand_msgs__msg__SystemStatus * msg = (wessling_hand_msgs__msg__SystemStatus *)malloc(sizeof(wessling_hand_msgs__msg__SystemStatus));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(wessling_hand_msgs__msg__SystemStatus));
  bool success = wessling_hand_msgs__msg__SystemStatus__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
wessling_hand_msgs__msg__SystemStatus__destroy(wessling_hand_msgs__msg__SystemStatus * msg)
{
  if (msg) {
    wessling_hand_msgs__msg__SystemStatus__fini(msg);
  }
  free(msg);
}


bool
wessling_hand_msgs__msg__SystemStatus__Sequence__init(wessling_hand_msgs__msg__SystemStatus__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  wessling_hand_msgs__msg__SystemStatus * data = NULL;
  if (size) {
    data = (wessling_hand_msgs__msg__SystemStatus *)calloc(size, sizeof(wessling_hand_msgs__msg__SystemStatus));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = wessling_hand_msgs__msg__SystemStatus__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        wessling_hand_msgs__msg__SystemStatus__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
wessling_hand_msgs__msg__SystemStatus__Sequence__fini(wessling_hand_msgs__msg__SystemStatus__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      wessling_hand_msgs__msg__SystemStatus__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

wessling_hand_msgs__msg__SystemStatus__Sequence *
wessling_hand_msgs__msg__SystemStatus__Sequence__create(size_t size)
{
  wessling_hand_msgs__msg__SystemStatus__Sequence * array = (wessling_hand_msgs__msg__SystemStatus__Sequence *)malloc(sizeof(wessling_hand_msgs__msg__SystemStatus__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = wessling_hand_msgs__msg__SystemStatus__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
wessling_hand_msgs__msg__SystemStatus__Sequence__destroy(wessling_hand_msgs__msg__SystemStatus__Sequence * array)
{
  if (array) {
    wessling_hand_msgs__msg__SystemStatus__Sequence__fini(array);
  }
  free(array);
}
