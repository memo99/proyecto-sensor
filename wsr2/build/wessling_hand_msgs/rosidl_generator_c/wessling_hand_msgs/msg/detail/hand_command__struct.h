// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from wessling_hand_msgs:msg/HandCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__STRUCT_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'finger'
#include "wessling_hand_msgs/msg/detail/finger_command__struct.h"

// Struct defined in msg/HandCommand in the package wessling_hand_msgs.
typedef struct wessling_hand_msgs__msg__HandCommand
{
  wessling_hand_msgs__msg__FingerCommand__Sequence finger;
} wessling_hand_msgs__msg__HandCommand;

// Struct for a sequence of wessling_hand_msgs__msg__HandCommand.
typedef struct wessling_hand_msgs__msg__HandCommand__Sequence
{
  wessling_hand_msgs__msg__HandCommand * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} wessling_hand_msgs__msg__HandCommand__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__STRUCT_H_
