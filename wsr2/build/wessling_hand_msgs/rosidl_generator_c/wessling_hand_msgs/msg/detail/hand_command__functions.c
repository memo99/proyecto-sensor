// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from wessling_hand_msgs:msg/HandCommand.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/msg/detail/hand_command__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `finger`
#include "wessling_hand_msgs/msg/detail/finger_command__functions.h"

bool
wessling_hand_msgs__msg__HandCommand__init(wessling_hand_msgs__msg__HandCommand * msg)
{
  if (!msg) {
    return false;
  }
  // finger
  if (!wessling_hand_msgs__msg__FingerCommand__Sequence__init(&msg->finger, 0)) {
    wessling_hand_msgs__msg__HandCommand__fini(msg);
    return false;
  }
  return true;
}

void
wessling_hand_msgs__msg__HandCommand__fini(wessling_hand_msgs__msg__HandCommand * msg)
{
  if (!msg) {
    return;
  }
  // finger
  wessling_hand_msgs__msg__FingerCommand__Sequence__fini(&msg->finger);
}

wessling_hand_msgs__msg__HandCommand *
wessling_hand_msgs__msg__HandCommand__create()
{
  wessling_hand_msgs__msg__HandCommand * msg = (wessling_hand_msgs__msg__HandCommand *)malloc(sizeof(wessling_hand_msgs__msg__HandCommand));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(wessling_hand_msgs__msg__HandCommand));
  bool success = wessling_hand_msgs__msg__HandCommand__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
wessling_hand_msgs__msg__HandCommand__destroy(wessling_hand_msgs__msg__HandCommand * msg)
{
  if (msg) {
    wessling_hand_msgs__msg__HandCommand__fini(msg);
  }
  free(msg);
}


bool
wessling_hand_msgs__msg__HandCommand__Sequence__init(wessling_hand_msgs__msg__HandCommand__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  wessling_hand_msgs__msg__HandCommand * data = NULL;
  if (size) {
    data = (wessling_hand_msgs__msg__HandCommand *)calloc(size, sizeof(wessling_hand_msgs__msg__HandCommand));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = wessling_hand_msgs__msg__HandCommand__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        wessling_hand_msgs__msg__HandCommand__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
wessling_hand_msgs__msg__HandCommand__Sequence__fini(wessling_hand_msgs__msg__HandCommand__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      wessling_hand_msgs__msg__HandCommand__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

wessling_hand_msgs__msg__HandCommand__Sequence *
wessling_hand_msgs__msg__HandCommand__Sequence__create(size_t size)
{
  wessling_hand_msgs__msg__HandCommand__Sequence * array = (wessling_hand_msgs__msg__HandCommand__Sequence *)malloc(sizeof(wessling_hand_msgs__msg__HandCommand__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = wessling_hand_msgs__msg__HandCommand__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
wessling_hand_msgs__msg__HandCommand__Sequence__destroy(wessling_hand_msgs__msg__HandCommand__Sequence * array)
{
  if (array) {
    wessling_hand_msgs__msg__HandCommand__Sequence__fini(array);
  }
  free(array);
}
