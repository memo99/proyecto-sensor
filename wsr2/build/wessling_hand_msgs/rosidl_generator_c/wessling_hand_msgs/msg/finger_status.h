// generated from rosidl_generator_c/resource/idl.h.em
// with input from wessling_hand_msgs:msg/FingerStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__FINGER_STATUS_H_
#define WESSLING_HAND_MSGS__MSG__FINGER_STATUS_H_

#include "wessling_hand_msgs/msg/detail/finger_status__struct.h"
#include "wessling_hand_msgs/msg/detail/finger_status__functions.h"
#include "wessling_hand_msgs/msg/detail/finger_status__type_support.h"

#endif  // WESSLING_HAND_MSGS__MSG__FINGER_STATUS_H_
