// generated from rosidl_generator_c/resource/idl.h.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__SRV__ENABLE_HANDS_H_
#define WESSLING_HAND_MSGS__SRV__ENABLE_HANDS_H_

#include "wessling_hand_msgs/srv/detail/enable_hands__struct.h"
#include "wessling_hand_msgs/srv/detail/enable_hands__functions.h"
#include "wessling_hand_msgs/srv/detail/enable_hands__type_support.h"

#endif  // WESSLING_HAND_MSGS__SRV__ENABLE_HANDS_H_
