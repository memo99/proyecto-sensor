// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/srv/detail/enable_hands__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// Include directives for member types
// Member `enable`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
wessling_hand_msgs__srv__EnableHands_Request__init(wessling_hand_msgs__srv__EnableHands_Request * msg)
{
  if (!msg) {
    return false;
  }
  // enable
  if (!rosidl_runtime_c__float__Sequence__init(&msg->enable, 0)) {
    wessling_hand_msgs__srv__EnableHands_Request__fini(msg);
    return false;
  }
  // num_fingers
  return true;
}

void
wessling_hand_msgs__srv__EnableHands_Request__fini(wessling_hand_msgs__srv__EnableHands_Request * msg)
{
  if (!msg) {
    return;
  }
  // enable
  rosidl_runtime_c__float__Sequence__fini(&msg->enable);
  // num_fingers
}

wessling_hand_msgs__srv__EnableHands_Request *
wessling_hand_msgs__srv__EnableHands_Request__create()
{
  wessling_hand_msgs__srv__EnableHands_Request * msg = (wessling_hand_msgs__srv__EnableHands_Request *)malloc(sizeof(wessling_hand_msgs__srv__EnableHands_Request));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(wessling_hand_msgs__srv__EnableHands_Request));
  bool success = wessling_hand_msgs__srv__EnableHands_Request__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
wessling_hand_msgs__srv__EnableHands_Request__destroy(wessling_hand_msgs__srv__EnableHands_Request * msg)
{
  if (msg) {
    wessling_hand_msgs__srv__EnableHands_Request__fini(msg);
  }
  free(msg);
}


bool
wessling_hand_msgs__srv__EnableHands_Request__Sequence__init(wessling_hand_msgs__srv__EnableHands_Request__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  wessling_hand_msgs__srv__EnableHands_Request * data = NULL;
  if (size) {
    data = (wessling_hand_msgs__srv__EnableHands_Request *)calloc(size, sizeof(wessling_hand_msgs__srv__EnableHands_Request));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = wessling_hand_msgs__srv__EnableHands_Request__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        wessling_hand_msgs__srv__EnableHands_Request__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
wessling_hand_msgs__srv__EnableHands_Request__Sequence__fini(wessling_hand_msgs__srv__EnableHands_Request__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      wessling_hand_msgs__srv__EnableHands_Request__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

wessling_hand_msgs__srv__EnableHands_Request__Sequence *
wessling_hand_msgs__srv__EnableHands_Request__Sequence__create(size_t size)
{
  wessling_hand_msgs__srv__EnableHands_Request__Sequence * array = (wessling_hand_msgs__srv__EnableHands_Request__Sequence *)malloc(sizeof(wessling_hand_msgs__srv__EnableHands_Request__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = wessling_hand_msgs__srv__EnableHands_Request__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
wessling_hand_msgs__srv__EnableHands_Request__Sequence__destroy(wessling_hand_msgs__srv__EnableHands_Request__Sequence * array)
{
  if (array) {
    wessling_hand_msgs__srv__EnableHands_Request__Sequence__fini(array);
  }
  free(array);
}


// Include directives for member types
// Member `enabled`
// already included above
// #include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
wessling_hand_msgs__srv__EnableHands_Response__init(wessling_hand_msgs__srv__EnableHands_Response * msg)
{
  if (!msg) {
    return false;
  }
  // enabled
  if (!rosidl_runtime_c__float__Sequence__init(&msg->enabled, 0)) {
    wessling_hand_msgs__srv__EnableHands_Response__fini(msg);
    return false;
  }
  return true;
}

void
wessling_hand_msgs__srv__EnableHands_Response__fini(wessling_hand_msgs__srv__EnableHands_Response * msg)
{
  if (!msg) {
    return;
  }
  // enabled
  rosidl_runtime_c__float__Sequence__fini(&msg->enabled);
}

wessling_hand_msgs__srv__EnableHands_Response *
wessling_hand_msgs__srv__EnableHands_Response__create()
{
  wessling_hand_msgs__srv__EnableHands_Response * msg = (wessling_hand_msgs__srv__EnableHands_Response *)malloc(sizeof(wessling_hand_msgs__srv__EnableHands_Response));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(wessling_hand_msgs__srv__EnableHands_Response));
  bool success = wessling_hand_msgs__srv__EnableHands_Response__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
wessling_hand_msgs__srv__EnableHands_Response__destroy(wessling_hand_msgs__srv__EnableHands_Response * msg)
{
  if (msg) {
    wessling_hand_msgs__srv__EnableHands_Response__fini(msg);
  }
  free(msg);
}


bool
wessling_hand_msgs__srv__EnableHands_Response__Sequence__init(wessling_hand_msgs__srv__EnableHands_Response__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  wessling_hand_msgs__srv__EnableHands_Response * data = NULL;
  if (size) {
    data = (wessling_hand_msgs__srv__EnableHands_Response *)calloc(size, sizeof(wessling_hand_msgs__srv__EnableHands_Response));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = wessling_hand_msgs__srv__EnableHands_Response__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        wessling_hand_msgs__srv__EnableHands_Response__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
wessling_hand_msgs__srv__EnableHands_Response__Sequence__fini(wessling_hand_msgs__srv__EnableHands_Response__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      wessling_hand_msgs__srv__EnableHands_Response__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

wessling_hand_msgs__srv__EnableHands_Response__Sequence *
wessling_hand_msgs__srv__EnableHands_Response__Sequence__create(size_t size)
{
  wessling_hand_msgs__srv__EnableHands_Response__Sequence * array = (wessling_hand_msgs__srv__EnableHands_Response__Sequence *)malloc(sizeof(wessling_hand_msgs__srv__EnableHands_Response__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = wessling_hand_msgs__srv__EnableHands_Response__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
wessling_hand_msgs__srv__EnableHands_Response__Sequence__destroy(wessling_hand_msgs__srv__EnableHands_Response__Sequence * array)
{
  if (array) {
    wessling_hand_msgs__srv__EnableHands_Response__Sequence__fini(array);
  }
  free(array);
}
