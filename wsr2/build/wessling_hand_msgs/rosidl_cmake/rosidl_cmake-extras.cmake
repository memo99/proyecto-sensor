# generated from rosidl_cmake/cmake/rosidl_cmake-extras.cmake.in

set(wessling_hand_msgs_IDL_FILES "msg/FingerCommand.idl;msg/FingerExtTorques.idl;msg/FingerStatus.idl;msg/HandCommand.idl;msg/HandExtTorques.idl;msg/HandStatus.idl;msg/SystemCommand.idl;msg/SystemExtTorques.idl;msg/SystemStatus.idl;srv/EnableHands.idl")
set(wessling_hand_msgs_INTERFACE_FILES "msg/FingerCommand.msg;msg/FingerExtTorques.msg;msg/FingerStatus.msg;msg/HandCommand.msg;msg/HandExtTorques.msg;msg/HandStatus.msg;msg/SystemCommand.msg;msg/SystemExtTorques.msg;msg/SystemStatus.msg;srv/EnableHands.srv;srv/EnableHands_Request.msg;srv/EnableHands_Response.msg")
