// generated from rosidl_typesupport_fastrtps_c/resource/idl__type_support_c.cpp.em
// with input from wessling_hand_msgs:msg/SystemExtTorques.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/msg/detail/system_ext_torques__rosidl_typesupport_fastrtps_c.h"


#include <cassert>
#include <limits>
#include <string>
#include "rosidl_typesupport_fastrtps_c/identifier.h"
#include "rosidl_typesupport_fastrtps_c/wstring_conversion.hpp"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
#include "wessling_hand_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"
#include "wessling_hand_msgs/msg/detail/system_ext_torques__struct.h"
#include "wessling_hand_msgs/msg/detail/system_ext_torques__functions.h"
#include "fastcdr/Cdr.h"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif
#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

// includes and forward declarations of message dependencies and their conversion functions

#if defined(__cplusplus)
extern "C"
{
#endif

#include "wessling_hand_msgs/msg/detail/hand_ext_torques__functions.h"  // hand

// forward declare type support functions
size_t get_serialized_size_wessling_hand_msgs__msg__HandExtTorques(
  const void * untyped_ros_message,
  size_t current_alignment);

size_t max_serialized_size_wessling_hand_msgs__msg__HandExtTorques(
  bool & full_bounded,
  size_t current_alignment);

const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, wessling_hand_msgs, msg, HandExtTorques)();


using _SystemExtTorques__ros_msg_type = wessling_hand_msgs__msg__SystemExtTorques;

static bool _SystemExtTorques__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  const _SystemExtTorques__ros_msg_type * ros_message = static_cast<const _SystemExtTorques__ros_msg_type *>(untyped_ros_message);
  // Field name: hand
  {
    const message_type_support_callbacks_t * callbacks =
      static_cast<const message_type_support_callbacks_t *>(
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
        rosidl_typesupport_fastrtps_c, wessling_hand_msgs, msg, HandExtTorques
      )()->data);
    size_t size = ros_message->hand.size;
    auto array_ptr = ros_message->hand.data;
    cdr << static_cast<uint32_t>(size);
    for (size_t i = 0; i < size; ++i) {
      if (!callbacks->cdr_serialize(
          &array_ptr[i], cdr))
      {
        return false;
      }
    }
  }

  // Field name: left_hand
  {
    cdr << (ros_message->left_hand ? true : false);
  }

  // Field name: right_hand
  {
    cdr << (ros_message->right_hand ? true : false);
  }

  return true;
}

static bool _SystemExtTorques__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  _SystemExtTorques__ros_msg_type * ros_message = static_cast<_SystemExtTorques__ros_msg_type *>(untyped_ros_message);
  // Field name: hand
  {
    const message_type_support_callbacks_t * callbacks =
      static_cast<const message_type_support_callbacks_t *>(
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
        rosidl_typesupport_fastrtps_c, wessling_hand_msgs, msg, HandExtTorques
      )()->data);
    uint32_t cdrSize;
    cdr >> cdrSize;
    size_t size = static_cast<size_t>(cdrSize);
    if (ros_message->hand.data) {
      wessling_hand_msgs__msg__HandExtTorques__Sequence__fini(&ros_message->hand);
    }
    if (!wessling_hand_msgs__msg__HandExtTorques__Sequence__init(&ros_message->hand, size)) {
      return "failed to create array for field 'hand'";
    }
    auto array_ptr = ros_message->hand.data;
    for (size_t i = 0; i < size; ++i) {
      if (!callbacks->cdr_deserialize(
          cdr, &array_ptr[i]))
      {
        return false;
      }
    }
  }

  // Field name: left_hand
  {
    uint8_t tmp;
    cdr >> tmp;
    ros_message->left_hand = tmp ? true : false;
  }

  // Field name: right_hand
  {
    uint8_t tmp;
    cdr >> tmp;
    ros_message->right_hand = tmp ? true : false;
  }

  return true;
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
size_t get_serialized_size_wessling_hand_msgs__msg__SystemExtTorques(
  const void * untyped_ros_message,
  size_t current_alignment)
{
  const _SystemExtTorques__ros_msg_type * ros_message = static_cast<const _SystemExtTorques__ros_msg_type *>(untyped_ros_message);
  (void)ros_message;
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // field.name hand
  {
    size_t array_size = ros_message->hand.size;
    auto array_ptr = ros_message->hand.data;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    for (size_t index = 0; index < array_size; ++index) {
      current_alignment += get_serialized_size_wessling_hand_msgs__msg__HandExtTorques(
        &array_ptr[index], current_alignment);
    }
  }
  // field.name left_hand
  {
    size_t item_size = sizeof(ros_message->left_hand);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // field.name right_hand
  {
    size_t item_size = sizeof(ros_message->right_hand);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

static uint32_t _SystemExtTorques__get_serialized_size(const void * untyped_ros_message)
{
  return static_cast<uint32_t>(
    get_serialized_size_wessling_hand_msgs__msg__SystemExtTorques(
      untyped_ros_message, 0));
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
size_t max_serialized_size_wessling_hand_msgs__msg__SystemExtTorques(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;

  // member: hand
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);


    for (size_t index = 0; index < array_size; ++index) {
      current_alignment +=
        max_serialized_size_wessling_hand_msgs__msg__HandExtTorques(
        full_bounded, current_alignment);
    }
  }
  // member: left_hand
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint8_t);
  }
  // member: right_hand
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint8_t);
  }

  return current_alignment - initial_alignment;
}

static size_t _SystemExtTorques__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_wessling_hand_msgs__msg__SystemExtTorques(
    full_bounded, 0);
}


static message_type_support_callbacks_t __callbacks_SystemExtTorques = {
  "wessling_hand_msgs::msg",
  "SystemExtTorques",
  _SystemExtTorques__cdr_serialize,
  _SystemExtTorques__cdr_deserialize,
  _SystemExtTorques__get_serialized_size,
  _SystemExtTorques__max_serialized_size
};

static rosidl_message_type_support_t _SystemExtTorques__type_support = {
  rosidl_typesupport_fastrtps_c__identifier,
  &__callbacks_SystemExtTorques,
  get_message_typesupport_handle_function,
};

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, wessling_hand_msgs, msg, SystemExtTorques)() {
  return &_SystemExtTorques__type_support;
}

#if defined(__cplusplus)
}
#endif
