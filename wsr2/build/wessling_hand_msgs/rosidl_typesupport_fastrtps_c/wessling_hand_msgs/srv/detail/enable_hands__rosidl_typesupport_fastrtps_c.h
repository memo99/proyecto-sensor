// generated from rosidl_typesupport_fastrtps_c/resource/idl__rosidl_typesupport_fastrtps_c.h.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice
#ifndef WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
#define WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_


#include <stddef.h>
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "wessling_hand_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
size_t get_serialized_size_wessling_hand_msgs__srv__EnableHands_Request(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
size_t max_serialized_size_wessling_hand_msgs__srv__EnableHands_Request(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, wessling_hand_msgs, srv, EnableHands_Request)();

#ifdef __cplusplus
}
#endif

// already included above
// #include <stddef.h>
// already included above
// #include "rosidl_runtime_c/message_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "wessling_hand_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
size_t get_serialized_size_wessling_hand_msgs__srv__EnableHands_Response(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
size_t max_serialized_size_wessling_hand_msgs__srv__EnableHands_Response(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, wessling_hand_msgs, srv, EnableHands_Response)();

#ifdef __cplusplus
}
#endif

#include "rosidl_runtime_c/service_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "wessling_hand_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_wessling_hand_msgs
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, wessling_hand_msgs, srv, EnableHands)();

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
