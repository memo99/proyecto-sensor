// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__BUILDER_HPP_
#define WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__BUILDER_HPP_

#include "wessling_hand_msgs/srv/detail/enable_hands__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace srv
{

namespace builder
{

class Init_EnableHands_Request_num_fingers
{
public:
  explicit Init_EnableHands_Request_num_fingers(::wessling_hand_msgs::srv::EnableHands_Request & msg)
  : msg_(msg)
  {}
  ::wessling_hand_msgs::srv::EnableHands_Request num_fingers(::wessling_hand_msgs::srv::EnableHands_Request::_num_fingers_type arg)
  {
    msg_.num_fingers = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::srv::EnableHands_Request msg_;
};

class Init_EnableHands_Request_enable
{
public:
  Init_EnableHands_Request_enable()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_EnableHands_Request_num_fingers enable(::wessling_hand_msgs::srv::EnableHands_Request::_enable_type arg)
  {
    msg_.enable = std::move(arg);
    return Init_EnableHands_Request_num_fingers(msg_);
  }

private:
  ::wessling_hand_msgs::srv::EnableHands_Request msg_;
};

}  // namespace builder

}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::srv::EnableHands_Request>()
{
  return wessling_hand_msgs::srv::builder::Init_EnableHands_Request_enable();
}

}  // namespace wessling_hand_msgs


namespace wessling_hand_msgs
{

namespace srv
{

namespace builder
{

class Init_EnableHands_Response_enabled
{
public:
  Init_EnableHands_Response_enabled()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::wessling_hand_msgs::srv::EnableHands_Response enabled(::wessling_hand_msgs::srv::EnableHands_Response::_enabled_type arg)
  {
    msg_.enabled = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::srv::EnableHands_Response msg_;
};

}  // namespace builder

}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::srv::EnableHands_Response>()
{
  return wessling_hand_msgs::srv::builder::Init_EnableHands_Response_enabled();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__BUILDER_HPP_
