// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__FINGER_STATUS_HPP_
#define WESSLING_HAND_MSGS__MSG__FINGER_STATUS_HPP_

#include "wessling_hand_msgs/msg/detail/finger_status__struct.hpp"
#include "wessling_hand_msgs/msg/detail/finger_status__builder.hpp"
#include "wessling_hand_msgs/msg/detail/finger_status__traits.hpp"

#endif  // WESSLING_HAND_MSGS__MSG__FINGER_STATUS_HPP_
