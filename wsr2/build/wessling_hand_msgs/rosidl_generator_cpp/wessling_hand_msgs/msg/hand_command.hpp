// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__HAND_COMMAND_HPP_
#define WESSLING_HAND_MSGS__MSG__HAND_COMMAND_HPP_

#include "wessling_hand_msgs/msg/detail/hand_command__struct.hpp"
#include "wessling_hand_msgs/msg/detail/hand_command__builder.hpp"
#include "wessling_hand_msgs/msg/detail/hand_command__traits.hpp"

#endif  // WESSLING_HAND_MSGS__MSG__HAND_COMMAND_HPP_
