// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from wessling_hand_msgs:msg/SystemExtTorques.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__STRUCT_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


// Include directives for member types
// Member 'hand'
#include "wessling_hand_msgs/msg/detail/hand_ext_torques__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__msg__SystemExtTorques __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__msg__SystemExtTorques __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct SystemExtTorques_
{
  using Type = SystemExtTorques_<ContainerAllocator>;

  explicit SystemExtTorques_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->left_hand = false;
      this->right_hand = false;
    }
  }

  explicit SystemExtTorques_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->left_hand = false;
      this->right_hand = false;
    }
  }

  // field types and members
  using _hand_type =
    std::vector<wessling_hand_msgs::msg::HandExtTorques_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::HandExtTorques_<ContainerAllocator>>::other>;
  _hand_type hand;
  using _left_hand_type =
    bool;
  _left_hand_type left_hand;
  using _right_hand_type =
    bool;
  _right_hand_type right_hand;

  // setters for named parameter idiom
  Type & set__hand(
    const std::vector<wessling_hand_msgs::msg::HandExtTorques_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::HandExtTorques_<ContainerAllocator>>::other> & _arg)
  {
    this->hand = _arg;
    return *this;
  }
  Type & set__left_hand(
    const bool & _arg)
  {
    this->left_hand = _arg;
    return *this;
  }
  Type & set__right_hand(
    const bool & _arg)
  {
    this->right_hand = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__msg__SystemExtTorques
    std::shared_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__msg__SystemExtTorques
    std::shared_ptr<wessling_hand_msgs::msg::SystemExtTorques_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const SystemExtTorques_ & other) const
  {
    if (this->hand != other.hand) {
      return false;
    }
    if (this->left_hand != other.left_hand) {
      return false;
    }
    if (this->right_hand != other.right_hand) {
      return false;
    }
    return true;
  }
  bool operator!=(const SystemExtTorques_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct SystemExtTorques_

// alias to use template instance with default allocator
using SystemExtTorques =
  wessling_hand_msgs::msg::SystemExtTorques_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__STRUCT_HPP_
