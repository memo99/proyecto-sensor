// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from wessling_hand_msgs:msg/HandCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__STRUCT_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


// Include directives for member types
// Member 'finger'
#include "wessling_hand_msgs/msg/detail/finger_command__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__msg__HandCommand __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__msg__HandCommand __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct HandCommand_
{
  using Type = HandCommand_<ContainerAllocator>;

  explicit HandCommand_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit HandCommand_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _finger_type =
    std::vector<wessling_hand_msgs::msg::FingerCommand_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::FingerCommand_<ContainerAllocator>>::other>;
  _finger_type finger;

  // setters for named parameter idiom
  Type & set__finger(
    const std::vector<wessling_hand_msgs::msg::FingerCommand_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::FingerCommand_<ContainerAllocator>>::other> & _arg)
  {
    this->finger = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::msg::HandCommand_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::msg::HandCommand_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__msg__HandCommand
    std::shared_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__msg__HandCommand
    std::shared_ptr<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const HandCommand_ & other) const
  {
    if (this->finger != other.finger) {
      return false;
    }
    return true;
  }
  bool operator!=(const HandCommand_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct HandCommand_

// alias to use template instance with default allocator
using HandCommand =
  wessling_hand_msgs::msg::HandCommand_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__STRUCT_HPP_
