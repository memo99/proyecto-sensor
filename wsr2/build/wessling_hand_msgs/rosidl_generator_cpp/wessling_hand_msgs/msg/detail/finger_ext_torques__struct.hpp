// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from wessling_hand_msgs:msg/FingerExtTorques.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__STRUCT_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__msg__FingerExtTorques __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__msg__FingerExtTorques __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct FingerExtTorques_
{
  using Type = FingerExtTorques_<ContainerAllocator>;

  explicit FingerExtTorques_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit FingerExtTorques_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _torque_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _torque_type torque;

  // setters for named parameter idiom
  Type & set__torque(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->torque = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__msg__FingerExtTorques
    std::shared_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__msg__FingerExtTorques
    std::shared_ptr<wessling_hand_msgs::msg::FingerExtTorques_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const FingerExtTorques_ & other) const
  {
    if (this->torque != other.torque) {
      return false;
    }
    return true;
  }
  bool operator!=(const FingerExtTorques_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct FingerExtTorques_

// alias to use template instance with default allocator
using FingerExtTorques =
  wessling_hand_msgs::msg::FingerExtTorques_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__STRUCT_HPP_
