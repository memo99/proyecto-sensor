// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/FingerStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/finger_status__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_FingerStatus_torque
{
public:
  explicit Init_FingerStatus_torque(::wessling_hand_msgs::msg::FingerStatus & msg)
  : msg_(msg)
  {}
  ::wessling_hand_msgs::msg::FingerStatus torque(::wessling_hand_msgs::msg::FingerStatus::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerStatus msg_;
};

class Init_FingerStatus_position
{
public:
  explicit Init_FingerStatus_position(::wessling_hand_msgs::msg::FingerStatus & msg)
  : msg_(msg)
  {}
  Init_FingerStatus_torque position(::wessling_hand_msgs::msg::FingerStatus::_position_type arg)
  {
    msg_.position = std::move(arg);
    return Init_FingerStatus_torque(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerStatus msg_;
};

class Init_FingerStatus_enabled
{
public:
  Init_FingerStatus_enabled()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_FingerStatus_position enabled(::wessling_hand_msgs::msg::FingerStatus::_enabled_type arg)
  {
    msg_.enabled = std::move(arg);
    return Init_FingerStatus_position(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerStatus msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::FingerStatus>()
{
  return wessling_hand_msgs::msg::builder::Init_FingerStatus_enabled();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__BUILDER_HPP_
