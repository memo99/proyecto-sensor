// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/SystemExtTorques.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/system_ext_torques__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_SystemExtTorques_right_hand
{
public:
  explicit Init_SystemExtTorques_right_hand(::wessling_hand_msgs::msg::SystemExtTorques & msg)
  : msg_(msg)
  {}
  ::wessling_hand_msgs::msg::SystemExtTorques right_hand(::wessling_hand_msgs::msg::SystemExtTorques::_right_hand_type arg)
  {
    msg_.right_hand = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemExtTorques msg_;
};

class Init_SystemExtTorques_left_hand
{
public:
  explicit Init_SystemExtTorques_left_hand(::wessling_hand_msgs::msg::SystemExtTorques & msg)
  : msg_(msg)
  {}
  Init_SystemExtTorques_right_hand left_hand(::wessling_hand_msgs::msg::SystemExtTorques::_left_hand_type arg)
  {
    msg_.left_hand = std::move(arg);
    return Init_SystemExtTorques_right_hand(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemExtTorques msg_;
};

class Init_SystemExtTorques_hand
{
public:
  Init_SystemExtTorques_hand()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_SystemExtTorques_left_hand hand(::wessling_hand_msgs::msg::SystemExtTorques::_hand_type arg)
  {
    msg_.hand = std::move(arg);
    return Init_SystemExtTorques_left_hand(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemExtTorques msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::SystemExtTorques>()
{
  return wessling_hand_msgs::msg::builder::Init_SystemExtTorques_hand();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_EXT_TORQUES__BUILDER_HPP_
