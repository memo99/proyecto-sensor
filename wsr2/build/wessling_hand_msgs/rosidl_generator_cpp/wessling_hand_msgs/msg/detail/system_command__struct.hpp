// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from wessling_hand_msgs:msg/SystemCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__STRUCT_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


// Include directives for member types
// Member 'hand'
#include "wessling_hand_msgs/msg/detail/hand_command__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__msg__SystemCommand __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__msg__SystemCommand __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct SystemCommand_
{
  using Type = SystemCommand_<ContainerAllocator>;

  explicit SystemCommand_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->emergency = false;
    }
  }

  explicit SystemCommand_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->emergency = false;
    }
  }

  // field types and members
  using _hand_type =
    std::vector<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>>::other>;
  _hand_type hand;
  using _emergency_type =
    bool;
  _emergency_type emergency;

  // setters for named parameter idiom
  Type & set__hand(
    const std::vector<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::HandCommand_<ContainerAllocator>>::other> & _arg)
  {
    this->hand = _arg;
    return *this;
  }
  Type & set__emergency(
    const bool & _arg)
  {
    this->emergency = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__msg__SystemCommand
    std::shared_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__msg__SystemCommand
    std::shared_ptr<wessling_hand_msgs::msg::SystemCommand_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const SystemCommand_ & other) const
  {
    if (this->hand != other.hand) {
      return false;
    }
    if (this->emergency != other.emergency) {
      return false;
    }
    return true;
  }
  bool operator!=(const SystemCommand_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct SystemCommand_

// alias to use template instance with default allocator
using SystemCommand =
  wessling_hand_msgs::msg::SystemCommand_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__STRUCT_HPP_
