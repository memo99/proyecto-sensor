// generated from rosidl_typesupport_fastrtps_cpp/resource/idl__type_support.cpp.em
// with input from wessling_hand_msgs:msg/SystemStatus.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/msg/detail/system_status__rosidl_typesupport_fastrtps_cpp.hpp"
#include "wessling_hand_msgs/msg/detail/system_status__struct.hpp"

#include <limits>
#include <stdexcept>
#include <string>
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_fastrtps_cpp/identifier.hpp"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_fastrtps_cpp/wstring_conversion.hpp"
#include "fastcdr/Cdr.h"


// forward declaration of message dependencies and their conversion functions
namespace wessling_hand_msgs
{
namespace msg
{
namespace typesupport_fastrtps_cpp
{
bool cdr_serialize(
  const wessling_hand_msgs::msg::HandStatus &,
  eprosima::fastcdr::Cdr &);
bool cdr_deserialize(
  eprosima::fastcdr::Cdr &,
  wessling_hand_msgs::msg::HandStatus &);
size_t get_serialized_size(
  const wessling_hand_msgs::msg::HandStatus &,
  size_t current_alignment);
size_t
max_serialized_size_HandStatus(
  bool & full_bounded,
  size_t current_alignment);
}  // namespace typesupport_fastrtps_cpp
}  // namespace msg
}  // namespace wessling_hand_msgs


namespace wessling_hand_msgs
{

namespace msg
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
cdr_serialize(
  const wessling_hand_msgs::msg::SystemStatus & ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  // Member: hand
  {
    size_t size = ros_message.hand.size();
    cdr << static_cast<uint32_t>(size);
    for (size_t i = 0; i < size; i++) {
      wessling_hand_msgs::msg::typesupport_fastrtps_cpp::cdr_serialize(
        ros_message.hand[i],
        cdr);
    }
  }
  // Member: brake_status
  cdr << (ros_message.brake_status ? true : false);
  // Member: left_hand
  cdr << (ros_message.left_hand ? true : false);
  // Member: right_hand
  cdr << (ros_message.right_hand ? true : false);
  return true;
}

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  wessling_hand_msgs::msg::SystemStatus & ros_message)
{
  // Member: hand
  {
    uint32_t cdrSize;
    cdr >> cdrSize;
    size_t size = static_cast<size_t>(cdrSize);
    ros_message.hand.resize(size);
    for (size_t i = 0; i < size; i++) {
      wessling_hand_msgs::msg::typesupport_fastrtps_cpp::cdr_deserialize(
        cdr, ros_message.hand[i]);
    }
  }

  // Member: brake_status
  {
    uint8_t tmp;
    cdr >> tmp;
    ros_message.brake_status = tmp ? true : false;
  }

  // Member: left_hand
  {
    uint8_t tmp;
    cdr >> tmp;
    ros_message.left_hand = tmp ? true : false;
  }

  // Member: right_hand
  {
    uint8_t tmp;
    cdr >> tmp;
    ros_message.right_hand = tmp ? true : false;
  }

  return true;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
get_serialized_size(
  const wessling_hand_msgs::msg::SystemStatus & ros_message,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // Member: hand
  {
    size_t array_size = ros_message.hand.size();

    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    for (size_t index = 0; index < array_size; ++index) {
      current_alignment +=
        wessling_hand_msgs::msg::typesupport_fastrtps_cpp::get_serialized_size(
        ros_message.hand[index], current_alignment);
    }
  }
  // Member: brake_status
  {
    size_t item_size = sizeof(ros_message.brake_status);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // Member: left_hand
  {
    size_t item_size = sizeof(ros_message.left_hand);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // Member: right_hand
  {
    size_t item_size = sizeof(ros_message.right_hand);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
max_serialized_size_SystemStatus(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;


  // Member: hand
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);


    for (size_t index = 0; index < array_size; ++index) {
      current_alignment +=
        wessling_hand_msgs::msg::typesupport_fastrtps_cpp::max_serialized_size_HandStatus(
        full_bounded, current_alignment);
    }
  }

  // Member: brake_status
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint8_t);
  }

  // Member: left_hand
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint8_t);
  }

  // Member: right_hand
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint8_t);
  }

  return current_alignment - initial_alignment;
}

static bool _SystemStatus__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  auto typed_message =
    static_cast<const wessling_hand_msgs::msg::SystemStatus *>(
    untyped_ros_message);
  return cdr_serialize(*typed_message, cdr);
}

static bool _SystemStatus__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  auto typed_message =
    static_cast<wessling_hand_msgs::msg::SystemStatus *>(
    untyped_ros_message);
  return cdr_deserialize(cdr, *typed_message);
}

static uint32_t _SystemStatus__get_serialized_size(
  const void * untyped_ros_message)
{
  auto typed_message =
    static_cast<const wessling_hand_msgs::msg::SystemStatus *>(
    untyped_ros_message);
  return static_cast<uint32_t>(get_serialized_size(*typed_message, 0));
}

static size_t _SystemStatus__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_SystemStatus(full_bounded, 0);
}

static message_type_support_callbacks_t _SystemStatus__callbacks = {
  "wessling_hand_msgs::msg",
  "SystemStatus",
  _SystemStatus__cdr_serialize,
  _SystemStatus__cdr_deserialize,
  _SystemStatus__get_serialized_size,
  _SystemStatus__max_serialized_size
};

static rosidl_message_type_support_t _SystemStatus__handle = {
  rosidl_typesupport_fastrtps_cpp::typesupport_identifier,
  &_SystemStatus__callbacks,
  get_message_typesupport_handle_function,
};

}  // namespace typesupport_fastrtps_cpp

}  // namespace msg

}  // namespace wessling_hand_msgs

namespace rosidl_typesupport_fastrtps_cpp
{

template<>
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
get_message_type_support_handle<wessling_hand_msgs::msg::SystemStatus>()
{
  return &wessling_hand_msgs::msg::typesupport_fastrtps_cpp::_SystemStatus__handle;
}

}  // namespace rosidl_typesupport_fastrtps_cpp

#ifdef __cplusplus
extern "C"
{
#endif

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, wessling_hand_msgs, msg, SystemStatus)() {
  return &wessling_hand_msgs::msg::typesupport_fastrtps_cpp::_SystemStatus__handle;
}

#ifdef __cplusplus
}
#endif
