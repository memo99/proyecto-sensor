// generated from rosidl_typesupport_fastrtps_cpp/resource/idl__type_support.cpp.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/srv/detail/enable_hands__rosidl_typesupport_fastrtps_cpp.hpp"
#include "wessling_hand_msgs/srv/detail/enable_hands__struct.hpp"

#include <limits>
#include <stdexcept>
#include <string>
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_fastrtps_cpp/identifier.hpp"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_fastrtps_cpp/wstring_conversion.hpp"
#include "fastcdr/Cdr.h"


// forward declaration of message dependencies and their conversion functions

namespace wessling_hand_msgs
{

namespace srv
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
cdr_serialize(
  const wessling_hand_msgs::srv::EnableHands_Request & ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  // Member: enable
  {
    cdr << ros_message.enable;
  }
  // Member: num_fingers
  cdr << ros_message.num_fingers;
  return true;
}

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  wessling_hand_msgs::srv::EnableHands_Request & ros_message)
{
  // Member: enable
  {
    cdr >> ros_message.enable;
  }

  // Member: num_fingers
  cdr >> ros_message.num_fingers;

  return true;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
get_serialized_size(
  const wessling_hand_msgs::srv::EnableHands_Request & ros_message,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // Member: enable
  {
    size_t array_size = ros_message.enable.size();

    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);
    size_t item_size = sizeof(ros_message.enable[0]);
    current_alignment += array_size * item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // Member: num_fingers
  {
    size_t item_size = sizeof(ros_message.num_fingers);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
max_serialized_size_EnableHands_Request(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;


  // Member: enable
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  // Member: num_fingers
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  return current_alignment - initial_alignment;
}

static bool _EnableHands_Request__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  auto typed_message =
    static_cast<const wessling_hand_msgs::srv::EnableHands_Request *>(
    untyped_ros_message);
  return cdr_serialize(*typed_message, cdr);
}

static bool _EnableHands_Request__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  auto typed_message =
    static_cast<wessling_hand_msgs::srv::EnableHands_Request *>(
    untyped_ros_message);
  return cdr_deserialize(cdr, *typed_message);
}

static uint32_t _EnableHands_Request__get_serialized_size(
  const void * untyped_ros_message)
{
  auto typed_message =
    static_cast<const wessling_hand_msgs::srv::EnableHands_Request *>(
    untyped_ros_message);
  return static_cast<uint32_t>(get_serialized_size(*typed_message, 0));
}

static size_t _EnableHands_Request__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_EnableHands_Request(full_bounded, 0);
}

static message_type_support_callbacks_t _EnableHands_Request__callbacks = {
  "wessling_hand_msgs::srv",
  "EnableHands_Request",
  _EnableHands_Request__cdr_serialize,
  _EnableHands_Request__cdr_deserialize,
  _EnableHands_Request__get_serialized_size,
  _EnableHands_Request__max_serialized_size
};

static rosidl_message_type_support_t _EnableHands_Request__handle = {
  rosidl_typesupport_fastrtps_cpp::typesupport_identifier,
  &_EnableHands_Request__callbacks,
  get_message_typesupport_handle_function,
};

}  // namespace typesupport_fastrtps_cpp

}  // namespace srv

}  // namespace wessling_hand_msgs

namespace rosidl_typesupport_fastrtps_cpp
{

template<>
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
get_message_type_support_handle<wessling_hand_msgs::srv::EnableHands_Request>()
{
  return &wessling_hand_msgs::srv::typesupport_fastrtps_cpp::_EnableHands_Request__handle;
}

}  // namespace rosidl_typesupport_fastrtps_cpp

#ifdef __cplusplus
extern "C"
{
#endif

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, wessling_hand_msgs, srv, EnableHands_Request)() {
  return &wessling_hand_msgs::srv::typesupport_fastrtps_cpp::_EnableHands_Request__handle;
}

#ifdef __cplusplus
}
#endif

// already included above
// #include <limits>
// already included above
// #include <stdexcept>
// already included above
// #include <string>
// already included above
// #include "rosidl_typesupport_cpp/message_type_support.hpp"
// already included above
// #include "rosidl_typesupport_fastrtps_cpp/identifier.hpp"
// already included above
// #include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
// already included above
// #include "rosidl_typesupport_fastrtps_cpp/message_type_support_decl.hpp"
// already included above
// #include "rosidl_typesupport_fastrtps_cpp/wstring_conversion.hpp"
// already included above
// #include "fastcdr/Cdr.h"


// forward declaration of message dependencies and their conversion functions

namespace wessling_hand_msgs
{

namespace srv
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
cdr_serialize(
  const wessling_hand_msgs::srv::EnableHands_Response & ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  // Member: enabled
  {
    cdr << ros_message.enabled;
  }
  return true;
}

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  wessling_hand_msgs::srv::EnableHands_Response & ros_message)
{
  // Member: enabled
  {
    cdr >> ros_message.enabled;
  }

  return true;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
get_serialized_size(
  const wessling_hand_msgs::srv::EnableHands_Response & ros_message,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // Member: enabled
  {
    size_t array_size = ros_message.enabled.size();

    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);
    size_t item_size = sizeof(ros_message.enabled[0]);
    current_alignment += array_size * item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_wessling_hand_msgs
max_serialized_size_EnableHands_Response(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;


  // Member: enabled
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  return current_alignment - initial_alignment;
}

static bool _EnableHands_Response__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  auto typed_message =
    static_cast<const wessling_hand_msgs::srv::EnableHands_Response *>(
    untyped_ros_message);
  return cdr_serialize(*typed_message, cdr);
}

static bool _EnableHands_Response__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  auto typed_message =
    static_cast<wessling_hand_msgs::srv::EnableHands_Response *>(
    untyped_ros_message);
  return cdr_deserialize(cdr, *typed_message);
}

static uint32_t _EnableHands_Response__get_serialized_size(
  const void * untyped_ros_message)
{
  auto typed_message =
    static_cast<const wessling_hand_msgs::srv::EnableHands_Response *>(
    untyped_ros_message);
  return static_cast<uint32_t>(get_serialized_size(*typed_message, 0));
}

static size_t _EnableHands_Response__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_EnableHands_Response(full_bounded, 0);
}

static message_type_support_callbacks_t _EnableHands_Response__callbacks = {
  "wessling_hand_msgs::srv",
  "EnableHands_Response",
  _EnableHands_Response__cdr_serialize,
  _EnableHands_Response__cdr_deserialize,
  _EnableHands_Response__get_serialized_size,
  _EnableHands_Response__max_serialized_size
};

static rosidl_message_type_support_t _EnableHands_Response__handle = {
  rosidl_typesupport_fastrtps_cpp::typesupport_identifier,
  &_EnableHands_Response__callbacks,
  get_message_typesupport_handle_function,
};

}  // namespace typesupport_fastrtps_cpp

}  // namespace srv

}  // namespace wessling_hand_msgs

namespace rosidl_typesupport_fastrtps_cpp
{

template<>
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
get_message_type_support_handle<wessling_hand_msgs::srv::EnableHands_Response>()
{
  return &wessling_hand_msgs::srv::typesupport_fastrtps_cpp::_EnableHands_Response__handle;
}

}  // namespace rosidl_typesupport_fastrtps_cpp

#ifdef __cplusplus
extern "C"
{
#endif

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, wessling_hand_msgs, srv, EnableHands_Response)() {
  return &wessling_hand_msgs::srv::typesupport_fastrtps_cpp::_EnableHands_Response__handle;
}

#ifdef __cplusplus
}
#endif

#include "rmw/error_handling.h"
// already included above
// #include "rosidl_typesupport_fastrtps_cpp/identifier.hpp"
#include "rosidl_typesupport_fastrtps_cpp/service_type_support.h"
#include "rosidl_typesupport_fastrtps_cpp/service_type_support_decl.hpp"

namespace wessling_hand_msgs
{

namespace srv
{

namespace typesupport_fastrtps_cpp
{

static service_type_support_callbacks_t _EnableHands__callbacks = {
  "wessling_hand_msgs::srv",
  "EnableHands",
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, wessling_hand_msgs, srv, EnableHands_Request)(),
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, wessling_hand_msgs, srv, EnableHands_Response)(),
};

static rosidl_service_type_support_t _EnableHands__handle = {
  rosidl_typesupport_fastrtps_cpp::typesupport_identifier,
  &_EnableHands__callbacks,
  get_service_typesupport_handle_function,
};

}  // namespace typesupport_fastrtps_cpp

}  // namespace srv

}  // namespace wessling_hand_msgs

namespace rosidl_typesupport_fastrtps_cpp
{

template<>
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_EXPORT_wessling_hand_msgs
const rosidl_service_type_support_t *
get_service_type_support_handle<wessling_hand_msgs::srv::EnableHands>()
{
  return &wessling_hand_msgs::srv::typesupport_fastrtps_cpp::_EnableHands__handle;
}

}  // namespace rosidl_typesupport_fastrtps_cpp

#ifdef __cplusplus
extern "C"
{
#endif

const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, wessling_hand_msgs, srv, EnableHands)() {
  return &wessling_hand_msgs::srv::typesupport_fastrtps_cpp::_EnableHands__handle;
}

#ifdef __cplusplus
}
#endif
