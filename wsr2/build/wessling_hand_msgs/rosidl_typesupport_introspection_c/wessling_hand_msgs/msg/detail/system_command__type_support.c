// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from wessling_hand_msgs:msg/SystemCommand.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "wessling_hand_msgs/msg/detail/system_command__rosidl_typesupport_introspection_c.h"
#include "wessling_hand_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "wessling_hand_msgs/msg/detail/system_command__functions.h"
#include "wessling_hand_msgs/msg/detail/system_command__struct.h"


// Include directives for member types
// Member `hand`
#include "wessling_hand_msgs/msg/hand_command.h"
// Member `hand`
#include "wessling_hand_msgs/msg/detail/hand_command__rosidl_typesupport_introspection_c.h"

#ifdef __cplusplus
extern "C"
{
#endif

void SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  wessling_hand_msgs__msg__SystemCommand__init(message_memory);
}

void SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_fini_function(void * message_memory)
{
  wessling_hand_msgs__msg__SystemCommand__fini(message_memory);
}

size_t SystemCommand__rosidl_typesupport_introspection_c__size_function__HandCommand__hand(
  const void * untyped_member)
{
  const wessling_hand_msgs__msg__HandCommand__Sequence * member =
    (const wessling_hand_msgs__msg__HandCommand__Sequence *)(untyped_member);
  return member->size;
}

const void * SystemCommand__rosidl_typesupport_introspection_c__get_const_function__HandCommand__hand(
  const void * untyped_member, size_t index)
{
  const wessling_hand_msgs__msg__HandCommand__Sequence * member =
    (const wessling_hand_msgs__msg__HandCommand__Sequence *)(untyped_member);
  return &member->data[index];
}

void * SystemCommand__rosidl_typesupport_introspection_c__get_function__HandCommand__hand(
  void * untyped_member, size_t index)
{
  wessling_hand_msgs__msg__HandCommand__Sequence * member =
    (wessling_hand_msgs__msg__HandCommand__Sequence *)(untyped_member);
  return &member->data[index];
}

bool SystemCommand__rosidl_typesupport_introspection_c__resize_function__HandCommand__hand(
  void * untyped_member, size_t size)
{
  wessling_hand_msgs__msg__HandCommand__Sequence * member =
    (wessling_hand_msgs__msg__HandCommand__Sequence *)(untyped_member);
  wessling_hand_msgs__msg__HandCommand__Sequence__fini(member);
  return wessling_hand_msgs__msg__HandCommand__Sequence__init(member, size);
}

static rosidl_typesupport_introspection_c__MessageMember SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_member_array[2] = {
  {
    "hand",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__msg__SystemCommand, hand),  // bytes offset in struct
    NULL,  // default value
    SystemCommand__rosidl_typesupport_introspection_c__size_function__HandCommand__hand,  // size() function pointer
    SystemCommand__rosidl_typesupport_introspection_c__get_const_function__HandCommand__hand,  // get_const(index) function pointer
    SystemCommand__rosidl_typesupport_introspection_c__get_function__HandCommand__hand,  // get(index) function pointer
    SystemCommand__rosidl_typesupport_introspection_c__resize_function__HandCommand__hand  // resize(index) function pointer
  },
  {
    "emergency",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__msg__SystemCommand, emergency),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_members = {
  "wessling_hand_msgs__msg",  // message namespace
  "SystemCommand",  // message name
  2,  // number of fields
  sizeof(wessling_hand_msgs__msg__SystemCommand),
  SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_member_array,  // message members
  SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_init_function,  // function to initialize message memory (memory has to be allocated)
  SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_type_support_handle = {
  0,
  &SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, msg, SystemCommand)() {
  SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_member_array[0].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, msg, HandCommand)();
  if (!SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_type_support_handle.typesupport_identifier) {
    SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &SystemCommand__rosidl_typesupport_introspection_c__SystemCommand_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
