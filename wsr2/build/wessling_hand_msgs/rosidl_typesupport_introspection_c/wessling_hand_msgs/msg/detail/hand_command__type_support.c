// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from wessling_hand_msgs:msg/HandCommand.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "wessling_hand_msgs/msg/detail/hand_command__rosidl_typesupport_introspection_c.h"
#include "wessling_hand_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "wessling_hand_msgs/msg/detail/hand_command__functions.h"
#include "wessling_hand_msgs/msg/detail/hand_command__struct.h"


// Include directives for member types
// Member `finger`
#include "wessling_hand_msgs/msg/finger_command.h"
// Member `finger`
#include "wessling_hand_msgs/msg/detail/finger_command__rosidl_typesupport_introspection_c.h"

#ifdef __cplusplus
extern "C"
{
#endif

void HandCommand__rosidl_typesupport_introspection_c__HandCommand_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  wessling_hand_msgs__msg__HandCommand__init(message_memory);
}

void HandCommand__rosidl_typesupport_introspection_c__HandCommand_fini_function(void * message_memory)
{
  wessling_hand_msgs__msg__HandCommand__fini(message_memory);
}

size_t HandCommand__rosidl_typesupport_introspection_c__size_function__FingerCommand__finger(
  const void * untyped_member)
{
  const wessling_hand_msgs__msg__FingerCommand__Sequence * member =
    (const wessling_hand_msgs__msg__FingerCommand__Sequence *)(untyped_member);
  return member->size;
}

const void * HandCommand__rosidl_typesupport_introspection_c__get_const_function__FingerCommand__finger(
  const void * untyped_member, size_t index)
{
  const wessling_hand_msgs__msg__FingerCommand__Sequence * member =
    (const wessling_hand_msgs__msg__FingerCommand__Sequence *)(untyped_member);
  return &member->data[index];
}

void * HandCommand__rosidl_typesupport_introspection_c__get_function__FingerCommand__finger(
  void * untyped_member, size_t index)
{
  wessling_hand_msgs__msg__FingerCommand__Sequence * member =
    (wessling_hand_msgs__msg__FingerCommand__Sequence *)(untyped_member);
  return &member->data[index];
}

bool HandCommand__rosidl_typesupport_introspection_c__resize_function__FingerCommand__finger(
  void * untyped_member, size_t size)
{
  wessling_hand_msgs__msg__FingerCommand__Sequence * member =
    (wessling_hand_msgs__msg__FingerCommand__Sequence *)(untyped_member);
  wessling_hand_msgs__msg__FingerCommand__Sequence__fini(member);
  return wessling_hand_msgs__msg__FingerCommand__Sequence__init(member, size);
}

static rosidl_typesupport_introspection_c__MessageMember HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_member_array[1] = {
  {
    "finger",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__msg__HandCommand, finger),  // bytes offset in struct
    NULL,  // default value
    HandCommand__rosidl_typesupport_introspection_c__size_function__FingerCommand__finger,  // size() function pointer
    HandCommand__rosidl_typesupport_introspection_c__get_const_function__FingerCommand__finger,  // get_const(index) function pointer
    HandCommand__rosidl_typesupport_introspection_c__get_function__FingerCommand__finger,  // get(index) function pointer
    HandCommand__rosidl_typesupport_introspection_c__resize_function__FingerCommand__finger  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_members = {
  "wessling_hand_msgs__msg",  // message namespace
  "HandCommand",  // message name
  1,  // number of fields
  sizeof(wessling_hand_msgs__msg__HandCommand),
  HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_member_array,  // message members
  HandCommand__rosidl_typesupport_introspection_c__HandCommand_init_function,  // function to initialize message memory (memory has to be allocated)
  HandCommand__rosidl_typesupport_introspection_c__HandCommand_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_type_support_handle = {
  0,
  &HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, msg, HandCommand)() {
  HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_member_array[0].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, msg, FingerCommand)();
  if (!HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_type_support_handle.typesupport_identifier) {
    HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &HandCommand__rosidl_typesupport_introspection_c__HandCommand_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
