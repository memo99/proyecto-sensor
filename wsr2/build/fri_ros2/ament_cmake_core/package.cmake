set(_AMENT_PACKAGE_NAME "fri_ros2")
set(fri_ros2_VERSION "0.1.0")
set(fri_ros2_MAINTAINER "degv364 <degv364@gmail.com>")
set(fri_ros2_BUILD_DEPENDS "rclcpp")
set(fri_ros2_BUILDTOOL_DEPENDS "ament_cmake")
set(fri_ros2_BUILD_EXPORT_DEPENDS "rclcpp")
set(fri_ros2_BUILDTOOL_EXPORT_DEPENDS )
set(fri_ros2_EXEC_DEPENDS "rclcpp")
set(fri_ros2_TEST_DEPENDS "ament_lint_auto" "ament_lint_common")
set(fri_ros2_GROUP_DEPENDS )
set(fri_ros2_MEMBER_OF_GROUPS )
set(fri_ros2_DEPRECATED "")
set(fri_ros2_EXPORT_TAGS)
list(APPEND fri_ros2_EXPORT_TAGS "<build_type>ament_cmake</build_type>")
