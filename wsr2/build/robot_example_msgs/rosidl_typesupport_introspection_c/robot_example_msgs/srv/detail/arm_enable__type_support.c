// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from robot_example_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "robot_example_msgs/srv/detail/arm_enable__rosidl_typesupport_introspection_c.h"
#include "robot_example_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "robot_example_msgs/srv/detail/arm_enable__functions.h"
#include "robot_example_msgs/srv/detail/arm_enable__struct.h"


// Include directives for member types
// Member `enable`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  robot_example_msgs__srv__ArmEnable_Request__init(message_memory);
}

void ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_fini_function(void * message_memory)
{
  robot_example_msgs__srv__ArmEnable_Request__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_member_array[1] = {
  {
    "enable",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(robot_example_msgs__srv__ArmEnable_Request, enable),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_members = {
  "robot_example_msgs__srv",  // message namespace
  "ArmEnable_Request",  // message name
  1,  // number of fields
  sizeof(robot_example_msgs__srv__ArmEnable_Request),
  ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_member_array,  // message members
  ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_init_function,  // function to initialize message memory (memory has to be allocated)
  ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_type_support_handle = {
  0,
  &ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_robot_example_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, srv, ArmEnable_Request)() {
  if (!ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_type_support_handle.typesupport_identifier) {
    ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &ArmEnable_Request__rosidl_typesupport_introspection_c__ArmEnable_Request_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

// already included above
// #include <stddef.h>
// already included above
// #include "robot_example_msgs/srv/detail/arm_enable__rosidl_typesupport_introspection_c.h"
// already included above
// #include "robot_example_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "rosidl_typesupport_introspection_c/field_types.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
// already included above
// #include "rosidl_typesupport_introspection_c/message_introspection.h"
// already included above
// #include "robot_example_msgs/srv/detail/arm_enable__functions.h"
// already included above
// #include "robot_example_msgs/srv/detail/arm_enable__struct.h"


// Include directives for member types
// Member `enabled`
// already included above
// #include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  robot_example_msgs__srv__ArmEnable_Response__init(message_memory);
}

void ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_fini_function(void * message_memory)
{
  robot_example_msgs__srv__ArmEnable_Response__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_member_array[1] = {
  {
    "enabled",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(robot_example_msgs__srv__ArmEnable_Response, enabled),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_members = {
  "robot_example_msgs__srv",  // message namespace
  "ArmEnable_Response",  // message name
  1,  // number of fields
  sizeof(robot_example_msgs__srv__ArmEnable_Response),
  ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_member_array,  // message members
  ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_init_function,  // function to initialize message memory (memory has to be allocated)
  ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_type_support_handle = {
  0,
  &ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_robot_example_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, srv, ArmEnable_Response)() {
  if (!ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_type_support_handle.typesupport_identifier) {
    ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &ArmEnable_Response__rosidl_typesupport_introspection_c__ArmEnable_Response_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

#include "rosidl_runtime_c/service_type_support_struct.h"
// already included above
// #include "robot_example_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "robot_example_msgs/srv/detail/arm_enable__rosidl_typesupport_introspection_c.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/service_introspection.h"

// this is intentionally not const to allow initialization later to prevent an initialization race
static rosidl_typesupport_introspection_c__ServiceMembers robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_service_members = {
  "robot_example_msgs__srv",  // service namespace
  "ArmEnable",  // service name
  // these two fields are initialized below on the first access
  NULL,  // request message
  // robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_Request_message_type_support_handle,
  NULL  // response message
  // robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_Response_message_type_support_handle
};

static rosidl_service_type_support_t robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_service_type_support_handle = {
  0,
  &robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_service_members,
  get_service_typesupport_handle_function,
};

// Forward declaration of request/response type support functions
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, srv, ArmEnable_Request)();

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, srv, ArmEnable_Response)();

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_robot_example_msgs
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, srv, ArmEnable)() {
  if (!robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_service_type_support_handle.typesupport_identifier) {
    robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_service_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  rosidl_typesupport_introspection_c__ServiceMembers * service_members =
    (rosidl_typesupport_introspection_c__ServiceMembers *)robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_service_type_support_handle.data;

  if (!service_members->request_members_) {
    service_members->request_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, srv, ArmEnable_Request)()->data;
  }
  if (!service_members->response_members_) {
    service_members->response_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, srv, ArmEnable_Response)()->data;
  }

  return &robot_example_msgs__srv__detail__arm_enable__rosidl_typesupport_introspection_c__ArmEnable_service_type_support_handle;
}
