// generated from rosidl_typesupport_fastrtps_cpp/resource/idl__type_support.cpp.em
// with input from robot_example_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice
#include "robot_example_msgs/msg/detail/arm_ext_torques__rosidl_typesupport_fastrtps_cpp.hpp"
#include "robot_example_msgs/msg/detail/arm_ext_torques__struct.hpp"

#include <limits>
#include <stdexcept>
#include <string>
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_fastrtps_cpp/identifier.hpp"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_fastrtps_cpp/wstring_conversion.hpp"
#include "fastcdr/Cdr.h"


// forward declaration of message dependencies and their conversion functions

namespace robot_example_msgs
{

namespace msg
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_robot_example_msgs
cdr_serialize(
  const robot_example_msgs::msg::ArmExtTorques & ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  // Member: torque
  {
    cdr << ros_message.torque;
  }
  return true;
}

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_robot_example_msgs
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  robot_example_msgs::msg::ArmExtTorques & ros_message)
{
  // Member: torque
  {
    cdr >> ros_message.torque;
  }

  return true;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_robot_example_msgs
get_serialized_size(
  const robot_example_msgs::msg::ArmExtTorques & ros_message,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // Member: torque
  {
    size_t array_size = ros_message.torque.size();

    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);
    size_t item_size = sizeof(ros_message.torque[0]);
    current_alignment += array_size * item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_robot_example_msgs
max_serialized_size_ArmExtTorques(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;


  // Member: torque
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  return current_alignment - initial_alignment;
}

static bool _ArmExtTorques__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  auto typed_message =
    static_cast<const robot_example_msgs::msg::ArmExtTorques *>(
    untyped_ros_message);
  return cdr_serialize(*typed_message, cdr);
}

static bool _ArmExtTorques__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  auto typed_message =
    static_cast<robot_example_msgs::msg::ArmExtTorques *>(
    untyped_ros_message);
  return cdr_deserialize(cdr, *typed_message);
}

static uint32_t _ArmExtTorques__get_serialized_size(
  const void * untyped_ros_message)
{
  auto typed_message =
    static_cast<const robot_example_msgs::msg::ArmExtTorques *>(
    untyped_ros_message);
  return static_cast<uint32_t>(get_serialized_size(*typed_message, 0));
}

static size_t _ArmExtTorques__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_ArmExtTorques(full_bounded, 0);
}

static message_type_support_callbacks_t _ArmExtTorques__callbacks = {
  "robot_example_msgs::msg",
  "ArmExtTorques",
  _ArmExtTorques__cdr_serialize,
  _ArmExtTorques__cdr_deserialize,
  _ArmExtTorques__get_serialized_size,
  _ArmExtTorques__max_serialized_size
};

static rosidl_message_type_support_t _ArmExtTorques__handle = {
  rosidl_typesupport_fastrtps_cpp::typesupport_identifier,
  &_ArmExtTorques__callbacks,
  get_message_typesupport_handle_function,
};

}  // namespace typesupport_fastrtps_cpp

}  // namespace msg

}  // namespace robot_example_msgs

namespace rosidl_typesupport_fastrtps_cpp
{

template<>
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_EXPORT_robot_example_msgs
const rosidl_message_type_support_t *
get_message_type_support_handle<robot_example_msgs::msg::ArmExtTorques>()
{
  return &robot_example_msgs::msg::typesupport_fastrtps_cpp::_ArmExtTorques__handle;
}

}  // namespace rosidl_typesupport_fastrtps_cpp

#ifdef __cplusplus
extern "C"
{
#endif

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, robot_example_msgs, msg, ArmExtTorques)() {
  return &robot_example_msgs::msg::typesupport_fastrtps_cpp::_ArmExtTorques__handle;
}

#ifdef __cplusplus
}
#endif
