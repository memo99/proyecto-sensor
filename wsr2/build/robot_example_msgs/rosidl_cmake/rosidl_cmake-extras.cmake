# generated from rosidl_cmake/cmake/rosidl_cmake-extras.cmake.in

set(robot_example_msgs_IDL_FILES "msg/ArmExtTorques.idl;msg/ArmCommand.idl;msg/ArmStatus.idl;srv/ArmEnable.idl")
set(robot_example_msgs_INTERFACE_FILES "msg/ArmExtTorques.msg;msg/ArmCommand.msg;msg/ArmStatus.msg;srv/ArmEnable.srv;srv/ArmEnable_Request.msg;srv/ArmEnable_Response.msg")
