file(REMOVE_RECURSE
  "CMakeFiles/robot_example_msgs__cpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/arm_command.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/arm_ext_torques.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/arm_status.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_command__builder.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_command__struct.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_command__traits.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_ext_torques__builder.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_ext_torques__struct.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_ext_torques__traits.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_status__builder.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_status__struct.hpp"
  "rosidl_generator_cpp/robot_example_msgs/msg/detail/arm_status__traits.hpp"
  "rosidl_generator_cpp/robot_example_msgs/srv/arm_enable.hpp"
  "rosidl_generator_cpp/robot_example_msgs/srv/detail/arm_enable__builder.hpp"
  "rosidl_generator_cpp/robot_example_msgs/srv/detail/arm_enable__struct.hpp"
  "rosidl_generator_cpp/robot_example_msgs/srv/detail/arm_enable__traits.hpp"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/robot_example_msgs__cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
