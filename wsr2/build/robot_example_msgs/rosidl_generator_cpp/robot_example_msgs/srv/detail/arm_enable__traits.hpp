// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from robot_example_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__TRAITS_HPP_
#define ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__TRAITS_HPP_

#include "robot_example_msgs/srv/detail/arm_enable__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<robot_example_msgs::srv::ArmEnable_Request>()
{
  return "robot_example_msgs::srv::ArmEnable_Request";
}

template<>
inline const char * name<robot_example_msgs::srv::ArmEnable_Request>()
{
  return "robot_example_msgs/srv/ArmEnable_Request";
}

template<>
struct has_fixed_size<robot_example_msgs::srv::ArmEnable_Request>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<robot_example_msgs::srv::ArmEnable_Request>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<robot_example_msgs::srv::ArmEnable_Request>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<robot_example_msgs::srv::ArmEnable_Response>()
{
  return "robot_example_msgs::srv::ArmEnable_Response";
}

template<>
inline const char * name<robot_example_msgs::srv::ArmEnable_Response>()
{
  return "robot_example_msgs/srv/ArmEnable_Response";
}

template<>
struct has_fixed_size<robot_example_msgs::srv::ArmEnable_Response>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<robot_example_msgs::srv::ArmEnable_Response>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<robot_example_msgs::srv::ArmEnable_Response>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<robot_example_msgs::srv::ArmEnable>()
{
  return "robot_example_msgs::srv::ArmEnable";
}

template<>
inline const char * name<robot_example_msgs::srv::ArmEnable>()
{
  return "robot_example_msgs/srv/ArmEnable";
}

template<>
struct has_fixed_size<robot_example_msgs::srv::ArmEnable>
  : std::integral_constant<
    bool,
    has_fixed_size<robot_example_msgs::srv::ArmEnable_Request>::value &&
    has_fixed_size<robot_example_msgs::srv::ArmEnable_Response>::value
  >
{
};

template<>
struct has_bounded_size<robot_example_msgs::srv::ArmEnable>
  : std::integral_constant<
    bool,
    has_bounded_size<robot_example_msgs::srv::ArmEnable_Request>::value &&
    has_bounded_size<robot_example_msgs::srv::ArmEnable_Response>::value
  >
{
};

template<>
struct is_service<robot_example_msgs::srv::ArmEnable>
  : std::true_type
{
};

template<>
struct is_service_request<robot_example_msgs::srv::ArmEnable_Request>
  : std::true_type
{
};

template<>
struct is_service_response<robot_example_msgs::srv::ArmEnable_Response>
  : std::true_type
{
};

}  // namespace rosidl_generator_traits

#endif  // ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__TRAITS_HPP_
