// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__SRV__ARM_ENABLE_HPP_
#define ROBOT_EXAMPLE_MSGS__SRV__ARM_ENABLE_HPP_

#include "robot_example_msgs/srv/detail/arm_enable__struct.hpp"
#include "robot_example_msgs/srv/detail/arm_enable__builder.hpp"
#include "robot_example_msgs/srv/detail/arm_enable__traits.hpp"

#endif  // ROBOT_EXAMPLE_MSGS__SRV__ARM_ENABLE_HPP_
