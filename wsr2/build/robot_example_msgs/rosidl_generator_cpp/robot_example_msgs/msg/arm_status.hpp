// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__ARM_STATUS_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__ARM_STATUS_HPP_

#include "robot_example_msgs/msg/detail/arm_status__struct.hpp"
#include "robot_example_msgs/msg/detail/arm_status__builder.hpp"
#include "robot_example_msgs/msg/detail/arm_status__traits.hpp"

#endif  // ROBOT_EXAMPLE_MSGS__MSG__ARM_STATUS_HPP_
