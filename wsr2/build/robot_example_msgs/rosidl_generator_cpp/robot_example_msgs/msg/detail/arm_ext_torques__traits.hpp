// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from robot_example_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__TRAITS_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__TRAITS_HPP_

#include "robot_example_msgs/msg/detail/arm_ext_torques__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<robot_example_msgs::msg::ArmExtTorques>()
{
  return "robot_example_msgs::msg::ArmExtTorques";
}

template<>
inline const char * name<robot_example_msgs::msg::ArmExtTorques>()
{
  return "robot_example_msgs/msg/ArmExtTorques";
}

template<>
struct has_fixed_size<robot_example_msgs::msg::ArmExtTorques>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<robot_example_msgs::msg::ArmExtTorques>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<robot_example_msgs::msg::ArmExtTorques>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__TRAITS_HPP_
