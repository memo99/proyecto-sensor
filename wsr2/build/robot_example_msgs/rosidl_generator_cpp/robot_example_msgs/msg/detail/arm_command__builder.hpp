// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from robot_example_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_COMMAND__BUILDER_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_COMMAND__BUILDER_HPP_

#include "robot_example_msgs/msg/detail/arm_command__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace robot_example_msgs
{

namespace msg
{

namespace builder
{

class Init_ArmCommand_stiffness
{
public:
  explicit Init_ArmCommand_stiffness(::robot_example_msgs::msg::ArmCommand & msg)
  : msg_(msg)
  {}
  ::robot_example_msgs::msg::ArmCommand stiffness(::robot_example_msgs::msg::ArmCommand::_stiffness_type arg)
  {
    msg_.stiffness = std::move(arg);
    return std::move(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmCommand msg_;
};

class Init_ArmCommand_velocity
{
public:
  explicit Init_ArmCommand_velocity(::robot_example_msgs::msg::ArmCommand & msg)
  : msg_(msg)
  {}
  Init_ArmCommand_stiffness velocity(::robot_example_msgs::msg::ArmCommand::_velocity_type arg)
  {
    msg_.velocity = std::move(arg);
    return Init_ArmCommand_stiffness(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmCommand msg_;
};

class Init_ArmCommand_kp
{
public:
  explicit Init_ArmCommand_kp(::robot_example_msgs::msg::ArmCommand & msg)
  : msg_(msg)
  {}
  Init_ArmCommand_velocity kp(::robot_example_msgs::msg::ArmCommand::_kp_type arg)
  {
    msg_.kp = std::move(arg);
    return Init_ArmCommand_velocity(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmCommand msg_;
};

class Init_ArmCommand_angle
{
public:
  explicit Init_ArmCommand_angle(::robot_example_msgs::msg::ArmCommand & msg)
  : msg_(msg)
  {}
  Init_ArmCommand_kp angle(::robot_example_msgs::msg::ArmCommand::_angle_type arg)
  {
    msg_.angle = std::move(arg);
    return Init_ArmCommand_kp(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmCommand msg_;
};

class Init_ArmCommand_enable
{
public:
  Init_ArmCommand_enable()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_ArmCommand_angle enable(::robot_example_msgs::msg::ArmCommand::_enable_type arg)
  {
    msg_.enable = std::move(arg);
    return Init_ArmCommand_angle(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmCommand msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::robot_example_msgs::msg::ArmCommand>()
{
  return robot_example_msgs::msg::builder::Init_ArmCommand_enable();
}

}  // namespace robot_example_msgs

#endif  // ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_COMMAND__BUILDER_HPP_
