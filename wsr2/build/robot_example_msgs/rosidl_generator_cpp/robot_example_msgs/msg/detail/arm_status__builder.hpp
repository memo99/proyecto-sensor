// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from robot_example_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__BUILDER_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__BUILDER_HPP_

#include "robot_example_msgs/msg/detail/arm_status__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace robot_example_msgs
{

namespace msg
{

namespace builder
{

class Init_ArmStatus_torque
{
public:
  explicit Init_ArmStatus_torque(::robot_example_msgs::msg::ArmStatus & msg)
  : msg_(msg)
  {}
  ::robot_example_msgs::msg::ArmStatus torque(::robot_example_msgs::msg::ArmStatus::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmStatus msg_;
};

class Init_ArmStatus_position
{
public:
  explicit Init_ArmStatus_position(::robot_example_msgs::msg::ArmStatus & msg)
  : msg_(msg)
  {}
  Init_ArmStatus_torque position(::robot_example_msgs::msg::ArmStatus::_position_type arg)
  {
    msg_.position = std::move(arg);
    return Init_ArmStatus_torque(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmStatus msg_;
};

class Init_ArmStatus_enabled
{
public:
  Init_ArmStatus_enabled()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_ArmStatus_position enabled(::robot_example_msgs::msg::ArmStatus::_enabled_type arg)
  {
    msg_.enabled = std::move(arg);
    return Init_ArmStatus_position(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmStatus msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::robot_example_msgs::msg::ArmStatus>()
{
  return robot_example_msgs::msg::builder::Init_ArmStatus_enabled();
}

}  // namespace robot_example_msgs

#endif  // ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__BUILDER_HPP_
