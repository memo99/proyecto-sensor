// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from robot_example_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice
#include "robot_example_msgs/srv/detail/arm_enable__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// Include directives for member types
// Member `enable`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
robot_example_msgs__srv__ArmEnable_Request__init(robot_example_msgs__srv__ArmEnable_Request * msg)
{
  if (!msg) {
    return false;
  }
  // enable
  if (!rosidl_runtime_c__float__Sequence__init(&msg->enable, 0)) {
    robot_example_msgs__srv__ArmEnable_Request__fini(msg);
    return false;
  }
  return true;
}

void
robot_example_msgs__srv__ArmEnable_Request__fini(robot_example_msgs__srv__ArmEnable_Request * msg)
{
  if (!msg) {
    return;
  }
  // enable
  rosidl_runtime_c__float__Sequence__fini(&msg->enable);
}

robot_example_msgs__srv__ArmEnable_Request *
robot_example_msgs__srv__ArmEnable_Request__create()
{
  robot_example_msgs__srv__ArmEnable_Request * msg = (robot_example_msgs__srv__ArmEnable_Request *)malloc(sizeof(robot_example_msgs__srv__ArmEnable_Request));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(robot_example_msgs__srv__ArmEnable_Request));
  bool success = robot_example_msgs__srv__ArmEnable_Request__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
robot_example_msgs__srv__ArmEnable_Request__destroy(robot_example_msgs__srv__ArmEnable_Request * msg)
{
  if (msg) {
    robot_example_msgs__srv__ArmEnable_Request__fini(msg);
  }
  free(msg);
}


bool
robot_example_msgs__srv__ArmEnable_Request__Sequence__init(robot_example_msgs__srv__ArmEnable_Request__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  robot_example_msgs__srv__ArmEnable_Request * data = NULL;
  if (size) {
    data = (robot_example_msgs__srv__ArmEnable_Request *)calloc(size, sizeof(robot_example_msgs__srv__ArmEnable_Request));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = robot_example_msgs__srv__ArmEnable_Request__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        robot_example_msgs__srv__ArmEnable_Request__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
robot_example_msgs__srv__ArmEnable_Request__Sequence__fini(robot_example_msgs__srv__ArmEnable_Request__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      robot_example_msgs__srv__ArmEnable_Request__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

robot_example_msgs__srv__ArmEnable_Request__Sequence *
robot_example_msgs__srv__ArmEnable_Request__Sequence__create(size_t size)
{
  robot_example_msgs__srv__ArmEnable_Request__Sequence * array = (robot_example_msgs__srv__ArmEnable_Request__Sequence *)malloc(sizeof(robot_example_msgs__srv__ArmEnable_Request__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = robot_example_msgs__srv__ArmEnable_Request__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
robot_example_msgs__srv__ArmEnable_Request__Sequence__destroy(robot_example_msgs__srv__ArmEnable_Request__Sequence * array)
{
  if (array) {
    robot_example_msgs__srv__ArmEnable_Request__Sequence__fini(array);
  }
  free(array);
}


// Include directives for member types
// Member `enabled`
// already included above
// #include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
robot_example_msgs__srv__ArmEnable_Response__init(robot_example_msgs__srv__ArmEnable_Response * msg)
{
  if (!msg) {
    return false;
  }
  // enabled
  if (!rosidl_runtime_c__float__Sequence__init(&msg->enabled, 0)) {
    robot_example_msgs__srv__ArmEnable_Response__fini(msg);
    return false;
  }
  return true;
}

void
robot_example_msgs__srv__ArmEnable_Response__fini(robot_example_msgs__srv__ArmEnable_Response * msg)
{
  if (!msg) {
    return;
  }
  // enabled
  rosidl_runtime_c__float__Sequence__fini(&msg->enabled);
}

robot_example_msgs__srv__ArmEnable_Response *
robot_example_msgs__srv__ArmEnable_Response__create()
{
  robot_example_msgs__srv__ArmEnable_Response * msg = (robot_example_msgs__srv__ArmEnable_Response *)malloc(sizeof(robot_example_msgs__srv__ArmEnable_Response));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(robot_example_msgs__srv__ArmEnable_Response));
  bool success = robot_example_msgs__srv__ArmEnable_Response__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
robot_example_msgs__srv__ArmEnable_Response__destroy(robot_example_msgs__srv__ArmEnable_Response * msg)
{
  if (msg) {
    robot_example_msgs__srv__ArmEnable_Response__fini(msg);
  }
  free(msg);
}


bool
robot_example_msgs__srv__ArmEnable_Response__Sequence__init(robot_example_msgs__srv__ArmEnable_Response__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  robot_example_msgs__srv__ArmEnable_Response * data = NULL;
  if (size) {
    data = (robot_example_msgs__srv__ArmEnable_Response *)calloc(size, sizeof(robot_example_msgs__srv__ArmEnable_Response));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = robot_example_msgs__srv__ArmEnable_Response__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        robot_example_msgs__srv__ArmEnable_Response__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
robot_example_msgs__srv__ArmEnable_Response__Sequence__fini(robot_example_msgs__srv__ArmEnable_Response__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      robot_example_msgs__srv__ArmEnable_Response__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

robot_example_msgs__srv__ArmEnable_Response__Sequence *
robot_example_msgs__srv__ArmEnable_Response__Sequence__create(size_t size)
{
  robot_example_msgs__srv__ArmEnable_Response__Sequence * array = (robot_example_msgs__srv__ArmEnable_Response__Sequence *)malloc(sizeof(robot_example_msgs__srv__ArmEnable_Response__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = robot_example_msgs__srv__ArmEnable_Response__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
robot_example_msgs__srv__ArmEnable_Response__Sequence__destroy(robot_example_msgs__srv__ArmEnable_Response__Sequence * array)
{
  if (array) {
    robot_example_msgs__srv__ArmEnable_Response__Sequence__fini(array);
  }
  free(array);
}
