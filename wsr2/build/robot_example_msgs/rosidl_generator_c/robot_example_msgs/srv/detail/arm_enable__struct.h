// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from robot_example_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_H_
#define ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'enable'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/ArmEnable in the package robot_example_msgs.
typedef struct robot_example_msgs__srv__ArmEnable_Request
{
  rosidl_runtime_c__float__Sequence enable;
} robot_example_msgs__srv__ArmEnable_Request;

// Struct for a sequence of robot_example_msgs__srv__ArmEnable_Request.
typedef struct robot_example_msgs__srv__ArmEnable_Request__Sequence
{
  robot_example_msgs__srv__ArmEnable_Request * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} robot_example_msgs__srv__ArmEnable_Request__Sequence;


// Constants defined in the message

// Include directives for member types
// Member 'enabled'
// already included above
// #include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/ArmEnable in the package robot_example_msgs.
typedef struct robot_example_msgs__srv__ArmEnable_Response
{
  rosidl_runtime_c__float__Sequence enabled;
} robot_example_msgs__srv__ArmEnable_Response;

// Struct for a sequence of robot_example_msgs__srv__ArmEnable_Response.
typedef struct robot_example_msgs__srv__ArmEnable_Response__Sequence
{
  robot_example_msgs__srv__ArmEnable_Response * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} robot_example_msgs__srv__ArmEnable_Response__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_H_
