// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from robot_example_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice
#include "robot_example_msgs/msg/detail/arm_command__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `angle`
// Member `kp`
// Member `velocity`
// Member `stiffness`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
robot_example_msgs__msg__ArmCommand__init(robot_example_msgs__msg__ArmCommand * msg)
{
  if (!msg) {
    return false;
  }
  // enable
  // angle
  if (!rosidl_runtime_c__float__Sequence__init(&msg->angle, 0)) {
    robot_example_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  // kp
  if (!rosidl_runtime_c__float__Sequence__init(&msg->kp, 0)) {
    robot_example_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  // velocity
  if (!rosidl_runtime_c__float__Sequence__init(&msg->velocity, 0)) {
    robot_example_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  // stiffness
  if (!rosidl_runtime_c__float__Sequence__init(&msg->stiffness, 0)) {
    robot_example_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  return true;
}

void
robot_example_msgs__msg__ArmCommand__fini(robot_example_msgs__msg__ArmCommand * msg)
{
  if (!msg) {
    return;
  }
  // enable
  // angle
  rosidl_runtime_c__float__Sequence__fini(&msg->angle);
  // kp
  rosidl_runtime_c__float__Sequence__fini(&msg->kp);
  // velocity
  rosidl_runtime_c__float__Sequence__fini(&msg->velocity);
  // stiffness
  rosidl_runtime_c__float__Sequence__fini(&msg->stiffness);
}

robot_example_msgs__msg__ArmCommand *
robot_example_msgs__msg__ArmCommand__create()
{
  robot_example_msgs__msg__ArmCommand * msg = (robot_example_msgs__msg__ArmCommand *)malloc(sizeof(robot_example_msgs__msg__ArmCommand));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(robot_example_msgs__msg__ArmCommand));
  bool success = robot_example_msgs__msg__ArmCommand__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
robot_example_msgs__msg__ArmCommand__destroy(robot_example_msgs__msg__ArmCommand * msg)
{
  if (msg) {
    robot_example_msgs__msg__ArmCommand__fini(msg);
  }
  free(msg);
}


bool
robot_example_msgs__msg__ArmCommand__Sequence__init(robot_example_msgs__msg__ArmCommand__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  robot_example_msgs__msg__ArmCommand * data = NULL;
  if (size) {
    data = (robot_example_msgs__msg__ArmCommand *)calloc(size, sizeof(robot_example_msgs__msg__ArmCommand));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = robot_example_msgs__msg__ArmCommand__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        robot_example_msgs__msg__ArmCommand__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
robot_example_msgs__msg__ArmCommand__Sequence__fini(robot_example_msgs__msg__ArmCommand__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      robot_example_msgs__msg__ArmCommand__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

robot_example_msgs__msg__ArmCommand__Sequence *
robot_example_msgs__msg__ArmCommand__Sequence__create(size_t size)
{
  robot_example_msgs__msg__ArmCommand__Sequence * array = (robot_example_msgs__msg__ArmCommand__Sequence *)malloc(sizeof(robot_example_msgs__msg__ArmCommand__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = robot_example_msgs__msg__ArmCommand__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
robot_example_msgs__msg__ArmCommand__Sequence__destroy(robot_example_msgs__msg__ArmCommand__Sequence * array)
{
  if (array) {
    robot_example_msgs__msg__ArmCommand__Sequence__fini(array);
  }
  free(array);
}
