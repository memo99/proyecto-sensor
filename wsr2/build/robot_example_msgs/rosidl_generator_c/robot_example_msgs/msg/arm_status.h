// generated from rosidl_generator_c/resource/idl.h.em
// with input from robot_example_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__ARM_STATUS_H_
#define ROBOT_EXAMPLE_MSGS__MSG__ARM_STATUS_H_

#include "robot_example_msgs/msg/detail/arm_status__struct.h"
#include "robot_example_msgs/msg/detail/arm_status__functions.h"
#include "robot_example_msgs/msg/detail/arm_status__type_support.h"

#endif  // ROBOT_EXAMPLE_MSGS__MSG__ARM_STATUS_H_
