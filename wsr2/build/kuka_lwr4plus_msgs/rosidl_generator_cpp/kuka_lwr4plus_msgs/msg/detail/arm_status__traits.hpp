// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from kuka_lwr4plus_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__TRAITS_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__TRAITS_HPP_

#include "kuka_lwr4plus_msgs/msg/detail/arm_status__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<kuka_lwr4plus_msgs::msg::ArmStatus>()
{
  return "kuka_lwr4plus_msgs::msg::ArmStatus";
}

template<>
inline const char * name<kuka_lwr4plus_msgs::msg::ArmStatus>()
{
  return "kuka_lwr4plus_msgs/msg/ArmStatus";
}

template<>
struct has_fixed_size<kuka_lwr4plus_msgs::msg::ArmStatus>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<kuka_lwr4plus_msgs::msg::ArmStatus>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<kuka_lwr4plus_msgs::msg::ArmStatus>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__TRAITS_HPP_
