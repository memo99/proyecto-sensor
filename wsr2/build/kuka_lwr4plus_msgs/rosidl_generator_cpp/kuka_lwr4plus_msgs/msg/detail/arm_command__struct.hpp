// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from kuka_lwr4plus_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__STRUCT_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__kuka_lwr4plus_msgs__msg__ArmCommand __attribute__((deprecated))
#else
# define DEPRECATED__kuka_lwr4plus_msgs__msg__ArmCommand __declspec(deprecated)
#endif

namespace kuka_lwr4plus_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct ArmCommand_
{
  using Type = ArmCommand_<ContainerAllocator>;

  explicit ArmCommand_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->enable = false;
    }
  }

  explicit ArmCommand_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->enable = false;
    }
  }

  // field types and members
  using _enable_type =
    bool;
  _enable_type enable;
  using _angle_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _angle_type angle;
  using _kp_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _kp_type kp;
  using _velocity_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _velocity_type velocity;
  using _stiffness_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _stiffness_type stiffness;

  // setters for named parameter idiom
  Type & set__enable(
    const bool & _arg)
  {
    this->enable = _arg;
    return *this;
  }
  Type & set__angle(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->angle = _arg;
    return *this;
  }
  Type & set__kp(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->kp = _arg;
    return *this;
  }
  Type & set__velocity(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->velocity = _arg;
    return *this;
  }
  Type & set__stiffness(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->stiffness = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator> *;
  using ConstRawPtr =
    const kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__kuka_lwr4plus_msgs__msg__ArmCommand
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__kuka_lwr4plus_msgs__msg__ArmCommand
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmCommand_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const ArmCommand_ & other) const
  {
    if (this->enable != other.enable) {
      return false;
    }
    if (this->angle != other.angle) {
      return false;
    }
    if (this->kp != other.kp) {
      return false;
    }
    if (this->velocity != other.velocity) {
      return false;
    }
    if (this->stiffness != other.stiffness) {
      return false;
    }
    return true;
  }
  bool operator!=(const ArmCommand_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct ArmCommand_

// alias to use template instance with default allocator
using ArmCommand =
  kuka_lwr4plus_msgs::msg::ArmCommand_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace kuka_lwr4plus_msgs

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__STRUCT_HPP_
