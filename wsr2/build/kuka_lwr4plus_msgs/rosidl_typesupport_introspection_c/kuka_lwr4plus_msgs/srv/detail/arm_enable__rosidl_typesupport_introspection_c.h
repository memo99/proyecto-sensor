// generated from rosidl_typesupport_introspection_c/resource/idl__rosidl_typesupport_introspection_c.h.em
// with input from kuka_lwr4plus_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
#define KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "kuka_lwr4plus_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_kuka_lwr4plus_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, kuka_lwr4plus_msgs, srv, ArmEnable_Request)();

// already included above
// #include "rosidl_runtime_c/message_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "kuka_lwr4plus_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_kuka_lwr4plus_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, kuka_lwr4plus_msgs, srv, ArmEnable_Response)();

#include "rosidl_runtime_c/service_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "kuka_lwr4plus_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_kuka_lwr4plus_msgs
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_c, kuka_lwr4plus_msgs, srv, ArmEnable)();

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
