// generated from rosidl_typesupport_introspection_cpp/resource/idl__rosidl_typesupport_introspection_cpp.h.em
// with input from kuka_lwr4plus_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_HPP_


#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

// TODO(dirk-thomas) these visibility macros should be message package specific
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, kuka_lwr4plus_msgs, msg, ArmStatus)();

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_HPP_
