// generated from rosidl_generator_c/resource/idl.h.em
// with input from kuka_lwr4plus_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__SRV__ARM_ENABLE_H_
#define KUKA_LWR4PLUS_MSGS__SRV__ARM_ENABLE_H_

#include "kuka_lwr4plus_msgs/srv/detail/arm_enable__struct.h"
#include "kuka_lwr4plus_msgs/srv/detail/arm_enable__functions.h"
#include "kuka_lwr4plus_msgs/srv/detail/arm_enable__type_support.h"

#endif  // KUKA_LWR4PLUS_MSGS__SRV__ARM_ENABLE_H_
