// generated from rosidl_generator_c/resource/idl.h.em
// with input from kuka_lwr4plus_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__ARM_COMMAND_H_
#define KUKA_LWR4PLUS_MSGS__MSG__ARM_COMMAND_H_

#include "kuka_lwr4plus_msgs/msg/detail/arm_command__struct.h"
#include "kuka_lwr4plus_msgs/msg/detail/arm_command__functions.h"
#include "kuka_lwr4plus_msgs/msg/detail/arm_command__type_support.h"

#endif  // KUKA_LWR4PLUS_MSGS__MSG__ARM_COMMAND_H_
