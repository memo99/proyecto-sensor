// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from kuka_lwr4plus_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice
#include "kuka_lwr4plus_msgs/msg/detail/arm_command__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `angle`
// Member `kp`
// Member `velocity`
// Member `stiffness`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
kuka_lwr4plus_msgs__msg__ArmCommand__init(kuka_lwr4plus_msgs__msg__ArmCommand * msg)
{
  if (!msg) {
    return false;
  }
  // enable
  // angle
  if (!rosidl_runtime_c__float__Sequence__init(&msg->angle, 0)) {
    kuka_lwr4plus_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  // kp
  if (!rosidl_runtime_c__float__Sequence__init(&msg->kp, 0)) {
    kuka_lwr4plus_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  // velocity
  if (!rosidl_runtime_c__float__Sequence__init(&msg->velocity, 0)) {
    kuka_lwr4plus_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  // stiffness
  if (!rosidl_runtime_c__float__Sequence__init(&msg->stiffness, 0)) {
    kuka_lwr4plus_msgs__msg__ArmCommand__fini(msg);
    return false;
  }
  return true;
}

void
kuka_lwr4plus_msgs__msg__ArmCommand__fini(kuka_lwr4plus_msgs__msg__ArmCommand * msg)
{
  if (!msg) {
    return;
  }
  // enable
  // angle
  rosidl_runtime_c__float__Sequence__fini(&msg->angle);
  // kp
  rosidl_runtime_c__float__Sequence__fini(&msg->kp);
  // velocity
  rosidl_runtime_c__float__Sequence__fini(&msg->velocity);
  // stiffness
  rosidl_runtime_c__float__Sequence__fini(&msg->stiffness);
}

kuka_lwr4plus_msgs__msg__ArmCommand *
kuka_lwr4plus_msgs__msg__ArmCommand__create()
{
  kuka_lwr4plus_msgs__msg__ArmCommand * msg = (kuka_lwr4plus_msgs__msg__ArmCommand *)malloc(sizeof(kuka_lwr4plus_msgs__msg__ArmCommand));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(kuka_lwr4plus_msgs__msg__ArmCommand));
  bool success = kuka_lwr4plus_msgs__msg__ArmCommand__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
kuka_lwr4plus_msgs__msg__ArmCommand__destroy(kuka_lwr4plus_msgs__msg__ArmCommand * msg)
{
  if (msg) {
    kuka_lwr4plus_msgs__msg__ArmCommand__fini(msg);
  }
  free(msg);
}


bool
kuka_lwr4plus_msgs__msg__ArmCommand__Sequence__init(kuka_lwr4plus_msgs__msg__ArmCommand__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  kuka_lwr4plus_msgs__msg__ArmCommand * data = NULL;
  if (size) {
    data = (kuka_lwr4plus_msgs__msg__ArmCommand *)calloc(size, sizeof(kuka_lwr4plus_msgs__msg__ArmCommand));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = kuka_lwr4plus_msgs__msg__ArmCommand__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        kuka_lwr4plus_msgs__msg__ArmCommand__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
kuka_lwr4plus_msgs__msg__ArmCommand__Sequence__fini(kuka_lwr4plus_msgs__msg__ArmCommand__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      kuka_lwr4plus_msgs__msg__ArmCommand__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

kuka_lwr4plus_msgs__msg__ArmCommand__Sequence *
kuka_lwr4plus_msgs__msg__ArmCommand__Sequence__create(size_t size)
{
  kuka_lwr4plus_msgs__msg__ArmCommand__Sequence * array = (kuka_lwr4plus_msgs__msg__ArmCommand__Sequence *)malloc(sizeof(kuka_lwr4plus_msgs__msg__ArmCommand__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = kuka_lwr4plus_msgs__msg__ArmCommand__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
kuka_lwr4plus_msgs__msg__ArmCommand__Sequence__destroy(kuka_lwr4plus_msgs__msg__ArmCommand__Sequence * array)
{
  if (array) {
    kuka_lwr4plus_msgs__msg__ArmCommand__Sequence__fini(array);
  }
  free(array);
}
