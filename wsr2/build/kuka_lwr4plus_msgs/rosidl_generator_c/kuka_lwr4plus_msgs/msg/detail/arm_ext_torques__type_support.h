// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from kuka_lwr4plus_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__TYPE_SUPPORT_H_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "kuka_lwr4plus_msgs/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  kuka_lwr4plus_msgs,
  msg,
  ArmExtTorques
)();

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__TYPE_SUPPORT_H_
