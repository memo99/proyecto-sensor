// generated from rosidl_typesupport_introspection_cpp/resource/idl__rosidl_typesupport_introspection_cpp.h.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_HPP_


#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

// TODO(dirk-thomas) these visibility macros should be message package specific
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, arcos_lab_mobile_platform_msgs, srv, MobEnable_Request)();

#ifdef __cplusplus
}
#endif

// already included above
// #include "rosidl_runtime_c/message_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "rosidl_typesupport_introspection_cpp/visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

// TODO(dirk-thomas) these visibility macros should be message package specific
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, arcos_lab_mobile_platform_msgs, srv, MobEnable_Response)();

#ifdef __cplusplus
}
#endif

#include "rosidl_runtime_c/service_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "rosidl_typesupport_introspection_cpp/visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_service_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, arcos_lab_mobile_platform_msgs, srv, MobEnable)();

#ifdef __cplusplus
}
#endif

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_HPP_
