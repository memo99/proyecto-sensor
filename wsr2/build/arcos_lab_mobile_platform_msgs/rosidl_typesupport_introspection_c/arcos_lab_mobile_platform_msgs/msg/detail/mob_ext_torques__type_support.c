// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobExtTorques.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_ext_torques__rosidl_typesupport_introspection_c.h"
#include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_ext_torques__functions.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_ext_torques__struct.h"


// Include directives for member types
// Member `torque`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  arcos_lab_mobile_platform_msgs__msg__MobExtTorques__init(message_memory);
}

void MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_fini_function(void * message_memory)
{
  arcos_lab_mobile_platform_msgs__msg__MobExtTorques__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_member_array[1] = {
  {
    "torque",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs__msg__MobExtTorques, torque),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_members = {
  "arcos_lab_mobile_platform_msgs__msg",  // message namespace
  "MobExtTorques",  // message name
  1,  // number of fields
  sizeof(arcos_lab_mobile_platform_msgs__msg__MobExtTorques),
  MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_member_array,  // message members
  MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_init_function,  // function to initialize message memory (memory has to be allocated)
  MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_type_support_handle = {
  0,
  &MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_arcos_lab_mobile_platform_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, arcos_lab_mobile_platform_msgs, msg, MobExtTorques)() {
  if (!MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_type_support_handle.typesupport_identifier) {
    MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &MobExtTorques__rosidl_typesupport_introspection_c__MobExtTorques_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
