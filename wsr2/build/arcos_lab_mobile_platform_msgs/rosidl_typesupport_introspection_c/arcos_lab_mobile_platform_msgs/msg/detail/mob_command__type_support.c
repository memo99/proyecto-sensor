// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobCommand.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__rosidl_typesupport_introspection_c.h"
#include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__functions.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__struct.h"


// Include directives for member types
// Member `angle`
// Member `kp`
// Member `velocity`
// Member `stiffness`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void MobCommand__rosidl_typesupport_introspection_c__MobCommand_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  arcos_lab_mobile_platform_msgs__msg__MobCommand__init(message_memory);
}

void MobCommand__rosidl_typesupport_introspection_c__MobCommand_fini_function(void * message_memory)
{
  arcos_lab_mobile_platform_msgs__msg__MobCommand__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_member_array[5] = {
  {
    "enable",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs__msg__MobCommand, enable),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "angle",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs__msg__MobCommand, angle),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "kp",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs__msg__MobCommand, kp),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "velocity",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs__msg__MobCommand, velocity),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "stiffness",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs__msg__MobCommand, stiffness),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_members = {
  "arcos_lab_mobile_platform_msgs__msg",  // message namespace
  "MobCommand",  // message name
  5,  // number of fields
  sizeof(arcos_lab_mobile_platform_msgs__msg__MobCommand),
  MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_member_array,  // message members
  MobCommand__rosidl_typesupport_introspection_c__MobCommand_init_function,  // function to initialize message memory (memory has to be allocated)
  MobCommand__rosidl_typesupport_introspection_c__MobCommand_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_type_support_handle = {
  0,
  &MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_arcos_lab_mobile_platform_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, arcos_lab_mobile_platform_msgs, msg, MobCommand)() {
  if (!MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_type_support_handle.typesupport_identifier) {
    MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &MobCommand__rosidl_typesupport_introspection_c__MobCommand_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
