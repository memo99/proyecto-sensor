#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "arcos_lab_mobile_platform_msgs::arcos_lab_mobile_platform_msgs__rosidl_typesupport_c" for configuration ""
set_property(TARGET arcos_lab_mobile_platform_msgs::arcos_lab_mobile_platform_msgs__rosidl_typesupport_c APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(arcos_lab_mobile_platform_msgs::arcos_lab_mobile_platform_msgs__rosidl_typesupport_c PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libarcos_lab_mobile_platform_msgs__rosidl_typesupport_c.so"
  IMPORTED_SONAME_NOCONFIG "libarcos_lab_mobile_platform_msgs__rosidl_typesupport_c.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS arcos_lab_mobile_platform_msgs::arcos_lab_mobile_platform_msgs__rosidl_typesupport_c )
list(APPEND _IMPORT_CHECK_FILES_FOR_arcos_lab_mobile_platform_msgs::arcos_lab_mobile_platform_msgs__rosidl_typesupport_c "${_IMPORT_PREFIX}/lib/libarcos_lab_mobile_platform_msgs__rosidl_typesupport_c.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
