// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__STRUCT_H_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'enable'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/MobEnable in the package arcos_lab_mobile_platform_msgs.
typedef struct arcos_lab_mobile_platform_msgs__srv__MobEnable_Request
{
  rosidl_runtime_c__float__Sequence enable;
} arcos_lab_mobile_platform_msgs__srv__MobEnable_Request;

// Struct for a sequence of arcos_lab_mobile_platform_msgs__srv__MobEnable_Request.
typedef struct arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence
{
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence;


// Constants defined in the message

// Include directives for member types
// Member 'enabled'
// already included above
// #include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/MobEnable in the package arcos_lab_mobile_platform_msgs.
typedef struct arcos_lab_mobile_platform_msgs__srv__MobEnable_Response
{
  rosidl_runtime_c__float__Sequence enabled;
} arcos_lab_mobile_platform_msgs__srv__MobEnable_Response;

// Struct for a sequence of arcos_lab_mobile_platform_msgs__srv__MobEnable_Response.
typedef struct arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence
{
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__STRUCT_H_
