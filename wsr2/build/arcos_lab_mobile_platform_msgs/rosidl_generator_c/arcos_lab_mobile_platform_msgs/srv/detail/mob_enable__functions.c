// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice
#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// Include directives for member types
// Member `enable`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * msg)
{
  if (!msg) {
    return false;
  }
  // enable
  if (!rosidl_runtime_c__float__Sequence__init(&msg->enable, 0)) {
    arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini(msg);
    return false;
  }
  return true;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * msg)
{
  if (!msg) {
    return;
  }
  // enable
  rosidl_runtime_c__float__Sequence__fini(&msg->enable);
}

arcos_lab_mobile_platform_msgs__srv__MobEnable_Request *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__create()
{
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * msg = (arcos_lab_mobile_platform_msgs__srv__MobEnable_Request *)malloc(sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request));
  bool success = arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * msg)
{
  if (msg) {
    arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini(msg);
  }
  free(msg);
}


bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * data = NULL;
  if (size) {
    data = (arcos_lab_mobile_platform_msgs__srv__MobEnable_Request *)calloc(size, sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__create(size_t size)
{
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence * array = (arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence *)malloc(sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence * array)
{
  if (array) {
    arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__fini(array);
  }
  free(array);
}


// Include directives for member types
// Member `enabled`
// already included above
// #include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * msg)
{
  if (!msg) {
    return false;
  }
  // enabled
  if (!rosidl_runtime_c__float__Sequence__init(&msg->enabled, 0)) {
    arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini(msg);
    return false;
  }
  return true;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * msg)
{
  if (!msg) {
    return;
  }
  // enabled
  rosidl_runtime_c__float__Sequence__fini(&msg->enabled);
}

arcos_lab_mobile_platform_msgs__srv__MobEnable_Response *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__create()
{
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * msg = (arcos_lab_mobile_platform_msgs__srv__MobEnable_Response *)malloc(sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response));
  bool success = arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * msg)
{
  if (msg) {
    arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini(msg);
  }
  free(msg);
}


bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * data = NULL;
  if (size) {
    data = (arcos_lab_mobile_platform_msgs__srv__MobEnable_Response *)calloc(size, sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__create(size_t size)
{
  arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence * array = (arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence *)malloc(sizeof(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence * array)
{
  if (array) {
    arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__fini(array);
  }
  free(array);
}
