// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobCommand.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__STRUCT_H_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'angle'
// Member 'kp'
// Member 'velocity'
// Member 'stiffness'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in msg/MobCommand in the package arcos_lab_mobile_platform_msgs.
typedef struct arcos_lab_mobile_platform_msgs__msg__MobCommand
{
  bool enable;
  rosidl_runtime_c__float__Sequence angle;
  rosidl_runtime_c__float__Sequence kp;
  rosidl_runtime_c__float__Sequence velocity;
  rosidl_runtime_c__float__Sequence stiffness;
} arcos_lab_mobile_platform_msgs__msg__MobCommand;

// Struct for a sequence of arcos_lab_mobile_platform_msgs__msg__MobCommand.
typedef struct arcos_lab_mobile_platform_msgs__msg__MobCommand__Sequence
{
  arcos_lab_mobile_platform_msgs__msg__MobCommand * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} arcos_lab_mobile_platform_msgs__msg__MobCommand__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__STRUCT_H_
