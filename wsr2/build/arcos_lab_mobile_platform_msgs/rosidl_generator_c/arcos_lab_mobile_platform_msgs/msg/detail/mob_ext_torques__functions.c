// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobExtTorques.idl
// generated code does not contain a copyright notice
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_ext_torques__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `torque`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__init(arcos_lab_mobile_platform_msgs__msg__MobExtTorques * msg)
{
  if (!msg) {
    return false;
  }
  // torque
  if (!rosidl_runtime_c__float__Sequence__init(&msg->torque, 0)) {
    arcos_lab_mobile_platform_msgs__msg__MobExtTorques__fini(msg);
    return false;
  }
  return true;
}

void
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__fini(arcos_lab_mobile_platform_msgs__msg__MobExtTorques * msg)
{
  if (!msg) {
    return;
  }
  // torque
  rosidl_runtime_c__float__Sequence__fini(&msg->torque);
}

arcos_lab_mobile_platform_msgs__msg__MobExtTorques *
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__create()
{
  arcos_lab_mobile_platform_msgs__msg__MobExtTorques * msg = (arcos_lab_mobile_platform_msgs__msg__MobExtTorques *)malloc(sizeof(arcos_lab_mobile_platform_msgs__msg__MobExtTorques));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(arcos_lab_mobile_platform_msgs__msg__MobExtTorques));
  bool success = arcos_lab_mobile_platform_msgs__msg__MobExtTorques__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__destroy(arcos_lab_mobile_platform_msgs__msg__MobExtTorques * msg)
{
  if (msg) {
    arcos_lab_mobile_platform_msgs__msg__MobExtTorques__fini(msg);
  }
  free(msg);
}


bool
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence__init(arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  arcos_lab_mobile_platform_msgs__msg__MobExtTorques * data = NULL;
  if (size) {
    data = (arcos_lab_mobile_platform_msgs__msg__MobExtTorques *)calloc(size, sizeof(arcos_lab_mobile_platform_msgs__msg__MobExtTorques));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = arcos_lab_mobile_platform_msgs__msg__MobExtTorques__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        arcos_lab_mobile_platform_msgs__msg__MobExtTorques__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence__fini(arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      arcos_lab_mobile_platform_msgs__msg__MobExtTorques__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence *
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence__create(size_t size)
{
  arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence * array = (arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence *)malloc(sizeof(arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence__destroy(arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence * array)
{
  if (array) {
    arcos_lab_mobile_platform_msgs__msg__MobExtTorques__Sequence__fini(array);
  }
  free(array);
}
