// generated from rosidl_generator_c/resource/idl__functions.h.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobStatus.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__FUNCTIONS_H_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_runtime_c/visibility_control.h"
#include "arcos_lab_mobile_platform_msgs/msg/rosidl_generator_c__visibility_control.h"

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__struct.h"

/// Initialize msg/MobStatus message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * arcos_lab_mobile_platform_msgs__msg__MobStatus
 * )) before or use
 * arcos_lab_mobile_platform_msgs__msg__MobStatus__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
bool
arcos_lab_mobile_platform_msgs__msg__MobStatus__init(arcos_lab_mobile_platform_msgs__msg__MobStatus * msg);

/// Finalize msg/MobStatus message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__msg__MobStatus__fini(arcos_lab_mobile_platform_msgs__msg__MobStatus * msg);

/// Create msg/MobStatus message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * arcos_lab_mobile_platform_msgs__msg__MobStatus__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
arcos_lab_mobile_platform_msgs__msg__MobStatus *
arcos_lab_mobile_platform_msgs__msg__MobStatus__create();

/// Destroy msg/MobStatus message.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__msg__MobStatus__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__msg__MobStatus__destroy(arcos_lab_mobile_platform_msgs__msg__MobStatus * msg);


/// Initialize array of msg/MobStatus messages.
/**
 * It allocates the memory for the number of elements and calls
 * arcos_lab_mobile_platform_msgs__msg__MobStatus__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
bool
arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence__init(arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence * array, size_t size);

/// Finalize array of msg/MobStatus messages.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__msg__MobStatus__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence__fini(arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence * array);

/// Create array of msg/MobStatus messages.
/**
 * It allocates the memory for the array and calls
 * arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence *
arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence__create(size_t size);

/// Destroy array of msg/MobStatus messages.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence__destroy(arcos_lab_mobile_platform_msgs__msg__MobStatus__Sequence * array);

#ifdef __cplusplus
}
#endif

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__FUNCTIONS_H_
