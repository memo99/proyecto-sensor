// generated from rosidl_generator_c/resource/idl.h.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobCommand.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_COMMAND_H_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_COMMAND_H_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__struct.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__functions.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__type_support.h"

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_COMMAND_H_
