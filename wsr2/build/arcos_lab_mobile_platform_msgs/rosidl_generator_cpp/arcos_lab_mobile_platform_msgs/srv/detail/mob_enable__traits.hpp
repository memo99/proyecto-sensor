// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__TRAITS_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__TRAITS_HPP_

#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>()
{
  return "arcos_lab_mobile_platform_msgs::srv::MobEnable_Request";
}

template<>
inline const char * name<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>()
{
  return "arcos_lab_mobile_platform_msgs/srv/MobEnable_Request";
}

template<>
struct has_fixed_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>()
{
  return "arcos_lab_mobile_platform_msgs::srv::MobEnable_Response";
}

template<>
inline const char * name<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>()
{
  return "arcos_lab_mobile_platform_msgs/srv/MobEnable_Response";
}

template<>
struct has_fixed_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<arcos_lab_mobile_platform_msgs::srv::MobEnable>()
{
  return "arcos_lab_mobile_platform_msgs::srv::MobEnable";
}

template<>
inline const char * name<arcos_lab_mobile_platform_msgs::srv::MobEnable>()
{
  return "arcos_lab_mobile_platform_msgs/srv/MobEnable";
}

template<>
struct has_fixed_size<arcos_lab_mobile_platform_msgs::srv::MobEnable>
  : std::integral_constant<
    bool,
    has_fixed_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>::value &&
    has_fixed_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>::value
  >
{
};

template<>
struct has_bounded_size<arcos_lab_mobile_platform_msgs::srv::MobEnable>
  : std::integral_constant<
    bool,
    has_bounded_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>::value &&
    has_bounded_size<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>::value
  >
{
};

template<>
struct is_service<arcos_lab_mobile_platform_msgs::srv::MobEnable>
  : std::true_type
{
};

template<>
struct is_service_request<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>
  : std::true_type
{
};

template<>
struct is_service_response<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>
  : std::true_type
{
};

}  // namespace rosidl_generator_traits

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__TRAITS_HPP_
