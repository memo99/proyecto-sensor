// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__STRUCT_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Request __attribute__((deprecated))
#else
# define DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Request __declspec(deprecated)
#endif

namespace arcos_lab_mobile_platform_msgs
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct MobEnable_Request_
{
  using Type = MobEnable_Request_<ContainerAllocator>;

  explicit MobEnable_Request_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit MobEnable_Request_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _enable_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _enable_type enable;

  // setters for named parameter idiom
  Type & set__enable(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->enable = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator> *;
  using ConstRawPtr =
    const arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Request
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Request
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const MobEnable_Request_ & other) const
  {
    if (this->enable != other.enable) {
      return false;
    }
    return true;
  }
  bool operator!=(const MobEnable_Request_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct MobEnable_Request_

// alias to use template instance with default allocator
using MobEnable_Request =
  arcos_lab_mobile_platform_msgs::srv::MobEnable_Request_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace arcos_lab_mobile_platform_msgs


#ifndef _WIN32
# define DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Response __attribute__((deprecated))
#else
# define DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Response __declspec(deprecated)
#endif

namespace arcos_lab_mobile_platform_msgs
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct MobEnable_Response_
{
  using Type = MobEnable_Response_<ContainerAllocator>;

  explicit MobEnable_Response_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit MobEnable_Response_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _enabled_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _enabled_type enabled;

  // setters for named parameter idiom
  Type & set__enabled(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->enabled = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator> *;
  using ConstRawPtr =
    const arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Response
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__srv__MobEnable_Response
    std::shared_ptr<arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const MobEnable_Response_ & other) const
  {
    if (this->enabled != other.enabled) {
      return false;
    }
    return true;
  }
  bool operator!=(const MobEnable_Response_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct MobEnable_Response_

// alias to use template instance with default allocator
using MobEnable_Response =
  arcos_lab_mobile_platform_msgs::srv::MobEnable_Response_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace arcos_lab_mobile_platform_msgs

namespace arcos_lab_mobile_platform_msgs
{

namespace srv
{

struct MobEnable
{
  using Request = arcos_lab_mobile_platform_msgs::srv::MobEnable_Request;
  using Response = arcos_lab_mobile_platform_msgs::srv::MobEnable_Response;
};

}  // namespace srv

}  // namespace arcos_lab_mobile_platform_msgs

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__STRUCT_HPP_
