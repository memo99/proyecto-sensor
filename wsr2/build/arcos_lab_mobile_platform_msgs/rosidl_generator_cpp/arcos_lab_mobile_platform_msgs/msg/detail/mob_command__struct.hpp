// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobCommand.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__STRUCT_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobCommand __attribute__((deprecated))
#else
# define DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobCommand __declspec(deprecated)
#endif

namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct MobCommand_
{
  using Type = MobCommand_<ContainerAllocator>;

  explicit MobCommand_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->enable = false;
    }
  }

  explicit MobCommand_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->enable = false;
    }
  }

  // field types and members
  using _enable_type =
    bool;
  _enable_type enable;
  using _angle_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _angle_type angle;
  using _kp_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _kp_type kp;
  using _velocity_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _velocity_type velocity;
  using _stiffness_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _stiffness_type stiffness;

  // setters for named parameter idiom
  Type & set__enable(
    const bool & _arg)
  {
    this->enable = _arg;
    return *this;
  }
  Type & set__angle(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->angle = _arg;
    return *this;
  }
  Type & set__kp(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->kp = _arg;
    return *this;
  }
  Type & set__velocity(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->velocity = _arg;
    return *this;
  }
  Type & set__stiffness(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->stiffness = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator> *;
  using ConstRawPtr =
    const arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobCommand
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobCommand
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobCommand_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const MobCommand_ & other) const
  {
    if (this->enable != other.enable) {
      return false;
    }
    if (this->angle != other.angle) {
      return false;
    }
    if (this->kp != other.kp) {
      return false;
    }
    if (this->velocity != other.velocity) {
      return false;
    }
    if (this->stiffness != other.stiffness) {
      return false;
    }
    return true;
  }
  bool operator!=(const MobCommand_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct MobCommand_

// alias to use template instance with default allocator
using MobCommand =
  arcos_lab_mobile_platform_msgs::msg::MobCommand_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace arcos_lab_mobile_platform_msgs

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__STRUCT_HPP_
