// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobExtTorques.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__STRUCT_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobExtTorques __attribute__((deprecated))
#else
# define DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobExtTorques __declspec(deprecated)
#endif

namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct MobExtTorques_
{
  using Type = MobExtTorques_<ContainerAllocator>;

  explicit MobExtTorques_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit MobExtTorques_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _torque_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _torque_type torque;

  // setters for named parameter idiom
  Type & set__torque(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->torque = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator> *;
  using ConstRawPtr =
    const arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobExtTorques
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__arcos_lab_mobile_platform_msgs__msg__MobExtTorques
    std::shared_ptr<arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const MobExtTorques_ & other) const
  {
    if (this->torque != other.torque) {
      return false;
    }
    return true;
  }
  bool operator!=(const MobExtTorques_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct MobExtTorques_

// alias to use template instance with default allocator
using MobExtTorques =
  arcos_lab_mobile_platform_msgs::msg::MobExtTorques_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace arcos_lab_mobile_platform_msgs

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__STRUCT_HPP_
