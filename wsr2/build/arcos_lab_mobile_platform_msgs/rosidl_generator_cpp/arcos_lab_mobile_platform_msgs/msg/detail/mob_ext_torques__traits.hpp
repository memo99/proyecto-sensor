// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobExtTorques.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__TRAITS_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__TRAITS_HPP_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_ext_torques__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<arcos_lab_mobile_platform_msgs::msg::MobExtTorques>()
{
  return "arcos_lab_mobile_platform_msgs::msg::MobExtTorques";
}

template<>
inline const char * name<arcos_lab_mobile_platform_msgs::msg::MobExtTorques>()
{
  return "arcos_lab_mobile_platform_msgs/msg/MobExtTorques";
}

template<>
struct has_fixed_size<arcos_lab_mobile_platform_msgs::msg::MobExtTorques>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<arcos_lab_mobile_platform_msgs::msg::MobExtTorques>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<arcos_lab_mobile_platform_msgs::msg::MobExtTorques>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__TRAITS_HPP_
