// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_COMMAND_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_COMMAND_HPP_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__struct.hpp"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__builder.hpp"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__traits.hpp"

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_COMMAND_HPP_
