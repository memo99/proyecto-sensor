// generated from rosidl_typesupport_fastrtps_c/resource/idl__type_support_c.cpp.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice
#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__rosidl_typesupport_fastrtps_c.h"


#include <cassert>
#include <limits>
#include <string>
#include "rosidl_typesupport_fastrtps_c/identifier.h"
#include "rosidl_typesupport_fastrtps_c/wstring_conversion.hpp"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
#include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"
#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__struct.h"
#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__functions.h"
#include "fastcdr/Cdr.h"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif
#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

// includes and forward declarations of message dependencies and their conversion functions

#if defined(__cplusplus)
extern "C"
{
#endif

#include "rosidl_runtime_c/primitives_sequence.h"  // enable
#include "rosidl_runtime_c/primitives_sequence_functions.h"  // enable

// forward declare type support functions


using _MobEnable_Request__ros_msg_type = arcos_lab_mobile_platform_msgs__srv__MobEnable_Request;

static bool _MobEnable_Request__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  const _MobEnable_Request__ros_msg_type * ros_message = static_cast<const _MobEnable_Request__ros_msg_type *>(untyped_ros_message);
  // Field name: enable
  {
    size_t size = ros_message->enable.size;
    auto array_ptr = ros_message->enable.data;
    cdr << static_cast<uint32_t>(size);
    cdr.serializeArray(array_ptr, size);
  }

  return true;
}

static bool _MobEnable_Request__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  _MobEnable_Request__ros_msg_type * ros_message = static_cast<_MobEnable_Request__ros_msg_type *>(untyped_ros_message);
  // Field name: enable
  {
    uint32_t cdrSize;
    cdr >> cdrSize;
    size_t size = static_cast<size_t>(cdrSize);
    if (ros_message->enable.data) {
      rosidl_runtime_c__float__Sequence__fini(&ros_message->enable);
    }
    if (!rosidl_runtime_c__float__Sequence__init(&ros_message->enable, size)) {
      return "failed to create array for field 'enable'";
    }
    auto array_ptr = ros_message->enable.data;
    cdr.deserializeArray(array_ptr, size);
  }

  return true;
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t get_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Request(
  const void * untyped_ros_message,
  size_t current_alignment)
{
  const _MobEnable_Request__ros_msg_type * ros_message = static_cast<const _MobEnable_Request__ros_msg_type *>(untyped_ros_message);
  (void)ros_message;
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // field.name enable
  {
    size_t array_size = ros_message->enable.size;
    auto array_ptr = ros_message->enable.data;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);
    (void)array_ptr;
    size_t item_size = sizeof(array_ptr[0]);
    current_alignment += array_size * item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

static uint32_t _MobEnable_Request__get_serialized_size(const void * untyped_ros_message)
{
  return static_cast<uint32_t>(
    get_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Request(
      untyped_ros_message, 0));
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t max_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Request(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;

  // member: enable
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  return current_alignment - initial_alignment;
}

static size_t _MobEnable_Request__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Request(
    full_bounded, 0);
}


static message_type_support_callbacks_t __callbacks_MobEnable_Request = {
  "arcos_lab_mobile_platform_msgs::srv",
  "MobEnable_Request",
  _MobEnable_Request__cdr_serialize,
  _MobEnable_Request__cdr_deserialize,
  _MobEnable_Request__get_serialized_size,
  _MobEnable_Request__max_serialized_size
};

static rosidl_message_type_support_t _MobEnable_Request__type_support = {
  rosidl_typesupport_fastrtps_c__identifier,
  &__callbacks_MobEnable_Request,
  get_message_typesupport_handle_function,
};

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable_Request)() {
  return &_MobEnable_Request__type_support;
}

#if defined(__cplusplus)
}
#endif

// already included above
// #include <cassert>
// already included above
// #include <limits>
// already included above
// #include <string>
// already included above
// #include "rosidl_typesupport_fastrtps_c/identifier.h"
// already included above
// #include "rosidl_typesupport_fastrtps_c/wstring_conversion.hpp"
// already included above
// #include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
// already included above
// #include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"
// already included above
// #include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__struct.h"
// already included above
// #include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__functions.h"
// already included above
// #include "fastcdr/Cdr.h"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif
#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

// includes and forward declarations of message dependencies and their conversion functions

#if defined(__cplusplus)
extern "C"
{
#endif

// already included above
// #include "rosidl_runtime_c/primitives_sequence.h"  // enabled
// already included above
// #include "rosidl_runtime_c/primitives_sequence_functions.h"  // enabled

// forward declare type support functions


using _MobEnable_Response__ros_msg_type = arcos_lab_mobile_platform_msgs__srv__MobEnable_Response;

static bool _MobEnable_Response__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  const _MobEnable_Response__ros_msg_type * ros_message = static_cast<const _MobEnable_Response__ros_msg_type *>(untyped_ros_message);
  // Field name: enabled
  {
    size_t size = ros_message->enabled.size;
    auto array_ptr = ros_message->enabled.data;
    cdr << static_cast<uint32_t>(size);
    cdr.serializeArray(array_ptr, size);
  }

  return true;
}

static bool _MobEnable_Response__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  _MobEnable_Response__ros_msg_type * ros_message = static_cast<_MobEnable_Response__ros_msg_type *>(untyped_ros_message);
  // Field name: enabled
  {
    uint32_t cdrSize;
    cdr >> cdrSize;
    size_t size = static_cast<size_t>(cdrSize);
    if (ros_message->enabled.data) {
      rosidl_runtime_c__float__Sequence__fini(&ros_message->enabled);
    }
    if (!rosidl_runtime_c__float__Sequence__init(&ros_message->enabled, size)) {
      return "failed to create array for field 'enabled'";
    }
    auto array_ptr = ros_message->enabled.data;
    cdr.deserializeArray(array_ptr, size);
  }

  return true;
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t get_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Response(
  const void * untyped_ros_message,
  size_t current_alignment)
{
  const _MobEnable_Response__ros_msg_type * ros_message = static_cast<const _MobEnable_Response__ros_msg_type *>(untyped_ros_message);
  (void)ros_message;
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // field.name enabled
  {
    size_t array_size = ros_message->enabled.size;
    auto array_ptr = ros_message->enabled.data;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);
    (void)array_ptr;
    size_t item_size = sizeof(array_ptr[0]);
    current_alignment += array_size * item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

static uint32_t _MobEnable_Response__get_serialized_size(const void * untyped_ros_message)
{
  return static_cast<uint32_t>(
    get_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Response(
      untyped_ros_message, 0));
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t max_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Response(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;

  // member: enabled
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  return current_alignment - initial_alignment;
}

static size_t _MobEnable_Response__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Response(
    full_bounded, 0);
}


static message_type_support_callbacks_t __callbacks_MobEnable_Response = {
  "arcos_lab_mobile_platform_msgs::srv",
  "MobEnable_Response",
  _MobEnable_Response__cdr_serialize,
  _MobEnable_Response__cdr_deserialize,
  _MobEnable_Response__get_serialized_size,
  _MobEnable_Response__max_serialized_size
};

static rosidl_message_type_support_t _MobEnable_Response__type_support = {
  rosidl_typesupport_fastrtps_c__identifier,
  &__callbacks_MobEnable_Response,
  get_message_typesupport_handle_function,
};

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable_Response)() {
  return &_MobEnable_Response__type_support;
}

#if defined(__cplusplus)
}
#endif

#include "rosidl_typesupport_fastrtps_cpp/service_type_support.h"
#include "rosidl_typesupport_cpp/service_type_support.hpp"
// already included above
// #include "rosidl_typesupport_fastrtps_c/identifier.h"
// already included above
// #include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"
#include "arcos_lab_mobile_platform_msgs/srv/mob_enable.h"

#if defined(__cplusplus)
extern "C"
{
#endif

static service_type_support_callbacks_t MobEnable__callbacks = {
  "arcos_lab_mobile_platform_msgs::srv",
  "MobEnable",
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable_Request)(),
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable_Response)(),
};

static rosidl_service_type_support_t MobEnable__handle = {
  rosidl_typesupport_fastrtps_c__identifier,
  &MobEnable__callbacks,
  get_service_typesupport_handle_function,
};

const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable)() {
  return &MobEnable__handle;
}

#if defined(__cplusplus)
}
#endif
