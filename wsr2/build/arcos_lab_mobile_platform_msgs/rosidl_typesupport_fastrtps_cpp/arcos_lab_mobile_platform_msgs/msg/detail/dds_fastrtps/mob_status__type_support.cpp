// generated from rosidl_typesupport_fastrtps_cpp/resource/idl__type_support.cpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobStatus.idl
// generated code does not contain a copyright notice
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__rosidl_typesupport_fastrtps_cpp.hpp"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__struct.hpp"

#include <limits>
#include <stdexcept>
#include <string>
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_fastrtps_cpp/identifier.hpp"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_fastrtps_cpp/wstring_conversion.hpp"
#include "fastcdr/Cdr.h"


// forward declaration of message dependencies and their conversion functions

namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
cdr_serialize(
  const arcos_lab_mobile_platform_msgs::msg::MobStatus & ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  // Member: enabled
  cdr << (ros_message.enabled ? true : false);
  // Member: position
  {
    cdr << ros_message.position;
  }
  // Member: torque
  {
    cdr << ros_message.torque;
  }
  return true;
}

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  arcos_lab_mobile_platform_msgs::msg::MobStatus & ros_message)
{
  // Member: enabled
  {
    uint8_t tmp;
    cdr >> tmp;
    ros_message.enabled = tmp ? true : false;
  }

  // Member: position
  {
    cdr >> ros_message.position;
  }

  // Member: torque
  {
    cdr >> ros_message.torque;
  }

  return true;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
get_serialized_size(
  const arcos_lab_mobile_platform_msgs::msg::MobStatus & ros_message,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // Member: enabled
  {
    size_t item_size = sizeof(ros_message.enabled);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // Member: position
  {
    size_t array_size = ros_message.position.size();

    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);
    size_t item_size = sizeof(ros_message.position[0]);
    current_alignment += array_size * item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // Member: torque
  {
    size_t array_size = ros_message.torque.size();

    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);
    size_t item_size = sizeof(ros_message.torque[0]);
    current_alignment += array_size * item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
max_serialized_size_MobStatus(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;


  // Member: enabled
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint8_t);
  }

  // Member: position
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  // Member: torque
  {
    size_t array_size = 0;
    full_bounded = false;
    current_alignment += padding +
      eprosima::fastcdr::Cdr::alignment(current_alignment, padding);

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }

  return current_alignment - initial_alignment;
}

static bool _MobStatus__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  auto typed_message =
    static_cast<const arcos_lab_mobile_platform_msgs::msg::MobStatus *>(
    untyped_ros_message);
  return cdr_serialize(*typed_message, cdr);
}

static bool _MobStatus__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  auto typed_message =
    static_cast<arcos_lab_mobile_platform_msgs::msg::MobStatus *>(
    untyped_ros_message);
  return cdr_deserialize(cdr, *typed_message);
}

static uint32_t _MobStatus__get_serialized_size(
  const void * untyped_ros_message)
{
  auto typed_message =
    static_cast<const arcos_lab_mobile_platform_msgs::msg::MobStatus *>(
    untyped_ros_message);
  return static_cast<uint32_t>(get_serialized_size(*typed_message, 0));
}

static size_t _MobStatus__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_MobStatus(full_bounded, 0);
}

static message_type_support_callbacks_t _MobStatus__callbacks = {
  "arcos_lab_mobile_platform_msgs::msg",
  "MobStatus",
  _MobStatus__cdr_serialize,
  _MobStatus__cdr_deserialize,
  _MobStatus__get_serialized_size,
  _MobStatus__max_serialized_size
};

static rosidl_message_type_support_t _MobStatus__handle = {
  rosidl_typesupport_fastrtps_cpp::typesupport_identifier,
  &_MobStatus__callbacks,
  get_message_typesupport_handle_function,
};

}  // namespace typesupport_fastrtps_cpp

}  // namespace msg

}  // namespace arcos_lab_mobile_platform_msgs

namespace rosidl_typesupport_fastrtps_cpp
{

template<>
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_EXPORT_arcos_lab_mobile_platform_msgs
const rosidl_message_type_support_t *
get_message_type_support_handle<arcos_lab_mobile_platform_msgs::msg::MobStatus>()
{
  return &arcos_lab_mobile_platform_msgs::msg::typesupport_fastrtps_cpp::_MobStatus__handle;
}

}  // namespace rosidl_typesupport_fastrtps_cpp

#ifdef __cplusplus
extern "C"
{
#endif

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, arcos_lab_mobile_platform_msgs, msg, MobStatus)() {
  return &arcos_lab_mobile_platform_msgs::msg::typesupport_fastrtps_cpp::_MobStatus__handle;
}

#ifdef __cplusplus
}
#endif
