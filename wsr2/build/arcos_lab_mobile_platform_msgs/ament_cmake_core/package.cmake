set(_AMENT_PACKAGE_NAME "arcos_lab_mobile_platform_msgs")
set(arcos_lab_mobile_platform_msgs_VERSION "0.1.0")
set(arcos_lab_mobile_platform_msgs_MAINTAINER "Federico Ruiz Ugalde <memeruiz@gmail.com>")
set(arcos_lab_mobile_platform_msgs_BUILD_DEPENDS "rosidl_default_generators")
set(arcos_lab_mobile_platform_msgs_BUILDTOOL_DEPENDS "ament_cmake")
set(arcos_lab_mobile_platform_msgs_BUILD_EXPORT_DEPENDS )
set(arcos_lab_mobile_platform_msgs_BUILDTOOL_EXPORT_DEPENDS )
set(arcos_lab_mobile_platform_msgs_EXEC_DEPENDS "rosidl_default_runtime")
set(arcos_lab_mobile_platform_msgs_TEST_DEPENDS "ament_lint_auto" "ament_lint_common")
set(arcos_lab_mobile_platform_msgs_GROUP_DEPENDS )
set(arcos_lab_mobile_platform_msgs_MEMBER_OF_GROUPS "rosidl_interface_packages")
set(arcos_lab_mobile_platform_msgs_DEPRECATED "")
set(arcos_lab_mobile_platform_msgs_EXPORT_TAGS)
list(APPEND arcos_lab_mobile_platform_msgs_EXPORT_TAGS "<build_type>ament_cmake</build_type>")
