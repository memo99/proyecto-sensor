# generated from rosidl_cmake/cmake/rosidl_cmake-extras.cmake.in

set(arcos_lab_mobile_platform_msgs_IDL_FILES "msg/MobExtTorques.idl;msg/MobCommand.idl;msg/MobStatus.idl;srv/MobEnable.idl")
set(arcos_lab_mobile_platform_msgs_INTERFACE_FILES "msg/MobExtTorques.msg;msg/MobCommand.msg;msg/MobStatus.msg;srv/MobEnable.srv;srv/MobEnable_Request.msg;srv/MobEnable_Response.msg")
