#!/usr/bin/env python
import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile

from arcos_lab_mobile_platform_msgs.msg import MobExtTorques

from numpy import pi, array

from functools import reduce


class Send_Ext_Torques(Node):
    def __init__(self, node_name='send_ext_torques'):
        super().__init__(node_name)
        qos_profile = QoSProfile(depth=10)
        self.pub = self.create_publisher(
            MobExtTorques,
            node_name+"/ext_torques",
            qos_profile)
        self.exttorques = self.mobexttorques()

#    rospy.wait_for_service('wessling_hand_enable')
#    rospy.loginfo("Service started")
#
#    client = rospy.ServiceProxy('wessling_hand_enable', enable_hands)
#    enable = [1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0]
# enable = [0.0]*10
#    resp = client.call(enable, len(enable))
#    if ():
#        rospy.loginfo("Finger enabled: " + str(resp.Enabled))
#    else:
#        rospy.logerr("Failed to call service")
#        return 1
    def printi(self, *args, **kwargs):
        self.get_logger().info(str(reduce(lambda x, y: str(x)+" "+str(y), args)))

    def mobexttorques(self, torque=[0.0, 0.0, 0.0]):
        exttorques = MobExtTorques()
        exttorques.torque = torque
        return (exttorques)

    def set_mob_ext_torques(self, torque):
        self.exttorques.torque = torque.tolist()

    def send_exttorques(self):
        self.printi("Yay")
        print(self.exttorques)
        self.pub.publish(self.exttorques)


#

torques = [[1. * pi / 180.0, 2. * pi / 180.0, 1 * pi / 180.0],
           [5. * pi / 180.0, 7. * pi / 180.0, 1 * pi / 180.0],
           [15. * pi / 180.0, 10. * pi / 180.0, 1 * pi / 180.0],
           [-10. * pi / 180.0, -20. * pi / 180.0, 1 * pi / 180.0],
           [0. * pi / 180.0, 0. * pi / 180.0, 0 * pi / 180.0]
           ]

torques = [[0.2, 0., 0 * pi / 180.0,            0.],
           [0.2, 0.2, 0 * pi / 180.0,            0.],
           [0.2, 0.2, 3 * pi / 180.0,            0.],
           [0.2, 0.2, 3 * pi / 180.0,            0.05],
           [0., 0., 0 * pi / 180.0,            0.]
           ]


def main():
    cur_torque_pos = 0
    rclpy.init()
    try:
        send_ext_torques = Send_Ext_Torques()
        rate = send_ext_torques.create_rate(0.5)
        while rclpy.ok():
            cur_torque_pos += 1
            if cur_torque_pos >= len(torques):
                cur_torque_pos = 0
            cur_torque = array(torques[cur_torque_pos])
            send_ext_torques.set_mob_ext_torques(
                cur_torque)

            send_ext_torques.send_exttorques()
            rclpy.spin_once(send_ext_torques)
            rate.sleep()
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
