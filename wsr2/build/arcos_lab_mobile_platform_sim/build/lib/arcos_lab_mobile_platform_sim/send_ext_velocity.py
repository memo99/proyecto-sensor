#!/usr/bin/env python
import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile

from arcos_lab_mobile_platform_msgs.msg import MobCommand

from numpy import pi, array

from functools import reduce

class Send_Ext_Velocity(Node):
    def __init__(self, node_name='send_ext_velocity'):
        super().__init__(node_name)
        qos_profile = QoSProfile(depth=10)
        self.pub = self.create_publisher(
            MobCommand,
            node_name+"/ext_velocity",
            qos_profile)
        self.extvelocities = self.mobextvelocities()

    def printi(self, *args, **kwargs):
        self.get_logger().info(str(reduce(lambda x, y: str(x)+" "+str(y), args)))

    def mobextvelocities(self, velocity=[0.0, 0.0, 0.0]):
        extvelocities = MobCommand()
        extvelocities.velocity = velocity
        return (extvelocities)

    def set_mob_ext_velocities(self, velocity):
        self.extvelocities.velocity = velocity.tolist()

    def send_extvelocities(self):
        self.printi("Vamos")
        print(self.extvelocities)
        self.pub.publish(self.extvelocities)

velocities_wz = [[0.0, 0.0, 0.1, 0.0],
           [0.0, 0.0, 0.1, 0.0],
           [0.0, 0.0, 0.1, 0.0],
           [0.0, 0.0, 0.1, 0.0]
           ] 

velocities_x = [[0.1, 0.0, 0.0, 0.0],
           [0.1, 0.0, 0.0, 0.0],
           [0.1, 0.0, 0.0, 0.0],
           [0.1, 0.0, 0.0, 0.0]
           ] 

velocities_y = [[0.0, 0.1, 0.0],
           [0.0, 0.1, 0.0, 0.0],
           [0.0, 0.1, 0.0, 0.0],
           [0.0, 0.1, 0.0, 0.0]
           ]

velocities_xy = [[0.1, 0.1, 0.0, 0.0],
           [0.1, 0.1, 0.0, 0.0],
           [0.1, 0.1, 0.0, 0.0],
           [0.1, 0.1, 0.0, 0.0]
           ] 

velocities_square = [[0.5, 0.0, 0.0, 0.0],
           [0.0, 0.5, 0.0, 0.0],
           [-0.5, 0.0, 0.0, 0.0],
           [0.0, -0.5, 0.0, 0.0]
           ]                                  

def main():
    cur_velocity_pos = 0
    movement = 2
    
    #Select the type of movement
    #1: rotacional only, 2: forward-backward, 3: sideway, 4: diagonal, 5: square
    if movement == 1:
        velocities=velocities_wz
    elif movement == 2:
        velocities = velocities_x
    elif movement == 3:
        velocities = velocities_y
    elif movement == 4:
        velocities = velocities_xy
    else:
        velocities = velocities_square  

    rclpy.init()
    try:
        send_ext_velocity = Send_Ext_Velocity()
        rate = send_ext_velocity.create_rate(30)
        while rclpy.ok():
            cur_velocity_pos += 1
            
            if cur_velocity_pos >= len(velocities):
                cur_velocity_pos = 0
            cur_velocity = array(velocities[cur_velocity_pos])
            send_ext_velocity.set_mob_ext_velocities(
                cur_velocity)

            send_ext_velocity.send_extvelocities()
            rclpy.spin_once(send_ext_velocity)
            rate.sleep()
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
