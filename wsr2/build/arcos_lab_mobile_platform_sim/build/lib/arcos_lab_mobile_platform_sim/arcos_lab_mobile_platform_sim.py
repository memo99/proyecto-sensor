#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2013 Federico Ruiz-Ugalde
# Author: Federico Ruiz Ugalde <memeruiz at gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import rclpy
from rclpy import time
from rclpy.node import Node
from rclpy.qos import QoSProfile
from array import array
from functools import reduce
from numpy import pi, array, column_stack, savetxt
import logging
# from std_msgs.msg import String

from sensor_msgs.msg import JointState

from arcos_lab_mobile_platform_msgs.msg import MobCommand
from arcos_lab_mobile_platform_msgs.msg import MobExtTorques
from arcos_lab_mobile_platform_msgs.msg import MobStatus

# from arcos_lab_mobile_platform_msgs.srv import enable_hands

from python_robot_misc_utils import joint_sim
logging.getLogger('python_robot_misc_utils.joint_sim.joint_sim').setLevel(
    logging.INFO)

module_name = "arcos_lab_mobile_platform_sim"


def nano_to_sec(now):
    return(now.nanoseconds*10**-9)


class Arcos_lab_mobile_platform_sim(Node):
    def __init__(self,
                 hands=2,
                 fingers=5,
                 node_name="arcos_lab_mobile_platform_sim",
                 joint_state_base_name="platform",
                 frame_id="platform"):
        super().__init__(node_name)
        self.joint_state_base_name = joint_state_base_name
        self.node_name = node_name
        qos_profile = QoSProfile(depth=10)
        self.declare_parameter('mob_instance', 'humanoid')
        self.mob_instance = self.get_parameter(
            'mob_instance').get_parameter_value().string_value
        self.pub_raw = self.create_publisher(MobStatus,
                                             self.node_name+"/out",
                                             qos_profile)
        self.pub_joint_states = self.create_publisher(JointState,
                                                      self.node_name+"/joint_states", qos_profile)
        self.num_joints = 4
        self.viscous_friction_coef = array([0.5] * self.num_joints)
        self.rot_inertia = array([1.0] * self.num_joints)

        self.clock = self.get_clock()
        self.cycle_freq = 30.0
        self.timer = self.create_timer(1.0/self.cycle_freq, self.my_loop)

        # self.hands_sim:
        #   Contains all the hand simulators, one for each finger
        # self.status:
        #   Contains a SystemStatus msg used for /out topic data
        #   Contains all realtime output information from the hand
        # self.joint_states:
        #   Contains all realtime joint_state data to be used with a
        #   Robot state publisher for TF
        self.mob_sim, self.mob_controller, self.status, self.joint_states, self.mob_ext_torques_empty, self.mob_velocity_empty = self.create_structures_and_sims()
        self.printi("Structures: ")
        self.printi("mob_sim: ")
        self.printi(str(self.mob_sim))
        self.printi("mob_controller: ")
        self.printi(str(self.mob_controller))
        self.printi("status: ")
        self.printi(self.status)
        self.printi("joint_states")
        self.printi(self.joint_states)
        self.set_frame_id(frame_id+"_"+self.mob_instance)

        self.printi("Creating /in topic for receiving input cmds")
        self.sub_mob_cmd = self.create_subscription(
            MobCommand,
            self.node_name+"/in",
            self.set_mob_velocity_refs,
            qos_profile)
        self.ext_velocity_empty = True
        self.last_set_ext_velocity = self.clock.now
        self.printi(
            "Creating /ext_torques for receiving simulation external torques")
        self.sub_ext_torque = self.create_subscription(
            MobExtTorques,  # FIX
            self.node_name+"/ext_torques",
            self.set_ext_torques,
            qos_profile)
        self.ext_torque_empty = True
        self.last_set_ext_torques = self.clock.now()
        self.ext_torque_timeout = 2.0
        # TODO: deal with the coupled joint

    def printi(self, *args, **kwargs):
        self.get_logger().info(str(reduce(lambda x, y: str(x)+" "+str(y), args)))

    def set_frame_id(self, frame_id):
        self.joint_states.header.frame_id = frame_id

    def create_structures_and_sims(self):
        self.printi("Creating All Structures and simulators")
        self.printi("Initializing MobStatus ROS msg struct")
        status = MobStatus()
        self.printi("Initializing JointState ROS msg struct")
        joint_states = JointState()
        self.printi("Initializing MobExtTorques ROS msg struct")
        mob_ext_torques = MobExtTorques()
        self.printi("Initializing MobCommand ROS msg struct")
        mob_velocity_reference = MobCommand()
        mob_sim = []
        self.t_now = nano_to_sec(self.clock.now())
        # TODO: maybe add timestamp to status

        status.position = [0.0] * self.num_joints
        status.torque = [0.0] * self.num_joints
        status.enabled = False
        mob_ext_torques.torque = [0.0] * self.num_joints
        # creating zero JointState msg
        self.printi("Initializing joint_states data")
        joint_states.position.extend([0.0] * self.num_joints)
        joint_states.velocity.extend([0.0] * self.num_joints)
        joint_states.effort.extend([0.0] * self.num_joints)
        for k in range(self.num_joints):
            joint_states.name.extend([
                # self.joint_state_base_name + "-arm-" + str(k)
                self.mob_instance+"_" + "mob_" + str(k) + "_joint"
            ])

        self.printi(f'Creating platform joints simulator')
        mob_sim = joint_sim.Sim_mech_joints(
            num_joints=self.num_joints,
            inertia=self.rot_inertia,
            viscous_friction_coef=self.viscous_friction_coef)
        mob_sim.init_time(self.t_now)
        self.printi(f'Creating platform controllers simulator')
        mob_controller = joint_sim.Sim_controllers(
            num_joints=self.num_joints,
            kqp=[35.0] * self.num_joints,
            kqi=[0.1] * self.num_joints,
            kqd=[16.0] * self.num_joints,
            stiffness=[0.1] * self.num_joints,
            ctrl_joint_limits=[[
                -170.0 * pi / 180.0,
                170.0 * pi / 180.0,
            ]] * self.num_joints,
            q_ke_i_lim=[0.01] * self.num_joints,
            torque_ref_max=[10.0] * self.num_joints,
            viscous_friction_coef=self.viscous_friction_coef)
        # TODO: set some initial position (the same in ROS and
        # sim)
        # return ((finger_sim, finger_controller), fstatus)

        return (mob_sim, mob_controller, status, joint_states, mob_ext_torques, mob_velocity_reference)

#     def systemcmd(self, hands=2, fingers=2):
#         cmd = SystemCommand()
#         for i in range(hands):
#             rospy.logdebug("Adding hand %s", i)
#             cmd.hand.append(self.handcmd(fingers))
#         return (cmd)

#     def handcmd(self, fingers=5):
#         hcmd = HandCommand()
#         for i in range(fingers):
#             rospy.logdebug("Adding finger %s", i)
#             fcmd = self.fingercmd()
#             hcmd.finger.append(fcmd)
#         return (hcmd)

#     def fingercmd(self, angle=[10.0, 10.0, 0.0]):
#         fcmd = FingerCommand()
#         fcmd.angle = angle
#         return (fcmd)

#     def set_finger_angles(self, hand, finger, angles):
#         self.cmd.hand[hand].finger[finger].angle = angles
#         self.cmd.hand[hand].finger[finger].enable = True

#     def set_finger_stiff(self, hand, finger, stiff):
#         self.cmd.hand[hand].finger[finger].stiffness = stiff

    def update_mob_state(self):
        """Updates the joint mechanical simulation state"""
        t_previous = self.t_now
        self.t_now_ros = self.clock.now()
        self.joint_states.header.stamp = self.t_now_ros.to_msg()
        self.t_now = nano_to_sec(self.t_now_ros)
        if (not self.ext_torque_empty) \
                and nano_to_sec(
                    self.t_now_ros-self.last_set_ext_torques) > self.ext_torque_timeout:
            print("Resetting ext_torque")
            self.set_ext_torques(self.mob_ext_torques_empty)
            self.ext_torque_empty = True
        # TODO: add timestamp to SystemStatus msg
        delta_time = self.t_now - t_previous
        self.mob_sim.update(self.t_now)
        measured_ext_torque = self.mob_sim.get_measured_torque_ext()
        self.mob_controller.update(
            q=self.mob_sim.get_q(),
            qv=self.mob_sim.get_qv(),
            measured_ext_torque=measured_ext_torque,
            delta_time=delta_time)
        #self.mob_sim.set_motor_torque(self.mob_controller.motor_torque)
        self.status.position = self.mob_sim.get_q(
        ).tolist()
        self.status.torque = measured_ext_torque.tolist()
        for k in range(self.num_joints):
            self.joint_states.position[k] = self.status.position[k]
            self.joint_states.effort[k] = self.status.torque[k]

    # def set_mob_refs(self, data):
    #     """Sets the joints reference values"""
    #     self.get_logger().debug(f'Commanding Mob')
    #     self.status.enabled = data.enable
    #     if len(data.angle) > 0:
    #         self.mob_controller.set_q_ref(data.angle)
    #     if len(data.stiffness) > 0:
    #         self.mob_controller.set_stiffness(
    #             data.stiffness)
    #     # TODO: kp, velocity
    #     pass

    def set_mob_velocity_refs(self, data):
        """Sets the velocity reference values vx,vy,wz """
        self.ext_velocity_empty = False
        self.last_set_ext_velocity = self.clock.now()
        self.printi("Set Velocity Reference for platform")
        self.printi(f"Velocity: {data.velocity}")
        self.mob_sim.set_qv(data.velocity)

    def set_ext_torques(self, data):
        """Sets the external joint torques"""
        self.ext_torque_empty = False
        self.last_set_ext_torques = self.clock.now()
        self.printi("Set External Torques for platform")
        self.mob_sim.set_torque_ext(data.torque)
        self.printi(f"Torque: {data.torque}")

    def my_loop(self):
        # Get/update "from-hand" data
        self.update_mob_state()
        # Publish raw data
        # Publish joint_state_data
        self.pub_raw.publish(self.status)
        self.pub_joint_states.publish(self.joint_states)


def main(args=None):
    rclpy.init(args=args)
    cur_angle_pos = 0
    arcos_lab_mobile_platform_sim = Arcos_lab_mobile_platform_sim()
    try:
        rclpy.spin(arcos_lab_mobile_platform_sim)
    except KeyboardInterrupt:
        pass
    arcos_lab_mobile_platform_sim.destroy_node()

    rclpy.shutdown()


if __name__ == '__main__':
    main()
