import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    return LaunchDescription([
        Node(
            package='kuka_lwr4plus_sim',
            executable='sim',
            remappings=[
                ("/kuka_lwr4plus_sim/joint_states", "/joint_states")
            ],
            name='kuka_lwr4plus_sim',
            output='screen'),
    ])
