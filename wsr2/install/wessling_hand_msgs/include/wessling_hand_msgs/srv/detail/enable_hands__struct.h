// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__STRUCT_H_
#define WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'enable'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/EnableHands in the package wessling_hand_msgs.
typedef struct wessling_hand_msgs__srv__EnableHands_Request
{
  rosidl_runtime_c__float__Sequence enable;
  int32_t num_fingers;
} wessling_hand_msgs__srv__EnableHands_Request;

// Struct for a sequence of wessling_hand_msgs__srv__EnableHands_Request.
typedef struct wessling_hand_msgs__srv__EnableHands_Request__Sequence
{
  wessling_hand_msgs__srv__EnableHands_Request * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} wessling_hand_msgs__srv__EnableHands_Request__Sequence;


// Constants defined in the message

// Include directives for member types
// Member 'enabled'
// already included above
// #include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/EnableHands in the package wessling_hand_msgs.
typedef struct wessling_hand_msgs__srv__EnableHands_Response
{
  rosidl_runtime_c__float__Sequence enabled;
} wessling_hand_msgs__srv__EnableHands_Response;

// Struct for a sequence of wessling_hand_msgs__srv__EnableHands_Response.
typedef struct wessling_hand_msgs__srv__EnableHands_Response__Sequence
{
  wessling_hand_msgs__srv__EnableHands_Response * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} wessling_hand_msgs__srv__EnableHands_Response__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__STRUCT_H_
