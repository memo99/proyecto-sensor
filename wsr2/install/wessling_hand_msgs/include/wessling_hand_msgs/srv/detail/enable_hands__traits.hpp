// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__TRAITS_HPP_
#define WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__TRAITS_HPP_

#include "wessling_hand_msgs/srv/detail/enable_hands__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<wessling_hand_msgs::srv::EnableHands_Request>()
{
  return "wessling_hand_msgs::srv::EnableHands_Request";
}

template<>
inline const char * name<wessling_hand_msgs::srv::EnableHands_Request>()
{
  return "wessling_hand_msgs/srv/EnableHands_Request";
}

template<>
struct has_fixed_size<wessling_hand_msgs::srv::EnableHands_Request>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<wessling_hand_msgs::srv::EnableHands_Request>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<wessling_hand_msgs::srv::EnableHands_Request>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<wessling_hand_msgs::srv::EnableHands_Response>()
{
  return "wessling_hand_msgs::srv::EnableHands_Response";
}

template<>
inline const char * name<wessling_hand_msgs::srv::EnableHands_Response>()
{
  return "wessling_hand_msgs/srv/EnableHands_Response";
}

template<>
struct has_fixed_size<wessling_hand_msgs::srv::EnableHands_Response>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<wessling_hand_msgs::srv::EnableHands_Response>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<wessling_hand_msgs::srv::EnableHands_Response>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<wessling_hand_msgs::srv::EnableHands>()
{
  return "wessling_hand_msgs::srv::EnableHands";
}

template<>
inline const char * name<wessling_hand_msgs::srv::EnableHands>()
{
  return "wessling_hand_msgs/srv/EnableHands";
}

template<>
struct has_fixed_size<wessling_hand_msgs::srv::EnableHands>
  : std::integral_constant<
    bool,
    has_fixed_size<wessling_hand_msgs::srv::EnableHands_Request>::value &&
    has_fixed_size<wessling_hand_msgs::srv::EnableHands_Response>::value
  >
{
};

template<>
struct has_bounded_size<wessling_hand_msgs::srv::EnableHands>
  : std::integral_constant<
    bool,
    has_bounded_size<wessling_hand_msgs::srv::EnableHands_Request>::value &&
    has_bounded_size<wessling_hand_msgs::srv::EnableHands_Response>::value
  >
{
};

template<>
struct is_service<wessling_hand_msgs::srv::EnableHands>
  : std::true_type
{
};

template<>
struct is_service_request<wessling_hand_msgs::srv::EnableHands_Request>
  : std::true_type
{
};

template<>
struct is_service_response<wessling_hand_msgs::srv::EnableHands_Response>
  : std::true_type
{
};

}  // namespace rosidl_generator_traits

#endif  // WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__TRAITS_HPP_
