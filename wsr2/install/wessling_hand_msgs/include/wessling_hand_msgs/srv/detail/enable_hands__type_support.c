// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "wessling_hand_msgs/srv/detail/enable_hands__rosidl_typesupport_introspection_c.h"
#include "wessling_hand_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "wessling_hand_msgs/srv/detail/enable_hands__functions.h"
#include "wessling_hand_msgs/srv/detail/enable_hands__struct.h"


// Include directives for member types
// Member `enable`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  wessling_hand_msgs__srv__EnableHands_Request__init(message_memory);
}

void EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_fini_function(void * message_memory)
{
  wessling_hand_msgs__srv__EnableHands_Request__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_member_array[2] = {
  {
    "enable",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__srv__EnableHands_Request, enable),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "num_fingers",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_INT32,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__srv__EnableHands_Request, num_fingers),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_members = {
  "wessling_hand_msgs__srv",  // message namespace
  "EnableHands_Request",  // message name
  2,  // number of fields
  sizeof(wessling_hand_msgs__srv__EnableHands_Request),
  EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_member_array,  // message members
  EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_init_function,  // function to initialize message memory (memory has to be allocated)
  EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_type_support_handle = {
  0,
  &EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, srv, EnableHands_Request)() {
  if (!EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_type_support_handle.typesupport_identifier) {
    EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &EnableHands_Request__rosidl_typesupport_introspection_c__EnableHands_Request_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

// already included above
// #include <stddef.h>
// already included above
// #include "wessling_hand_msgs/srv/detail/enable_hands__rosidl_typesupport_introspection_c.h"
// already included above
// #include "wessling_hand_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "rosidl_typesupport_introspection_c/field_types.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
// already included above
// #include "rosidl_typesupport_introspection_c/message_introspection.h"
// already included above
// #include "wessling_hand_msgs/srv/detail/enable_hands__functions.h"
// already included above
// #include "wessling_hand_msgs/srv/detail/enable_hands__struct.h"


// Include directives for member types
// Member `enabled`
// already included above
// #include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  wessling_hand_msgs__srv__EnableHands_Response__init(message_memory);
}

void EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_fini_function(void * message_memory)
{
  wessling_hand_msgs__srv__EnableHands_Response__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_member_array[1] = {
  {
    "enabled",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__srv__EnableHands_Response, enabled),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_members = {
  "wessling_hand_msgs__srv",  // message namespace
  "EnableHands_Response",  // message name
  1,  // number of fields
  sizeof(wessling_hand_msgs__srv__EnableHands_Response),
  EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_member_array,  // message members
  EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_init_function,  // function to initialize message memory (memory has to be allocated)
  EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_type_support_handle = {
  0,
  &EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, srv, EnableHands_Response)() {
  if (!EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_type_support_handle.typesupport_identifier) {
    EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &EnableHands_Response__rosidl_typesupport_introspection_c__EnableHands_Response_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif

#include "rosidl_runtime_c/service_type_support_struct.h"
// already included above
// #include "wessling_hand_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
// already included above
// #include "wessling_hand_msgs/srv/detail/enable_hands__rosidl_typesupport_introspection_c.h"
// already included above
// #include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/service_introspection.h"

// this is intentionally not const to allow initialization later to prevent an initialization race
static rosidl_typesupport_introspection_c__ServiceMembers wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_service_members = {
  "wessling_hand_msgs__srv",  // service namespace
  "EnableHands",  // service name
  // these two fields are initialized below on the first access
  NULL,  // request message
  // wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_Request_message_type_support_handle,
  NULL  // response message
  // wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_Response_message_type_support_handle
};

static rosidl_service_type_support_t wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_service_type_support_handle = {
  0,
  &wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_service_members,
  get_service_typesupport_handle_function,
};

// Forward declaration of request/response type support functions
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, srv, EnableHands_Request)();

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, srv, EnableHands_Response)();

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_wessling_hand_msgs
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, srv, EnableHands)() {
  if (!wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_service_type_support_handle.typesupport_identifier) {
    wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_service_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  rosidl_typesupport_introspection_c__ServiceMembers * service_members =
    (rosidl_typesupport_introspection_c__ServiceMembers *)wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_service_type_support_handle.data;

  if (!service_members->request_members_) {
    service_members->request_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, srv, EnableHands_Request)()->data;
  }
  if (!service_members->response_members_) {
    service_members->response_members_ =
      (const rosidl_typesupport_introspection_c__MessageMembers *)
      ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, srv, EnableHands_Response)()->data;
  }

  return &wessling_hand_msgs__srv__detail__enable_hands__rosidl_typesupport_introspection_c__EnableHands_service_type_support_handle;
}
