// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__STRUCT_HPP_
#define WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__srv__EnableHands_Request __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__srv__EnableHands_Request __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct EnableHands_Request_
{
  using Type = EnableHands_Request_<ContainerAllocator>;

  explicit EnableHands_Request_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->num_fingers = 0l;
    }
  }

  explicit EnableHands_Request_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->num_fingers = 0l;
    }
  }

  // field types and members
  using _enable_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _enable_type enable;
  using _num_fingers_type =
    int32_t;
  _num_fingers_type num_fingers;

  // setters for named parameter idiom
  Type & set__enable(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->enable = _arg;
    return *this;
  }
  Type & set__num_fingers(
    const int32_t & _arg)
  {
    this->num_fingers = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__srv__EnableHands_Request
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__srv__EnableHands_Request
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Request_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const EnableHands_Request_ & other) const
  {
    if (this->enable != other.enable) {
      return false;
    }
    if (this->num_fingers != other.num_fingers) {
      return false;
    }
    return true;
  }
  bool operator!=(const EnableHands_Request_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct EnableHands_Request_

// alias to use template instance with default allocator
using EnableHands_Request =
  wessling_hand_msgs::srv::EnableHands_Request_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace wessling_hand_msgs


#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__srv__EnableHands_Response __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__srv__EnableHands_Response __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct EnableHands_Response_
{
  using Type = EnableHands_Response_<ContainerAllocator>;

  explicit EnableHands_Response_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit EnableHands_Response_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _enabled_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _enabled_type enabled;

  // setters for named parameter idiom
  Type & set__enabled(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->enabled = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__srv__EnableHands_Response
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__srv__EnableHands_Response
    std::shared_ptr<wessling_hand_msgs::srv::EnableHands_Response_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const EnableHands_Response_ & other) const
  {
    if (this->enabled != other.enabled) {
      return false;
    }
    return true;
  }
  bool operator!=(const EnableHands_Response_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct EnableHands_Response_

// alias to use template instance with default allocator
using EnableHands_Response =
  wessling_hand_msgs::srv::EnableHands_Response_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace wessling_hand_msgs

namespace wessling_hand_msgs
{

namespace srv
{

struct EnableHands
{
  using Request = wessling_hand_msgs::srv::EnableHands_Request;
  using Response = wessling_hand_msgs::srv::EnableHands_Response;
};

}  // namespace srv

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__STRUCT_HPP_
