// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from wessling_hand_msgs:srv/EnableHands.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__TYPE_SUPPORT_H_
#define WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "wessling_hand_msgs/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  wessling_hand_msgs,
  srv,
  EnableHands_Request
)();

// already included above
// #include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  wessling_hand_msgs,
  srv,
  EnableHands_Response
)();

#include "rosidl_runtime_c/service_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(
  rosidl_typesupport_c,
  wessling_hand_msgs,
  srv,
  EnableHands
)();

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__SRV__DETAIL__ENABLE_HANDS__TYPE_SUPPORT_H_
