// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__SRV__ENABLE_HANDS_HPP_
#define WESSLING_HAND_MSGS__SRV__ENABLE_HANDS_HPP_

#include "wessling_hand_msgs/srv/detail/enable_hands__struct.hpp"
#include "wessling_hand_msgs/srv/detail/enable_hands__builder.hpp"
#include "wessling_hand_msgs/srv/detail/enable_hands__traits.hpp"

#endif  // WESSLING_HAND_MSGS__SRV__ENABLE_HANDS_HPP_
