// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/SystemCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/system_command__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_SystemCommand_emergency
{
public:
  explicit Init_SystemCommand_emergency(::wessling_hand_msgs::msg::SystemCommand & msg)
  : msg_(msg)
  {}
  ::wessling_hand_msgs::msg::SystemCommand emergency(::wessling_hand_msgs::msg::SystemCommand::_emergency_type arg)
  {
    msg_.emergency = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemCommand msg_;
};

class Init_SystemCommand_hand
{
public:
  Init_SystemCommand_hand()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_SystemCommand_emergency hand(::wessling_hand_msgs::msg::SystemCommand::_hand_type arg)
  {
    msg_.hand = std::move(arg);
    return Init_SystemCommand_emergency(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemCommand msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::SystemCommand>()
{
  return wessling_hand_msgs::msg::builder::Init_SystemCommand_hand();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__BUILDER_HPP_
