// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from wessling_hand_msgs:msg/FingerStatus.idl
// generated code does not contain a copyright notice
#include "wessling_hand_msgs/msg/detail/finger_status__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `position`
// Member `torque`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
wessling_hand_msgs__msg__FingerStatus__init(wessling_hand_msgs__msg__FingerStatus * msg)
{
  if (!msg) {
    return false;
  }
  // enabled
  // position
  if (!rosidl_runtime_c__float__Sequence__init(&msg->position, 0)) {
    wessling_hand_msgs__msg__FingerStatus__fini(msg);
    return false;
  }
  // torque
  if (!rosidl_runtime_c__float__Sequence__init(&msg->torque, 0)) {
    wessling_hand_msgs__msg__FingerStatus__fini(msg);
    return false;
  }
  return true;
}

void
wessling_hand_msgs__msg__FingerStatus__fini(wessling_hand_msgs__msg__FingerStatus * msg)
{
  if (!msg) {
    return;
  }
  // enabled
  // position
  rosidl_runtime_c__float__Sequence__fini(&msg->position);
  // torque
  rosidl_runtime_c__float__Sequence__fini(&msg->torque);
}

wessling_hand_msgs__msg__FingerStatus *
wessling_hand_msgs__msg__FingerStatus__create()
{
  wessling_hand_msgs__msg__FingerStatus * msg = (wessling_hand_msgs__msg__FingerStatus *)malloc(sizeof(wessling_hand_msgs__msg__FingerStatus));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(wessling_hand_msgs__msg__FingerStatus));
  bool success = wessling_hand_msgs__msg__FingerStatus__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
wessling_hand_msgs__msg__FingerStatus__destroy(wessling_hand_msgs__msg__FingerStatus * msg)
{
  if (msg) {
    wessling_hand_msgs__msg__FingerStatus__fini(msg);
  }
  free(msg);
}


bool
wessling_hand_msgs__msg__FingerStatus__Sequence__init(wessling_hand_msgs__msg__FingerStatus__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  wessling_hand_msgs__msg__FingerStatus * data = NULL;
  if (size) {
    data = (wessling_hand_msgs__msg__FingerStatus *)calloc(size, sizeof(wessling_hand_msgs__msg__FingerStatus));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = wessling_hand_msgs__msg__FingerStatus__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        wessling_hand_msgs__msg__FingerStatus__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
wessling_hand_msgs__msg__FingerStatus__Sequence__fini(wessling_hand_msgs__msg__FingerStatus__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      wessling_hand_msgs__msg__FingerStatus__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

wessling_hand_msgs__msg__FingerStatus__Sequence *
wessling_hand_msgs__msg__FingerStatus__Sequence__create(size_t size)
{
  wessling_hand_msgs__msg__FingerStatus__Sequence * array = (wessling_hand_msgs__msg__FingerStatus__Sequence *)malloc(sizeof(wessling_hand_msgs__msg__FingerStatus__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = wessling_hand_msgs__msg__FingerStatus__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
wessling_hand_msgs__msg__FingerStatus__Sequence__destroy(wessling_hand_msgs__msg__FingerStatus__Sequence * array)
{
  if (array) {
    wessling_hand_msgs__msg__FingerStatus__Sequence__fini(array);
  }
  free(array);
}
