// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/SystemStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_STATUS__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_STATUS__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/system_status__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_SystemStatus_right_hand
{
public:
  explicit Init_SystemStatus_right_hand(::wessling_hand_msgs::msg::SystemStatus & msg)
  : msg_(msg)
  {}
  ::wessling_hand_msgs::msg::SystemStatus right_hand(::wessling_hand_msgs::msg::SystemStatus::_right_hand_type arg)
  {
    msg_.right_hand = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemStatus msg_;
};

class Init_SystemStatus_left_hand
{
public:
  explicit Init_SystemStatus_left_hand(::wessling_hand_msgs::msg::SystemStatus & msg)
  : msg_(msg)
  {}
  Init_SystemStatus_right_hand left_hand(::wessling_hand_msgs::msg::SystemStatus::_left_hand_type arg)
  {
    msg_.left_hand = std::move(arg);
    return Init_SystemStatus_right_hand(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemStatus msg_;
};

class Init_SystemStatus_brake_status
{
public:
  explicit Init_SystemStatus_brake_status(::wessling_hand_msgs::msg::SystemStatus & msg)
  : msg_(msg)
  {}
  Init_SystemStatus_left_hand brake_status(::wessling_hand_msgs::msg::SystemStatus::_brake_status_type arg)
  {
    msg_.brake_status = std::move(arg);
    return Init_SystemStatus_left_hand(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemStatus msg_;
};

class Init_SystemStatus_hand
{
public:
  Init_SystemStatus_hand()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_SystemStatus_brake_status hand(::wessling_hand_msgs::msg::SystemStatus::_hand_type arg)
  {
    msg_.hand = std::move(arg);
    return Init_SystemStatus_brake_status(msg_);
  }

private:
  ::wessling_hand_msgs::msg::SystemStatus msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::SystemStatus>()
{
  return wessling_hand_msgs::msg::builder::Init_SystemStatus_hand();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_STATUS__BUILDER_HPP_
