// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from wessling_hand_msgs:msg/FingerExtTorques.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "wessling_hand_msgs/msg/detail/finger_ext_torques__rosidl_typesupport_introspection_c.h"
#include "wessling_hand_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "wessling_hand_msgs/msg/detail/finger_ext_torques__functions.h"
#include "wessling_hand_msgs/msg/detail/finger_ext_torques__struct.h"


// Include directives for member types
// Member `torque`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  wessling_hand_msgs__msg__FingerExtTorques__init(message_memory);
}

void FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_fini_function(void * message_memory)
{
  wessling_hand_msgs__msg__FingerExtTorques__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_member_array[1] = {
  {
    "torque",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__msg__FingerExtTorques, torque),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_members = {
  "wessling_hand_msgs__msg",  // message namespace
  "FingerExtTorques",  // message name
  1,  // number of fields
  sizeof(wessling_hand_msgs__msg__FingerExtTorques),
  FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_member_array,  // message members
  FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_init_function,  // function to initialize message memory (memory has to be allocated)
  FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_type_support_handle = {
  0,
  &FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, msg, FingerExtTorques)() {
  if (!FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_type_support_handle.typesupport_identifier) {
    FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &FingerExtTorques__rosidl_typesupport_introspection_c__FingerExtTorques_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
