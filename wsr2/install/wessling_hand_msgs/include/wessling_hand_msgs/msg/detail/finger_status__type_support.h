// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from wessling_hand_msgs:msg/FingerStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__TYPE_SUPPORT_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "wessling_hand_msgs/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  wessling_hand_msgs,
  msg,
  FingerStatus
)();

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__TYPE_SUPPORT_H_
