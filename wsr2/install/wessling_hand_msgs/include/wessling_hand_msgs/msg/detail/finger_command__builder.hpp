// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/FingerCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_COMMAND__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_COMMAND__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/finger_command__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_FingerCommand_stiffness
{
public:
  explicit Init_FingerCommand_stiffness(::wessling_hand_msgs::msg::FingerCommand & msg)
  : msg_(msg)
  {}
  ::wessling_hand_msgs::msg::FingerCommand stiffness(::wessling_hand_msgs::msg::FingerCommand::_stiffness_type arg)
  {
    msg_.stiffness = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerCommand msg_;
};

class Init_FingerCommand_velocity
{
public:
  explicit Init_FingerCommand_velocity(::wessling_hand_msgs::msg::FingerCommand & msg)
  : msg_(msg)
  {}
  Init_FingerCommand_stiffness velocity(::wessling_hand_msgs::msg::FingerCommand::_velocity_type arg)
  {
    msg_.velocity = std::move(arg);
    return Init_FingerCommand_stiffness(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerCommand msg_;
};

class Init_FingerCommand_kp
{
public:
  explicit Init_FingerCommand_kp(::wessling_hand_msgs::msg::FingerCommand & msg)
  : msg_(msg)
  {}
  Init_FingerCommand_velocity kp(::wessling_hand_msgs::msg::FingerCommand::_kp_type arg)
  {
    msg_.kp = std::move(arg);
    return Init_FingerCommand_velocity(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerCommand msg_;
};

class Init_FingerCommand_angle
{
public:
  explicit Init_FingerCommand_angle(::wessling_hand_msgs::msg::FingerCommand & msg)
  : msg_(msg)
  {}
  Init_FingerCommand_kp angle(::wessling_hand_msgs::msg::FingerCommand::_angle_type arg)
  {
    msg_.angle = std::move(arg);
    return Init_FingerCommand_kp(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerCommand msg_;
};

class Init_FingerCommand_enable
{
public:
  Init_FingerCommand_enable()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_FingerCommand_angle enable(::wessling_hand_msgs::msg::FingerCommand::_enable_type arg)
  {
    msg_.enable = std::move(arg);
    return Init_FingerCommand_angle(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerCommand msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::FingerCommand>()
{
  return wessling_hand_msgs::msg::builder::Init_FingerCommand_enable();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_COMMAND__BUILDER_HPP_
