// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from wessling_hand_msgs:msg/HandStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__STRUCT_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'finger'
#include "wessling_hand_msgs/msg/detail/finger_status__struct.h"

// Struct defined in msg/HandStatus in the package wessling_hand_msgs.
typedef struct wessling_hand_msgs__msg__HandStatus
{
  wessling_hand_msgs__msg__FingerStatus__Sequence finger;
  float brake_status;
  bool port_status;
} wessling_hand_msgs__msg__HandStatus;

// Struct for a sequence of wessling_hand_msgs__msg__HandStatus.
typedef struct wessling_hand_msgs__msg__HandStatus__Sequence
{
  wessling_hand_msgs__msg__HandStatus * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} wessling_hand_msgs__msg__HandStatus__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__STRUCT_H_
