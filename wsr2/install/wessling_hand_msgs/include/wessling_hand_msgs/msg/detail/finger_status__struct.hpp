// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from wessling_hand_msgs:msg/FingerStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__STRUCT_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__msg__FingerStatus __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__msg__FingerStatus __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct FingerStatus_
{
  using Type = FingerStatus_<ContainerAllocator>;

  explicit FingerStatus_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->enabled = false;
    }
  }

  explicit FingerStatus_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->enabled = false;
    }
  }

  // field types and members
  using _enabled_type =
    bool;
  _enabled_type enabled;
  using _position_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _position_type position;
  using _torque_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _torque_type torque;

  // setters for named parameter idiom
  Type & set__enabled(
    const bool & _arg)
  {
    this->enabled = _arg;
    return *this;
  }
  Type & set__position(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->position = _arg;
    return *this;
  }
  Type & set__torque(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->torque = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__msg__FingerStatus
    std::shared_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__msg__FingerStatus
    std::shared_ptr<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const FingerStatus_ & other) const
  {
    if (this->enabled != other.enabled) {
      return false;
    }
    if (this->position != other.position) {
      return false;
    }
    if (this->torque != other.torque) {
      return false;
    }
    return true;
  }
  bool operator!=(const FingerStatus_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct FingerStatus_

// alias to use template instance with default allocator
using FingerStatus =
  wessling_hand_msgs::msg::FingerStatus_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_STATUS__STRUCT_HPP_
