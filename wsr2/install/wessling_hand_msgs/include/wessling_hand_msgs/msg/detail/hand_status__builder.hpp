// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/HandStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/hand_status__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_HandStatus_port_status
{
public:
  explicit Init_HandStatus_port_status(::wessling_hand_msgs::msg::HandStatus & msg)
  : msg_(msg)
  {}
  ::wessling_hand_msgs::msg::HandStatus port_status(::wessling_hand_msgs::msg::HandStatus::_port_status_type arg)
  {
    msg_.port_status = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::HandStatus msg_;
};

class Init_HandStatus_brake_status
{
public:
  explicit Init_HandStatus_brake_status(::wessling_hand_msgs::msg::HandStatus & msg)
  : msg_(msg)
  {}
  Init_HandStatus_port_status brake_status(::wessling_hand_msgs::msg::HandStatus::_brake_status_type arg)
  {
    msg_.brake_status = std::move(arg);
    return Init_HandStatus_port_status(msg_);
  }

private:
  ::wessling_hand_msgs::msg::HandStatus msg_;
};

class Init_HandStatus_finger
{
public:
  Init_HandStatus_finger()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_HandStatus_brake_status finger(::wessling_hand_msgs::msg::HandStatus::_finger_type arg)
  {
    msg_.finger = std::move(arg);
    return Init_HandStatus_brake_status(msg_);
  }

private:
  ::wessling_hand_msgs::msg::HandStatus msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::HandStatus>()
{
  return wessling_hand_msgs::msg::builder::Init_HandStatus_finger();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__BUILDER_HPP_
