// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from wessling_hand_msgs:msg/HandCommand.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "wessling_hand_msgs/msg/detail/hand_command__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace wessling_hand_msgs
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

void HandCommand_init_function(
  void * message_memory, rosidl_runtime_cpp::MessageInitialization _init)
{
  new (message_memory) wessling_hand_msgs::msg::HandCommand(_init);
}

void HandCommand_fini_function(void * message_memory)
{
  auto typed_message = static_cast<wessling_hand_msgs::msg::HandCommand *>(message_memory);
  typed_message->~HandCommand();
}

size_t size_function__HandCommand__finger(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<wessling_hand_msgs::msg::FingerCommand> *>(untyped_member);
  return member->size();
}

const void * get_const_function__HandCommand__finger(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<wessling_hand_msgs::msg::FingerCommand> *>(untyped_member);
  return &member[index];
}

void * get_function__HandCommand__finger(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<wessling_hand_msgs::msg::FingerCommand> *>(untyped_member);
  return &member[index];
}

void resize_function__HandCommand__finger(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<wessling_hand_msgs::msg::FingerCommand> *>(untyped_member);
  member->resize(size);
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember HandCommand_message_member_array[1] = {
  {
    "finger",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    ::rosidl_typesupport_introspection_cpp::get_message_type_support_handle<wessling_hand_msgs::msg::FingerCommand>(),  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::HandCommand, finger),  // bytes offset in struct
    nullptr,  // default value
    size_function__HandCommand__finger,  // size() function pointer
    get_const_function__HandCommand__finger,  // get_const(index) function pointer
    get_function__HandCommand__finger,  // get(index) function pointer
    resize_function__HandCommand__finger  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers HandCommand_message_members = {
  "wessling_hand_msgs::msg",  // message namespace
  "HandCommand",  // message name
  1,  // number of fields
  sizeof(wessling_hand_msgs::msg::HandCommand),
  HandCommand_message_member_array,  // message members
  HandCommand_init_function,  // function to initialize message memory (memory has to be allocated)
  HandCommand_fini_function  // function to terminate message instance (will not free memory)
};

static const rosidl_message_type_support_t HandCommand_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &HandCommand_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace wessling_hand_msgs


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<wessling_hand_msgs::msg::HandCommand>()
{
  return &::wessling_hand_msgs::msg::rosidl_typesupport_introspection_cpp::HandCommand_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, wessling_hand_msgs, msg, HandCommand)() {
  return &::wessling_hand_msgs::msg::rosidl_typesupport_introspection_cpp::HandCommand_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
