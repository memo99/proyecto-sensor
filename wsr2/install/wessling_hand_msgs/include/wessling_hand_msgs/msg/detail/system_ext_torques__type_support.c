// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from wessling_hand_msgs:msg/SystemExtTorques.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "wessling_hand_msgs/msg/detail/system_ext_torques__rosidl_typesupport_introspection_c.h"
#include "wessling_hand_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "wessling_hand_msgs/msg/detail/system_ext_torques__functions.h"
#include "wessling_hand_msgs/msg/detail/system_ext_torques__struct.h"


// Include directives for member types
// Member `hand`
#include "wessling_hand_msgs/msg/hand_ext_torques.h"
// Member `hand`
#include "wessling_hand_msgs/msg/detail/hand_ext_torques__rosidl_typesupport_introspection_c.h"

#ifdef __cplusplus
extern "C"
{
#endif

void SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  wessling_hand_msgs__msg__SystemExtTorques__init(message_memory);
}

void SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_fini_function(void * message_memory)
{
  wessling_hand_msgs__msg__SystemExtTorques__fini(message_memory);
}

size_t SystemExtTorques__rosidl_typesupport_introspection_c__size_function__HandExtTorques__hand(
  const void * untyped_member)
{
  const wessling_hand_msgs__msg__HandExtTorques__Sequence * member =
    (const wessling_hand_msgs__msg__HandExtTorques__Sequence *)(untyped_member);
  return member->size;
}

const void * SystemExtTorques__rosidl_typesupport_introspection_c__get_const_function__HandExtTorques__hand(
  const void * untyped_member, size_t index)
{
  const wessling_hand_msgs__msg__HandExtTorques__Sequence * member =
    (const wessling_hand_msgs__msg__HandExtTorques__Sequence *)(untyped_member);
  return &member->data[index];
}

void * SystemExtTorques__rosidl_typesupport_introspection_c__get_function__HandExtTorques__hand(
  void * untyped_member, size_t index)
{
  wessling_hand_msgs__msg__HandExtTorques__Sequence * member =
    (wessling_hand_msgs__msg__HandExtTorques__Sequence *)(untyped_member);
  return &member->data[index];
}

bool SystemExtTorques__rosidl_typesupport_introspection_c__resize_function__HandExtTorques__hand(
  void * untyped_member, size_t size)
{
  wessling_hand_msgs__msg__HandExtTorques__Sequence * member =
    (wessling_hand_msgs__msg__HandExtTorques__Sequence *)(untyped_member);
  wessling_hand_msgs__msg__HandExtTorques__Sequence__fini(member);
  return wessling_hand_msgs__msg__HandExtTorques__Sequence__init(member, size);
}

static rosidl_typesupport_introspection_c__MessageMember SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_member_array[3] = {
  {
    "hand",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_MESSAGE,  // type
    0,  // upper bound of string
    NULL,  // members of sub message (initialized later)
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__msg__SystemExtTorques, hand),  // bytes offset in struct
    NULL,  // default value
    SystemExtTorques__rosidl_typesupport_introspection_c__size_function__HandExtTorques__hand,  // size() function pointer
    SystemExtTorques__rosidl_typesupport_introspection_c__get_const_function__HandExtTorques__hand,  // get_const(index) function pointer
    SystemExtTorques__rosidl_typesupport_introspection_c__get_function__HandExtTorques__hand,  // get(index) function pointer
    SystemExtTorques__rosidl_typesupport_introspection_c__resize_function__HandExtTorques__hand  // resize(index) function pointer
  },
  {
    "left_hand",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__msg__SystemExtTorques, left_hand),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "right_hand",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs__msg__SystemExtTorques, right_hand),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_members = {
  "wessling_hand_msgs__msg",  // message namespace
  "SystemExtTorques",  // message name
  3,  // number of fields
  sizeof(wessling_hand_msgs__msg__SystemExtTorques),
  SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_member_array,  // message members
  SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_init_function,  // function to initialize message memory (memory has to be allocated)
  SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_type_support_handle = {
  0,
  &SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_wessling_hand_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, msg, SystemExtTorques)() {
  SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_member_array[0].members_ =
    ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, wessling_hand_msgs, msg, HandExtTorques)();
  if (!SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_type_support_handle.typesupport_identifier) {
    SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &SystemExtTorques__rosidl_typesupport_introspection_c__SystemExtTorques_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
