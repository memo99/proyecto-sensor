// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/HandCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/hand_command__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_HandCommand_finger
{
public:
  Init_HandCommand_finger()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::wessling_hand_msgs::msg::HandCommand finger(::wessling_hand_msgs::msg::HandCommand::_finger_type arg)
  {
    msg_.finger = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::HandCommand msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::HandCommand>()
{
  return wessling_hand_msgs::msg::builder::Init_HandCommand_finger();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__HAND_COMMAND__BUILDER_HPP_
