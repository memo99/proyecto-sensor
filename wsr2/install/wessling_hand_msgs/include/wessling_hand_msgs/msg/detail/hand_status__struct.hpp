// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from wessling_hand_msgs:msg/HandStatus.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__STRUCT_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


// Include directives for member types
// Member 'finger'
#include "wessling_hand_msgs/msg/detail/finger_status__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__wessling_hand_msgs__msg__HandStatus __attribute__((deprecated))
#else
# define DEPRECATED__wessling_hand_msgs__msg__HandStatus __declspec(deprecated)
#endif

namespace wessling_hand_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct HandStatus_
{
  using Type = HandStatus_<ContainerAllocator>;

  explicit HandStatus_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->brake_status = 0.0f;
      this->port_status = false;
    }
  }

  explicit HandStatus_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      this->brake_status = 0.0f;
      this->port_status = false;
    }
  }

  // field types and members
  using _finger_type =
    std::vector<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>>::other>;
  _finger_type finger;
  using _brake_status_type =
    float;
  _brake_status_type brake_status;
  using _port_status_type =
    bool;
  _port_status_type port_status;

  // setters for named parameter idiom
  Type & set__finger(
    const std::vector<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>, typename ContainerAllocator::template rebind<wessling_hand_msgs::msg::FingerStatus_<ContainerAllocator>>::other> & _arg)
  {
    this->finger = _arg;
    return *this;
  }
  Type & set__brake_status(
    const float & _arg)
  {
    this->brake_status = _arg;
    return *this;
  }
  Type & set__port_status(
    const bool & _arg)
  {
    this->port_status = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    wessling_hand_msgs::msg::HandStatus_<ContainerAllocator> *;
  using ConstRawPtr =
    const wessling_hand_msgs::msg::HandStatus_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::HandStatus_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      wessling_hand_msgs::msg::HandStatus_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__wessling_hand_msgs__msg__HandStatus
    std::shared_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__wessling_hand_msgs__msg__HandStatus
    std::shared_ptr<wessling_hand_msgs::msg::HandStatus_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const HandStatus_ & other) const
  {
    if (this->finger != other.finger) {
      return false;
    }
    if (this->brake_status != other.brake_status) {
      return false;
    }
    if (this->port_status != other.port_status) {
      return false;
    }
    return true;
  }
  bool operator!=(const HandStatus_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct HandStatus_

// alias to use template instance with default allocator
using HandStatus =
  wessling_hand_msgs::msg::HandStatus_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__HAND_STATUS__STRUCT_HPP_
