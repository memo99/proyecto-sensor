// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from wessling_hand_msgs:msg/FingerCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_COMMAND__TRAITS_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_COMMAND__TRAITS_HPP_

#include "wessling_hand_msgs/msg/detail/finger_command__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<wessling_hand_msgs::msg::FingerCommand>()
{
  return "wessling_hand_msgs::msg::FingerCommand";
}

template<>
inline const char * name<wessling_hand_msgs::msg::FingerCommand>()
{
  return "wessling_hand_msgs/msg/FingerCommand";
}

template<>
struct has_fixed_size<wessling_hand_msgs::msg::FingerCommand>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<wessling_hand_msgs::msg::FingerCommand>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<wessling_hand_msgs::msg::FingerCommand>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_COMMAND__TRAITS_HPP_
