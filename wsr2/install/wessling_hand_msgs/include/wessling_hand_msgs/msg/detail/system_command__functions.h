// generated from rosidl_generator_c/resource/idl__functions.h.em
// with input from wessling_hand_msgs:msg/SystemCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__FUNCTIONS_H_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_runtime_c/visibility_control.h"
#include "wessling_hand_msgs/msg/rosidl_generator_c__visibility_control.h"

#include "wessling_hand_msgs/msg/detail/system_command__struct.h"

/// Initialize msg/SystemCommand message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * wessling_hand_msgs__msg__SystemCommand
 * )) before or use
 * wessling_hand_msgs__msg__SystemCommand__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
bool
wessling_hand_msgs__msg__SystemCommand__init(wessling_hand_msgs__msg__SystemCommand * msg);

/// Finalize msg/SystemCommand message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__SystemCommand__fini(wessling_hand_msgs__msg__SystemCommand * msg);

/// Create msg/SystemCommand message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * wessling_hand_msgs__msg__SystemCommand__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
wessling_hand_msgs__msg__SystemCommand *
wessling_hand_msgs__msg__SystemCommand__create();

/// Destroy msg/SystemCommand message.
/**
 * It calls
 * wessling_hand_msgs__msg__SystemCommand__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__SystemCommand__destroy(wessling_hand_msgs__msg__SystemCommand * msg);


/// Initialize array of msg/SystemCommand messages.
/**
 * It allocates the memory for the number of elements and calls
 * wessling_hand_msgs__msg__SystemCommand__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
bool
wessling_hand_msgs__msg__SystemCommand__Sequence__init(wessling_hand_msgs__msg__SystemCommand__Sequence * array, size_t size);

/// Finalize array of msg/SystemCommand messages.
/**
 * It calls
 * wessling_hand_msgs__msg__SystemCommand__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__SystemCommand__Sequence__fini(wessling_hand_msgs__msg__SystemCommand__Sequence * array);

/// Create array of msg/SystemCommand messages.
/**
 * It allocates the memory for the array and calls
 * wessling_hand_msgs__msg__SystemCommand__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
wessling_hand_msgs__msg__SystemCommand__Sequence *
wessling_hand_msgs__msg__SystemCommand__Sequence__create(size_t size);

/// Destroy array of msg/SystemCommand messages.
/**
 * It calls
 * wessling_hand_msgs__msg__SystemCommand__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_wessling_hand_msgs
void
wessling_hand_msgs__msg__SystemCommand__Sequence__destroy(wessling_hand_msgs__msg__SystemCommand__Sequence * array);

#ifdef __cplusplus
}
#endif

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__FUNCTIONS_H_
