// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from wessling_hand_msgs:msg/FingerExtTorques.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__BUILDER_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__BUILDER_HPP_

#include "wessling_hand_msgs/msg/detail/finger_ext_torques__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace wessling_hand_msgs
{

namespace msg
{

namespace builder
{

class Init_FingerExtTorques_torque
{
public:
  Init_FingerExtTorques_torque()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::wessling_hand_msgs::msg::FingerExtTorques torque(::wessling_hand_msgs::msg::FingerExtTorques::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::wessling_hand_msgs::msg::FingerExtTorques msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::wessling_hand_msgs::msg::FingerExtTorques>()
{
  return wessling_hand_msgs::msg::builder::Init_FingerExtTorques_torque();
}

}  // namespace wessling_hand_msgs

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__FINGER_EXT_TORQUES__BUILDER_HPP_
