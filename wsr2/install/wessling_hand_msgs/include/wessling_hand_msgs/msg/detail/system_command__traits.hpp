// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from wessling_hand_msgs:msg/SystemCommand.idl
// generated code does not contain a copyright notice

#ifndef WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__TRAITS_HPP_
#define WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__TRAITS_HPP_

#include "wessling_hand_msgs/msg/detail/system_command__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<wessling_hand_msgs::msg::SystemCommand>()
{
  return "wessling_hand_msgs::msg::SystemCommand";
}

template<>
inline const char * name<wessling_hand_msgs::msg::SystemCommand>()
{
  return "wessling_hand_msgs/msg/SystemCommand";
}

template<>
struct has_fixed_size<wessling_hand_msgs::msg::SystemCommand>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<wessling_hand_msgs::msg::SystemCommand>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<wessling_hand_msgs::msg::SystemCommand>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // WESSLING_HAND_MSGS__MSG__DETAIL__SYSTEM_COMMAND__TRAITS_HPP_
