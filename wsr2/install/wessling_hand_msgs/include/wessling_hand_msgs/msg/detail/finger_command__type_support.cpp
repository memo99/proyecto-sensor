// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from wessling_hand_msgs:msg/FingerCommand.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "wessling_hand_msgs/msg/detail/finger_command__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace wessling_hand_msgs
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

void FingerCommand_init_function(
  void * message_memory, rosidl_runtime_cpp::MessageInitialization _init)
{
  new (message_memory) wessling_hand_msgs::msg::FingerCommand(_init);
}

void FingerCommand_fini_function(void * message_memory)
{
  auto typed_message = static_cast<wessling_hand_msgs::msg::FingerCommand *>(message_memory);
  typed_message->~FingerCommand();
}

size_t size_function__FingerCommand__angle(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__FingerCommand__angle(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__FingerCommand__angle(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__FingerCommand__angle(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__FingerCommand__kp(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__FingerCommand__kp(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__FingerCommand__kp(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__FingerCommand__kp(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__FingerCommand__velocity(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__FingerCommand__velocity(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__FingerCommand__velocity(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__FingerCommand__velocity(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__FingerCommand__stiffness(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__FingerCommand__stiffness(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__FingerCommand__stiffness(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__FingerCommand__stiffness(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember FingerCommand_message_member_array[5] = {
  {
    "enable",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::FingerCommand, enable),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr  // resize(index) function pointer
  },
  {
    "angle",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::FingerCommand, angle),  // bytes offset in struct
    nullptr,  // default value
    size_function__FingerCommand__angle,  // size() function pointer
    get_const_function__FingerCommand__angle,  // get_const(index) function pointer
    get_function__FingerCommand__angle,  // get(index) function pointer
    resize_function__FingerCommand__angle  // resize(index) function pointer
  },
  {
    "kp",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::FingerCommand, kp),  // bytes offset in struct
    nullptr,  // default value
    size_function__FingerCommand__kp,  // size() function pointer
    get_const_function__FingerCommand__kp,  // get_const(index) function pointer
    get_function__FingerCommand__kp,  // get(index) function pointer
    resize_function__FingerCommand__kp  // resize(index) function pointer
  },
  {
    "velocity",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::FingerCommand, velocity),  // bytes offset in struct
    nullptr,  // default value
    size_function__FingerCommand__velocity,  // size() function pointer
    get_const_function__FingerCommand__velocity,  // get_const(index) function pointer
    get_function__FingerCommand__velocity,  // get(index) function pointer
    resize_function__FingerCommand__velocity  // resize(index) function pointer
  },
  {
    "stiffness",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(wessling_hand_msgs::msg::FingerCommand, stiffness),  // bytes offset in struct
    nullptr,  // default value
    size_function__FingerCommand__stiffness,  // size() function pointer
    get_const_function__FingerCommand__stiffness,  // get_const(index) function pointer
    get_function__FingerCommand__stiffness,  // get(index) function pointer
    resize_function__FingerCommand__stiffness  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers FingerCommand_message_members = {
  "wessling_hand_msgs::msg",  // message namespace
  "FingerCommand",  // message name
  5,  // number of fields
  sizeof(wessling_hand_msgs::msg::FingerCommand),
  FingerCommand_message_member_array,  // message members
  FingerCommand_init_function,  // function to initialize message memory (memory has to be allocated)
  FingerCommand_fini_function  // function to terminate message instance (will not free memory)
};

static const rosidl_message_type_support_t FingerCommand_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &FingerCommand_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace wessling_hand_msgs


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<wessling_hand_msgs::msg::FingerCommand>()
{
  return &::wessling_hand_msgs::msg::rosidl_typesupport_introspection_cpp::FingerCommand_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, wessling_hand_msgs, msg, FingerCommand)() {
  return &::wessling_hand_msgs::msg::rosidl_typesupport_introspection_cpp::FingerCommand_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
