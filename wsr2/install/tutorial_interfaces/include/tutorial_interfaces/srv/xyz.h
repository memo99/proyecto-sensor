// generated from rosidl_generator_c/resource/idl.h.em
// with input from tutorial_interfaces:srv/XYZ.idl
// generated code does not contain a copyright notice

#ifndef TUTORIAL_INTERFACES__SRV__XYZ_H_
#define TUTORIAL_INTERFACES__SRV__XYZ_H_

#include "tutorial_interfaces/srv/detail/xyz__struct.h"
#include "tutorial_interfaces/srv/detail/xyz__functions.h"
#include "tutorial_interfaces/srv/detail/xyz__type_support.h"

#endif  // TUTORIAL_INTERFACES__SRV__XYZ_H_
