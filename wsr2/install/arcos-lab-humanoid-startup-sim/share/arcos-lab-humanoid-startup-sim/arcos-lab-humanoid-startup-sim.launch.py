import launch
import launch.actions
import launch.substitutions
import launch_ros.actions
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from ament_index_python.packages import get_package_share_directory
import subprocess
import sys

wessling_hand_sim_dir = get_package_share_directory('wessling_hand_sim')
arcos_lab_humanoid_robot_descriptions_dir = get_package_share_directory(
    'arcos-lab-humanoid-robot-descriptions')


# From sublaunch files
# Wessling hands
# Torso *
# Base *
included_launch = launch.actions.IncludeLaunchDescription(
    PythonLaunchDescriptionSource([
        wessling_hand_sim_dir,
        '/wessling_hand_sim.launch.py'
    ]
    )
)

print("Test")


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='false')

    # Run xacro first to generate complete urdf model:
    robot_desc = subprocess.run(
        ["xacro", arcos_lab_humanoid_robot_descriptions_dir+"/urdfs"+"/arcos-lab-humanoid-robot.xacro"], text=True, capture_output=True)

    return launch.LaunchDescription([
        included_launch,
        DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'),
        # Node(
        #     package='tf2_ros',
        #     executable="static_transform_publisher",
        #     arguments=["0", "0.2", "0", "0", "0",
        #                "0", "map", "wessling-hand1-base"]
        # ),
        # kuka arm right
        Node(
            package='kuka_lwr4plus_sim',
            executable='kuka_lwr4plus_sim',
            remappings=[
                ("kuka_lwr4plus_sim/joint_states", "/joint_states")
            ],
            name='kuka_lwr4plus_sim',
            namespace='right',
            output='screen',
            parameters=[{'arm_instance': 'right'}],
        ),
        Node(
            package='kuka_lwr4plus_sim',
            executable='kuka_lwr4plus_sim',
            remappings=[
                ("kuka_lwr4plus_sim/joint_states", "/joint_states")
            ],
            name='kuka_lwr4plus_sim',
            namespace='left',
            output='screen',
            parameters=[{'arm_instance': 'left'}],
        ),
        Node(
            package='arcos_lab_mobile_platform_sim',
            executable='arcos_lab_mobile_platform_sim',
            remappings=[
                ("arcos_lab_mobile_platform_sim/joint_states", "/joint_states")
            ],
            name='arcos_lab_mobile_platform_sim',
            # namespace='humanoid',
            output='screen',
            #parameters=[{'mob_instance': 'humanoid'}],
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time,
                         'robot_description': robot_desc.stdout}],
        ),
    ])
