import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
import subprocess
import sys


wessling_hand_urdf_dir = get_package_share_directory('wessling_hand_urdf')


def generate_launch_description():

    use_sim_time = LaunchConfiguration('use_sim_time', default='false')

    # Run xacro first to generate complete urdf model:
    hands_desc = subprocess.run(
        ["xacro", wessling_hand_urdf_dir+"/urdfs"+"/hands_test.xacro"], text=True, capture_output=True)

    # # Run xacro first to generate complete urdf model:
    # robot_desc_left = subprocess.run(
    #     ["xacro", wessling_hand_urdf_dir+"/urdfs"+"/hand_left.xacro"], text=True, capture_output=True)

    # urdf_file_name = 'urdfs/hand_right.urdf'
    # urdf_right = os.path.join(
    #     get_package_share_directory('wessling_hand_urdf'),
    #     urdf_file_name)
    # with open(urdf_right, 'r') as infp:
    #     robot_desc_right = infp.read()

    # urdf_file_name = 'urdfs/hand_left.urdf'
    # urdf_left = os.path.join(
    #     get_package_share_directory('wessling_hand_urdf'),
    #     urdf_file_name)
    # with open(urdf_left, 'r') as infp:
    #     robot_desc_left = infp.read()

    return LaunchDescription([
        DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'),
        # Node(
        #     package='tf2_ros',
        #     executable="static_transform_publisher",
        #     arguments=["0", "0.2", "0", "0", "0",
        #                "0", "robot_base", "wessling-hand0-base"]
        # ),
        # Node(
        #     package='tf2_ros',
        #     executable="static_transform_publisher",
        #     arguments=["0", "-0.2", "0", "0", "0",
        #                "0", "robot_base", "wessling-hand1-base"]
        # ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher_hands',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time,
                         'robot_description': hands_desc.stdout}],
            #    arguments=[urdf_right]
        ),
        # Node(
        #     package='robot_state_publisher',
        #     executable='robot_state_publisher',
        #     name='robot_state_publisher_left',
        #     output='screen',
        #     parameters=[{'use_sim_time': use_sim_time,
        #                  'robot_description': robot_desc_left.stdout}],
        #     #    arguments=[urdf_left]
        # ),
        Node(
            package='wessling_hand_sim',
            executable='sim',
            remappings=[
                ("/wessling_hand_sim/joint_states", "/joint_states")
            ],
            name='wessling_hand_sim',
            output='screen'),
    ])
