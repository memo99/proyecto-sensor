// generated from rosidl_typesupport_fastrtps_c/resource/idl__rosidl_typesupport_fastrtps_c.h.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice
#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_


#include <stddef.h>
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t get_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Request(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t max_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Request(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable_Request)();

#ifdef __cplusplus
}
#endif

// already included above
// #include <stddef.h>
// already included above
// #include "rosidl_runtime_c/message_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t get_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Response(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
size_t max_serialized_size_arcos_lab_mobile_platform_msgs__srv__MobEnable_Response(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable_Response)();

#ifdef __cplusplus
}
#endif

#include "rosidl_runtime_c/service_type_support_struct.h"
// already included above
// #include "rosidl_typesupport_interface/macros.h"
// already included above
// #include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_arcos_lab_mobile_platform_msgs
const rosidl_service_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__SERVICE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, arcos_lab_mobile_platform_msgs, srv, MobEnable)();

#ifdef __cplusplus
}
#endif

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
