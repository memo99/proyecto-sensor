// generated from rosidl_generator_c/resource/idl__functions.h.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__FUNCTIONS_H_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_runtime_c/visibility_control.h"
#include "arcos_lab_mobile_platform_msgs/msg/rosidl_generator_c__visibility_control.h"

#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__struct.h"

/// Initialize srv/MobEnable message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request
 * )) before or use
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * msg);

/// Finalize srv/MobEnable message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * msg);

/// Create srv/MobEnable message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__create();

/// Destroy srv/MobEnable message.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request * msg);


/// Initialize array of srv/MobEnable messages.
/**
 * It allocates the memory for the number of elements and calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence * array, size_t size);

/// Finalize array of srv/MobEnable messages.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence * array);

/// Create array of srv/MobEnable messages.
/**
 * It allocates the memory for the array and calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__create(size_t size);

/// Destroy array of srv/MobEnable messages.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Request__Sequence * array);

/// Initialize srv/MobEnable message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response
 * )) before or use
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * msg);

/// Finalize srv/MobEnable message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * msg);

/// Create srv/MobEnable message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__create();

/// Destroy srv/MobEnable message.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response * msg);


/// Initialize array of srv/MobEnable messages.
/**
 * It allocates the memory for the number of elements and calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
bool
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__init(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence * array, size_t size);

/// Finalize array of srv/MobEnable messages.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__fini(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence * array);

/// Create array of srv/MobEnable messages.
/**
 * It allocates the memory for the array and calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence *
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__create(size_t size);

/// Destroy array of srv/MobEnable messages.
/**
 * It calls
 * arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_arcos_lab_mobile_platform_msgs
void
arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence__destroy(arcos_lab_mobile_platform_msgs__srv__MobEnable_Response__Sequence * array);

#ifdef __cplusplus
}
#endif

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__FUNCTIONS_H_
