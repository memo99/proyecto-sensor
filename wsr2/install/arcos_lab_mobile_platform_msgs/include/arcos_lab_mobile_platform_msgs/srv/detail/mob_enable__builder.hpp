// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__BUILDER_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__BUILDER_HPP_

#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace arcos_lab_mobile_platform_msgs
{

namespace srv
{

namespace builder
{

class Init_MobEnable_Request_enable
{
public:
  Init_MobEnable_Request_enable()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::arcos_lab_mobile_platform_msgs::srv::MobEnable_Request enable(::arcos_lab_mobile_platform_msgs::srv::MobEnable_Request::_enable_type arg)
  {
    msg_.enable = std::move(arg);
    return std::move(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::srv::MobEnable_Request msg_;
};

}  // namespace builder

}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::arcos_lab_mobile_platform_msgs::srv::MobEnable_Request>()
{
  return arcos_lab_mobile_platform_msgs::srv::builder::Init_MobEnable_Request_enable();
}

}  // namespace arcos_lab_mobile_platform_msgs


namespace arcos_lab_mobile_platform_msgs
{

namespace srv
{

namespace builder
{

class Init_MobEnable_Response_enabled
{
public:
  Init_MobEnable_Response_enabled()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::arcos_lab_mobile_platform_msgs::srv::MobEnable_Response enabled(::arcos_lab_mobile_platform_msgs::srv::MobEnable_Response::_enabled_type arg)
  {
    msg_.enabled = std::move(arg);
    return std::move(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::srv::MobEnable_Response msg_;
};

}  // namespace builder

}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::arcos_lab_mobile_platform_msgs::srv::MobEnable_Response>()
{
  return arcos_lab_mobile_platform_msgs::srv::builder::Init_MobEnable_Response_enabled();
}

}  // namespace arcos_lab_mobile_platform_msgs

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__DETAIL__MOB_ENABLE__BUILDER_HPP_
