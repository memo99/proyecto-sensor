// generated from rosidl_generator_c/resource/idl.h.em
// with input from arcos_lab_mobile_platform_msgs:srv/MobEnable.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__MOB_ENABLE_H_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__MOB_ENABLE_H_

#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__struct.h"
#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__functions.h"
#include "arcos_lab_mobile_platform_msgs/srv/detail/mob_enable__type_support.h"

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__SRV__MOB_ENABLE_H_
