// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_STATUS_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_STATUS_HPP_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__struct.hpp"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__builder.hpp"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__traits.hpp"

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__MOB_STATUS_HPP_
