// generated from rosidl_typesupport_fastrtps_cpp/resource/idl__rosidl_typesupport_fastrtps_cpp.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobCommand.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_

#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "arcos_lab_mobile_platform_msgs/msg/rosidl_typesupport_fastrtps_cpp__visibility_control.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__struct.hpp"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif
#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

#include "fastcdr/Cdr.h"

namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

namespace typesupport_fastrtps_cpp
{

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
cdr_serialize(
  const arcos_lab_mobile_platform_msgs::msg::MobCommand & ros_message,
  eprosima::fastcdr::Cdr & cdr);

bool
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  arcos_lab_mobile_platform_msgs::msg::MobCommand & ros_message);

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
get_serialized_size(
  const arcos_lab_mobile_platform_msgs::msg::MobCommand & ros_message,
  size_t current_alignment);

size_t
ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
max_serialized_size_MobCommand(
  bool & full_bounded,
  size_t current_alignment);

}  // namespace typesupport_fastrtps_cpp

}  // namespace msg

}  // namespace arcos_lab_mobile_platform_msgs

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_CPP_PUBLIC_arcos_lab_mobile_platform_msgs
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_cpp, arcos_lab_mobile_platform_msgs, msg, MobCommand)();

#ifdef __cplusplus
}
#endif

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__ROSIDL_TYPESUPPORT_FASTRTPS_CPP_HPP_
