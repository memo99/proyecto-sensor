// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobStatus.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__BUILDER_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__BUILDER_HPP_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

namespace builder
{

class Init_MobStatus_torque
{
public:
  explicit Init_MobStatus_torque(::arcos_lab_mobile_platform_msgs::msg::MobStatus & msg)
  : msg_(msg)
  {}
  ::arcos_lab_mobile_platform_msgs::msg::MobStatus torque(::arcos_lab_mobile_platform_msgs::msg::MobStatus::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobStatus msg_;
};

class Init_MobStatus_position
{
public:
  explicit Init_MobStatus_position(::arcos_lab_mobile_platform_msgs::msg::MobStatus & msg)
  : msg_(msg)
  {}
  Init_MobStatus_torque position(::arcos_lab_mobile_platform_msgs::msg::MobStatus::_position_type arg)
  {
    msg_.position = std::move(arg);
    return Init_MobStatus_torque(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobStatus msg_;
};

class Init_MobStatus_enabled
{
public:
  Init_MobStatus_enabled()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_MobStatus_position enabled(::arcos_lab_mobile_platform_msgs::msg::MobStatus::_enabled_type arg)
  {
    msg_.enabled = std::move(arg);
    return Init_MobStatus_position(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobStatus msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::arcos_lab_mobile_platform_msgs::msg::MobStatus>()
{
  return arcos_lab_mobile_platform_msgs::msg::builder::Init_MobStatus_enabled();
}

}  // namespace arcos_lab_mobile_platform_msgs

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__BUILDER_HPP_
