// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobCommand.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

void MobCommand_init_function(
  void * message_memory, rosidl_runtime_cpp::MessageInitialization _init)
{
  new (message_memory) arcos_lab_mobile_platform_msgs::msg::MobCommand(_init);
}

void MobCommand_fini_function(void * message_memory)
{
  auto typed_message = static_cast<arcos_lab_mobile_platform_msgs::msg::MobCommand *>(message_memory);
  typed_message->~MobCommand();
}

size_t size_function__MobCommand__angle(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__MobCommand__angle(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__MobCommand__angle(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__MobCommand__angle(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__MobCommand__kp(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__MobCommand__kp(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__MobCommand__kp(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__MobCommand__kp(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__MobCommand__velocity(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__MobCommand__velocity(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__MobCommand__velocity(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__MobCommand__velocity(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__MobCommand__stiffness(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__MobCommand__stiffness(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__MobCommand__stiffness(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__MobCommand__stiffness(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember MobCommand_message_member_array[5] = {
  {
    "enable",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs::msg::MobCommand, enable),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr  // resize(index) function pointer
  },
  {
    "angle",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs::msg::MobCommand, angle),  // bytes offset in struct
    nullptr,  // default value
    size_function__MobCommand__angle,  // size() function pointer
    get_const_function__MobCommand__angle,  // get_const(index) function pointer
    get_function__MobCommand__angle,  // get(index) function pointer
    resize_function__MobCommand__angle  // resize(index) function pointer
  },
  {
    "kp",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs::msg::MobCommand, kp),  // bytes offset in struct
    nullptr,  // default value
    size_function__MobCommand__kp,  // size() function pointer
    get_const_function__MobCommand__kp,  // get_const(index) function pointer
    get_function__MobCommand__kp,  // get(index) function pointer
    resize_function__MobCommand__kp  // resize(index) function pointer
  },
  {
    "velocity",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs::msg::MobCommand, velocity),  // bytes offset in struct
    nullptr,  // default value
    size_function__MobCommand__velocity,  // size() function pointer
    get_const_function__MobCommand__velocity,  // get_const(index) function pointer
    get_function__MobCommand__velocity,  // get(index) function pointer
    resize_function__MobCommand__velocity  // resize(index) function pointer
  },
  {
    "stiffness",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(arcos_lab_mobile_platform_msgs::msg::MobCommand, stiffness),  // bytes offset in struct
    nullptr,  // default value
    size_function__MobCommand__stiffness,  // size() function pointer
    get_const_function__MobCommand__stiffness,  // get_const(index) function pointer
    get_function__MobCommand__stiffness,  // get(index) function pointer
    resize_function__MobCommand__stiffness  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers MobCommand_message_members = {
  "arcos_lab_mobile_platform_msgs::msg",  // message namespace
  "MobCommand",  // message name
  5,  // number of fields
  sizeof(arcos_lab_mobile_platform_msgs::msg::MobCommand),
  MobCommand_message_member_array,  // message members
  MobCommand_init_function,  // function to initialize message memory (memory has to be allocated)
  MobCommand_fini_function  // function to terminate message instance (will not free memory)
};

static const rosidl_message_type_support_t MobCommand_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &MobCommand_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace arcos_lab_mobile_platform_msgs


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<arcos_lab_mobile_platform_msgs::msg::MobCommand>()
{
  return &::arcos_lab_mobile_platform_msgs::msg::rosidl_typesupport_introspection_cpp::MobCommand_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, arcos_lab_mobile_platform_msgs, msg, MobCommand)() {
  return &::arcos_lab_mobile_platform_msgs::msg::rosidl_typesupport_introspection_cpp::MobCommand_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
