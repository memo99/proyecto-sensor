// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobExtTorques.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__BUILDER_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__BUILDER_HPP_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_ext_torques__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

namespace builder
{

class Init_MobExtTorques_torque
{
public:
  Init_MobExtTorques_torque()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::arcos_lab_mobile_platform_msgs::msg::MobExtTorques torque(::arcos_lab_mobile_platform_msgs::msg::MobExtTorques::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobExtTorques msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::arcos_lab_mobile_platform_msgs::msg::MobExtTorques>()
{
  return arcos_lab_mobile_platform_msgs::msg::builder::Init_MobExtTorques_torque();
}

}  // namespace arcos_lab_mobile_platform_msgs

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_EXT_TORQUES__BUILDER_HPP_
