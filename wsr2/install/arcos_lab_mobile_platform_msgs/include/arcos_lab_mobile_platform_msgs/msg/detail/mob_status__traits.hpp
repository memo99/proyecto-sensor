// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobStatus.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__TRAITS_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__TRAITS_HPP_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_status__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<arcos_lab_mobile_platform_msgs::msg::MobStatus>()
{
  return "arcos_lab_mobile_platform_msgs::msg::MobStatus";
}

template<>
inline const char * name<arcos_lab_mobile_platform_msgs::msg::MobStatus>()
{
  return "arcos_lab_mobile_platform_msgs/msg/MobStatus";
}

template<>
struct has_fixed_size<arcos_lab_mobile_platform_msgs::msg::MobStatus>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<arcos_lab_mobile_platform_msgs::msg::MobStatus>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<arcos_lab_mobile_platform_msgs::msg::MobStatus>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_STATUS__TRAITS_HPP_
