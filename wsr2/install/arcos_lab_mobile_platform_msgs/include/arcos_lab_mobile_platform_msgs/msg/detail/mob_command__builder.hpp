// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from arcos_lab_mobile_platform_msgs:msg/MobCommand.idl
// generated code does not contain a copyright notice

#ifndef ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__BUILDER_HPP_
#define ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__BUILDER_HPP_

#include "arcos_lab_mobile_platform_msgs/msg/detail/mob_command__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace arcos_lab_mobile_platform_msgs
{

namespace msg
{

namespace builder
{

class Init_MobCommand_stiffness
{
public:
  explicit Init_MobCommand_stiffness(::arcos_lab_mobile_platform_msgs::msg::MobCommand & msg)
  : msg_(msg)
  {}
  ::arcos_lab_mobile_platform_msgs::msg::MobCommand stiffness(::arcos_lab_mobile_platform_msgs::msg::MobCommand::_stiffness_type arg)
  {
    msg_.stiffness = std::move(arg);
    return std::move(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobCommand msg_;
};

class Init_MobCommand_velocity
{
public:
  explicit Init_MobCommand_velocity(::arcos_lab_mobile_platform_msgs::msg::MobCommand & msg)
  : msg_(msg)
  {}
  Init_MobCommand_stiffness velocity(::arcos_lab_mobile_platform_msgs::msg::MobCommand::_velocity_type arg)
  {
    msg_.velocity = std::move(arg);
    return Init_MobCommand_stiffness(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobCommand msg_;
};

class Init_MobCommand_kp
{
public:
  explicit Init_MobCommand_kp(::arcos_lab_mobile_platform_msgs::msg::MobCommand & msg)
  : msg_(msg)
  {}
  Init_MobCommand_velocity kp(::arcos_lab_mobile_platform_msgs::msg::MobCommand::_kp_type arg)
  {
    msg_.kp = std::move(arg);
    return Init_MobCommand_velocity(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobCommand msg_;
};

class Init_MobCommand_angle
{
public:
  explicit Init_MobCommand_angle(::arcos_lab_mobile_platform_msgs::msg::MobCommand & msg)
  : msg_(msg)
  {}
  Init_MobCommand_kp angle(::arcos_lab_mobile_platform_msgs::msg::MobCommand::_angle_type arg)
  {
    msg_.angle = std::move(arg);
    return Init_MobCommand_kp(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobCommand msg_;
};

class Init_MobCommand_enable
{
public:
  Init_MobCommand_enable()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_MobCommand_angle enable(::arcos_lab_mobile_platform_msgs::msg::MobCommand::_enable_type arg)
  {
    msg_.enable = std::move(arg);
    return Init_MobCommand_angle(msg_);
  }

private:
  ::arcos_lab_mobile_platform_msgs::msg::MobCommand msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::arcos_lab_mobile_platform_msgs::msg::MobCommand>()
{
  return arcos_lab_mobile_platform_msgs::msg::builder::Init_MobCommand_enable();
}

}  // namespace arcos_lab_mobile_platform_msgs

#endif  // ARCOS_LAB_MOBILE_PLATFORM_MSGS__MSG__DETAIL__MOB_COMMAND__BUILDER_HPP_
