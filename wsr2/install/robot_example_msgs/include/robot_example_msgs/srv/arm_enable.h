// generated from rosidl_generator_c/resource/idl.h.em
// with input from robot_example_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__SRV__ARM_ENABLE_H_
#define ROBOT_EXAMPLE_MSGS__SRV__ARM_ENABLE_H_

#include "robot_example_msgs/srv/detail/arm_enable__struct.h"
#include "robot_example_msgs/srv/detail/arm_enable__functions.h"
#include "robot_example_msgs/srv/detail/arm_enable__type_support.h"

#endif  // ROBOT_EXAMPLE_MSGS__SRV__ARM_ENABLE_H_
