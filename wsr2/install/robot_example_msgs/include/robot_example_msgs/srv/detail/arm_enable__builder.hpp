// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from robot_example_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__BUILDER_HPP_
#define ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__BUILDER_HPP_

#include "robot_example_msgs/srv/detail/arm_enable__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace robot_example_msgs
{

namespace srv
{

namespace builder
{

class Init_ArmEnable_Request_enable
{
public:
  Init_ArmEnable_Request_enable()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::robot_example_msgs::srv::ArmEnable_Request enable(::robot_example_msgs::srv::ArmEnable_Request::_enable_type arg)
  {
    msg_.enable = std::move(arg);
    return std::move(msg_);
  }

private:
  ::robot_example_msgs::srv::ArmEnable_Request msg_;
};

}  // namespace builder

}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::robot_example_msgs::srv::ArmEnable_Request>()
{
  return robot_example_msgs::srv::builder::Init_ArmEnable_Request_enable();
}

}  // namespace robot_example_msgs


namespace robot_example_msgs
{

namespace srv
{

namespace builder
{

class Init_ArmEnable_Response_enabled
{
public:
  Init_ArmEnable_Response_enabled()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::robot_example_msgs::srv::ArmEnable_Response enabled(::robot_example_msgs::srv::ArmEnable_Response::_enabled_type arg)
  {
    msg_.enabled = std::move(arg);
    return std::move(msg_);
  }

private:
  ::robot_example_msgs::srv::ArmEnable_Response msg_;
};

}  // namespace builder

}  // namespace srv

template<typename MessageType>
auto build();

template<>
inline
auto build<::robot_example_msgs::srv::ArmEnable_Response>()
{
  return robot_example_msgs::srv::builder::Init_ArmEnable_Response_enabled();
}

}  // namespace robot_example_msgs

#endif  // ROBOT_EXAMPLE_MSGS__SRV__DETAIL__ARM_ENABLE__BUILDER_HPP_
