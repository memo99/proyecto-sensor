// generated from rosidl_generator_c/resource/idl.h.em
// with input from robot_example_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__ARM_EXT_TORQUES_H_
#define ROBOT_EXAMPLE_MSGS__MSG__ARM_EXT_TORQUES_H_

#include "robot_example_msgs/msg/detail/arm_ext_torques__struct.h"
#include "robot_example_msgs/msg/detail/arm_ext_torques__functions.h"
#include "robot_example_msgs/msg/detail/arm_ext_torques__type_support.h"

#endif  // ROBOT_EXAMPLE_MSGS__MSG__ARM_EXT_TORQUES_H_
