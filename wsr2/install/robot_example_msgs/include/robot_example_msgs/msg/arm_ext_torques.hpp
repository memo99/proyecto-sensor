// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__ARM_EXT_TORQUES_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__ARM_EXT_TORQUES_HPP_

#include "robot_example_msgs/msg/detail/arm_ext_torques__struct.hpp"
#include "robot_example_msgs/msg/detail/arm_ext_torques__builder.hpp"
#include "robot_example_msgs/msg/detail/arm_ext_torques__traits.hpp"

#endif  // ROBOT_EXAMPLE_MSGS__MSG__ARM_EXT_TORQUES_HPP_
