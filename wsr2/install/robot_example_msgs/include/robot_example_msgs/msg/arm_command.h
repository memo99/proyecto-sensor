// generated from rosidl_generator_c/resource/idl.h.em
// with input from robot_example_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__ARM_COMMAND_H_
#define ROBOT_EXAMPLE_MSGS__MSG__ARM_COMMAND_H_

#include "robot_example_msgs/msg/detail/arm_command__struct.h"
#include "robot_example_msgs/msg/detail/arm_command__functions.h"
#include "robot_example_msgs/msg/detail/arm_command__type_support.h"

#endif  // ROBOT_EXAMPLE_MSGS__MSG__ARM_COMMAND_H_
