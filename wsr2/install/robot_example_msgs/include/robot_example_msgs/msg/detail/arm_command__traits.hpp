// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from robot_example_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_COMMAND__TRAITS_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_COMMAND__TRAITS_HPP_

#include "robot_example_msgs/msg/detail/arm_command__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<robot_example_msgs::msg::ArmCommand>()
{
  return "robot_example_msgs::msg::ArmCommand";
}

template<>
inline const char * name<robot_example_msgs::msg::ArmCommand>()
{
  return "robot_example_msgs/msg/ArmCommand";
}

template<>
struct has_fixed_size<robot_example_msgs::msg::ArmCommand>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<robot_example_msgs::msg::ArmCommand>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<robot_example_msgs::msg::ArmCommand>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_COMMAND__TRAITS_HPP_
