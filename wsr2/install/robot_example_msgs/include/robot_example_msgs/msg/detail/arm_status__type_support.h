// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from robot_example_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__TYPE_SUPPORT_H_
#define ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "robot_example_msgs/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_robot_example_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  robot_example_msgs,
  msg,
  ArmStatus
)();

#ifdef __cplusplus
}
#endif

#endif  // ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__TYPE_SUPPORT_H_
