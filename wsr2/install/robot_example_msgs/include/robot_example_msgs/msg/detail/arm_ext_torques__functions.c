// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from robot_example_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice
#include "robot_example_msgs/msg/detail/arm_ext_torques__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


// Include directives for member types
// Member `torque`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

bool
robot_example_msgs__msg__ArmExtTorques__init(robot_example_msgs__msg__ArmExtTorques * msg)
{
  if (!msg) {
    return false;
  }
  // torque
  if (!rosidl_runtime_c__float__Sequence__init(&msg->torque, 0)) {
    robot_example_msgs__msg__ArmExtTorques__fini(msg);
    return false;
  }
  return true;
}

void
robot_example_msgs__msg__ArmExtTorques__fini(robot_example_msgs__msg__ArmExtTorques * msg)
{
  if (!msg) {
    return;
  }
  // torque
  rosidl_runtime_c__float__Sequence__fini(&msg->torque);
}

robot_example_msgs__msg__ArmExtTorques *
robot_example_msgs__msg__ArmExtTorques__create()
{
  robot_example_msgs__msg__ArmExtTorques * msg = (robot_example_msgs__msg__ArmExtTorques *)malloc(sizeof(robot_example_msgs__msg__ArmExtTorques));
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(robot_example_msgs__msg__ArmExtTorques));
  bool success = robot_example_msgs__msg__ArmExtTorques__init(msg);
  if (!success) {
    free(msg);
    return NULL;
  }
  return msg;
}

void
robot_example_msgs__msg__ArmExtTorques__destroy(robot_example_msgs__msg__ArmExtTorques * msg)
{
  if (msg) {
    robot_example_msgs__msg__ArmExtTorques__fini(msg);
  }
  free(msg);
}


bool
robot_example_msgs__msg__ArmExtTorques__Sequence__init(robot_example_msgs__msg__ArmExtTorques__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  robot_example_msgs__msg__ArmExtTorques * data = NULL;
  if (size) {
    data = (robot_example_msgs__msg__ArmExtTorques *)calloc(size, sizeof(robot_example_msgs__msg__ArmExtTorques));
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = robot_example_msgs__msg__ArmExtTorques__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        robot_example_msgs__msg__ArmExtTorques__fini(&data[i - 1]);
      }
      free(data);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
robot_example_msgs__msg__ArmExtTorques__Sequence__fini(robot_example_msgs__msg__ArmExtTorques__Sequence * array)
{
  if (!array) {
    return;
  }
  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      robot_example_msgs__msg__ArmExtTorques__fini(&array->data[i]);
    }
    free(array->data);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

robot_example_msgs__msg__ArmExtTorques__Sequence *
robot_example_msgs__msg__ArmExtTorques__Sequence__create(size_t size)
{
  robot_example_msgs__msg__ArmExtTorques__Sequence * array = (robot_example_msgs__msg__ArmExtTorques__Sequence *)malloc(sizeof(robot_example_msgs__msg__ArmExtTorques__Sequence));
  if (!array) {
    return NULL;
  }
  bool success = robot_example_msgs__msg__ArmExtTorques__Sequence__init(array, size);
  if (!success) {
    free(array);
    return NULL;
  }
  return array;
}

void
robot_example_msgs__msg__ArmExtTorques__Sequence__destroy(robot_example_msgs__msg__ArmExtTorques__Sequence * array)
{
  if (array) {
    robot_example_msgs__msg__ArmExtTorques__Sequence__fini(array);
  }
  free(array);
}
