// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from robot_example_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__BUILDER_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__BUILDER_HPP_

#include "robot_example_msgs/msg/detail/arm_ext_torques__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace robot_example_msgs
{

namespace msg
{

namespace builder
{

class Init_ArmExtTorques_torque
{
public:
  Init_ArmExtTorques_torque()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::robot_example_msgs::msg::ArmExtTorques torque(::robot_example_msgs::msg::ArmExtTorques::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::robot_example_msgs::msg::ArmExtTorques msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::robot_example_msgs::msg::ArmExtTorques>()
{
  return robot_example_msgs::msg::builder::Init_ArmExtTorques_torque();
}

}  // namespace robot_example_msgs

#endif  // ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__BUILDER_HPP_
