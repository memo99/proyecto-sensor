// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from robot_example_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "robot_example_msgs/msg/detail/arm_status__rosidl_typesupport_introspection_c.h"
#include "robot_example_msgs/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "robot_example_msgs/msg/detail/arm_status__functions.h"
#include "robot_example_msgs/msg/detail/arm_status__struct.h"


// Include directives for member types
// Member `position`
// Member `torque`
#include "rosidl_runtime_c/primitives_sequence_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  robot_example_msgs__msg__ArmStatus__init(message_memory);
}

void ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_fini_function(void * message_memory)
{
  robot_example_msgs__msg__ArmStatus__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_member_array[3] = {
  {
    "enabled",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(robot_example_msgs__msg__ArmStatus, enabled),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "position",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(robot_example_msgs__msg__ArmStatus, position),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "torque",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(robot_example_msgs__msg__ArmStatus, torque),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_members = {
  "robot_example_msgs__msg",  // message namespace
  "ArmStatus",  // message name
  3,  // number of fields
  sizeof(robot_example_msgs__msg__ArmStatus),
  ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_member_array,  // message members
  ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_init_function,  // function to initialize message memory (memory has to be allocated)
  ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_type_support_handle = {
  0,
  &ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_robot_example_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, robot_example_msgs, msg, ArmStatus)() {
  if (!ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_type_support_handle.typesupport_identifier) {
    ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &ArmStatus__rosidl_typesupport_introspection_c__ArmStatus_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
