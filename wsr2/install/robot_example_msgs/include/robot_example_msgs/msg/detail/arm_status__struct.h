// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from robot_example_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__STRUCT_H_
#define ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'position'
// Member 'torque'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in msg/ArmStatus in the package robot_example_msgs.
typedef struct robot_example_msgs__msg__ArmStatus
{
  bool enabled;
  rosidl_runtime_c__float__Sequence position;
  rosidl_runtime_c__float__Sequence torque;
} robot_example_msgs__msg__ArmStatus;

// Struct for a sequence of robot_example_msgs__msg__ArmStatus.
typedef struct robot_example_msgs__msg__ArmStatus__Sequence
{
  robot_example_msgs__msg__ArmStatus * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} robot_example_msgs__msg__ArmStatus__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ROBOT_EXAMPLE_MSGS__MSG__DETAIL__ARM_STATUS__STRUCT_H_
