// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef ROBOT_EXAMPLE_MSGS__MSG__ARM_COMMAND_HPP_
#define ROBOT_EXAMPLE_MSGS__MSG__ARM_COMMAND_HPP_

#include "robot_example_msgs/msg/detail/arm_command__struct.hpp"
#include "robot_example_msgs/msg/detail/arm_command__builder.hpp"
#include "robot_example_msgs/msg/detail/arm_command__traits.hpp"

#endif  // ROBOT_EXAMPLE_MSGS__MSG__ARM_COMMAND_HPP_
