// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from kuka_lwr4plus_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__STRUCT_H_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'angle'
// Member 'kp'
// Member 'velocity'
// Member 'stiffness'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in msg/ArmCommand in the package kuka_lwr4plus_msgs.
typedef struct kuka_lwr4plus_msgs__msg__ArmCommand
{
  bool enable;
  rosidl_runtime_c__float__Sequence angle;
  rosidl_runtime_c__float__Sequence kp;
  rosidl_runtime_c__float__Sequence velocity;
  rosidl_runtime_c__float__Sequence stiffness;
} kuka_lwr4plus_msgs__msg__ArmCommand;

// Struct for a sequence of kuka_lwr4plus_msgs__msg__ArmCommand.
typedef struct kuka_lwr4plus_msgs__msg__ArmCommand__Sequence
{
  kuka_lwr4plus_msgs__msg__ArmCommand * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} kuka_lwr4plus_msgs__msg__ArmCommand__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__STRUCT_H_
