// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from kuka_lwr4plus_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__BUILDER_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__BUILDER_HPP_

#include "kuka_lwr4plus_msgs/msg/detail/arm_status__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace kuka_lwr4plus_msgs
{

namespace msg
{

namespace builder
{

class Init_ArmStatus_torque
{
public:
  explicit Init_ArmStatus_torque(::kuka_lwr4plus_msgs::msg::ArmStatus & msg)
  : msg_(msg)
  {}
  ::kuka_lwr4plus_msgs::msg::ArmStatus torque(::kuka_lwr4plus_msgs::msg::ArmStatus::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::kuka_lwr4plus_msgs::msg::ArmStatus msg_;
};

class Init_ArmStatus_position
{
public:
  explicit Init_ArmStatus_position(::kuka_lwr4plus_msgs::msg::ArmStatus & msg)
  : msg_(msg)
  {}
  Init_ArmStatus_torque position(::kuka_lwr4plus_msgs::msg::ArmStatus::_position_type arg)
  {
    msg_.position = std::move(arg);
    return Init_ArmStatus_torque(msg_);
  }

private:
  ::kuka_lwr4plus_msgs::msg::ArmStatus msg_;
};

class Init_ArmStatus_enabled
{
public:
  Init_ArmStatus_enabled()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_ArmStatus_position enabled(::kuka_lwr4plus_msgs::msg::ArmStatus::_enabled_type arg)
  {
    msg_.enabled = std::move(arg);
    return Init_ArmStatus_position(msg_);
  }

private:
  ::kuka_lwr4plus_msgs::msg::ArmStatus msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::kuka_lwr4plus_msgs::msg::ArmStatus>()
{
  return kuka_lwr4plus_msgs::msg::builder::Init_ArmStatus_enabled();
}

}  // namespace kuka_lwr4plus_msgs

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__BUILDER_HPP_
