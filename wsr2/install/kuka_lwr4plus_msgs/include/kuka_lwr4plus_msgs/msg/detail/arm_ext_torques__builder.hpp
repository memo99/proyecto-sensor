// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from kuka_lwr4plus_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__BUILDER_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__BUILDER_HPP_

#include "kuka_lwr4plus_msgs/msg/detail/arm_ext_torques__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace kuka_lwr4plus_msgs
{

namespace msg
{

namespace builder
{

class Init_ArmExtTorques_torque
{
public:
  Init_ArmExtTorques_torque()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  ::kuka_lwr4plus_msgs::msg::ArmExtTorques torque(::kuka_lwr4plus_msgs::msg::ArmExtTorques::_torque_type arg)
  {
    msg_.torque = std::move(arg);
    return std::move(msg_);
  }

private:
  ::kuka_lwr4plus_msgs::msg::ArmExtTorques msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::kuka_lwr4plus_msgs::msg::ArmExtTorques>()
{
  return kuka_lwr4plus_msgs::msg::builder::Init_ArmExtTorques_torque();
}

}  // namespace kuka_lwr4plus_msgs

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__BUILDER_HPP_
