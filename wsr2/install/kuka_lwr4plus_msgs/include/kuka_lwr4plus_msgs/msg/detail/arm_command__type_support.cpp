// generated from rosidl_typesupport_introspection_cpp/resource/idl__type_support.cpp.em
// with input from kuka_lwr4plus_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice

#include "array"
#include "cstddef"
#include "string"
#include "vector"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_cpp/message_type_support.hpp"
#include "rosidl_typesupport_interface/macros.h"
#include "kuka_lwr4plus_msgs/msg/detail/arm_command__struct.hpp"
#include "rosidl_typesupport_introspection_cpp/field_types.hpp"
#include "rosidl_typesupport_introspection_cpp/identifier.hpp"
#include "rosidl_typesupport_introspection_cpp/message_introspection.hpp"
#include "rosidl_typesupport_introspection_cpp/message_type_support_decl.hpp"
#include "rosidl_typesupport_introspection_cpp/visibility_control.h"

namespace kuka_lwr4plus_msgs
{

namespace msg
{

namespace rosidl_typesupport_introspection_cpp
{

void ArmCommand_init_function(
  void * message_memory, rosidl_runtime_cpp::MessageInitialization _init)
{
  new (message_memory) kuka_lwr4plus_msgs::msg::ArmCommand(_init);
}

void ArmCommand_fini_function(void * message_memory)
{
  auto typed_message = static_cast<kuka_lwr4plus_msgs::msg::ArmCommand *>(message_memory);
  typed_message->~ArmCommand();
}

size_t size_function__ArmCommand__angle(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__ArmCommand__angle(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__ArmCommand__angle(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__ArmCommand__angle(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__ArmCommand__kp(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__ArmCommand__kp(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__ArmCommand__kp(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__ArmCommand__kp(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__ArmCommand__velocity(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__ArmCommand__velocity(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__ArmCommand__velocity(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__ArmCommand__velocity(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

size_t size_function__ArmCommand__stiffness(const void * untyped_member)
{
  const auto * member = reinterpret_cast<const std::vector<float> *>(untyped_member);
  return member->size();
}

const void * get_const_function__ArmCommand__stiffness(const void * untyped_member, size_t index)
{
  const auto & member =
    *reinterpret_cast<const std::vector<float> *>(untyped_member);
  return &member[index];
}

void * get_function__ArmCommand__stiffness(void * untyped_member, size_t index)
{
  auto & member =
    *reinterpret_cast<std::vector<float> *>(untyped_member);
  return &member[index];
}

void resize_function__ArmCommand__stiffness(void * untyped_member, size_t size)
{
  auto * member =
    reinterpret_cast<std::vector<float> *>(untyped_member);
  member->resize(size);
}

static const ::rosidl_typesupport_introspection_cpp::MessageMember ArmCommand_message_member_array[5] = {
  {
    "enable",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_BOOLEAN,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(kuka_lwr4plus_msgs::msg::ArmCommand, enable),  // bytes offset in struct
    nullptr,  // default value
    nullptr,  // size() function pointer
    nullptr,  // get_const(index) function pointer
    nullptr,  // get(index) function pointer
    nullptr  // resize(index) function pointer
  },
  {
    "angle",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(kuka_lwr4plus_msgs::msg::ArmCommand, angle),  // bytes offset in struct
    nullptr,  // default value
    size_function__ArmCommand__angle,  // size() function pointer
    get_const_function__ArmCommand__angle,  // get_const(index) function pointer
    get_function__ArmCommand__angle,  // get(index) function pointer
    resize_function__ArmCommand__angle  // resize(index) function pointer
  },
  {
    "kp",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(kuka_lwr4plus_msgs::msg::ArmCommand, kp),  // bytes offset in struct
    nullptr,  // default value
    size_function__ArmCommand__kp,  // size() function pointer
    get_const_function__ArmCommand__kp,  // get_const(index) function pointer
    get_function__ArmCommand__kp,  // get(index) function pointer
    resize_function__ArmCommand__kp  // resize(index) function pointer
  },
  {
    "velocity",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(kuka_lwr4plus_msgs::msg::ArmCommand, velocity),  // bytes offset in struct
    nullptr,  // default value
    size_function__ArmCommand__velocity,  // size() function pointer
    get_const_function__ArmCommand__velocity,  // get_const(index) function pointer
    get_function__ArmCommand__velocity,  // get(index) function pointer
    resize_function__ArmCommand__velocity  // resize(index) function pointer
  },
  {
    "stiffness",  // name
    ::rosidl_typesupport_introspection_cpp::ROS_TYPE_FLOAT,  // type
    0,  // upper bound of string
    nullptr,  // members of sub message
    true,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(kuka_lwr4plus_msgs::msg::ArmCommand, stiffness),  // bytes offset in struct
    nullptr,  // default value
    size_function__ArmCommand__stiffness,  // size() function pointer
    get_const_function__ArmCommand__stiffness,  // get_const(index) function pointer
    get_function__ArmCommand__stiffness,  // get(index) function pointer
    resize_function__ArmCommand__stiffness  // resize(index) function pointer
  }
};

static const ::rosidl_typesupport_introspection_cpp::MessageMembers ArmCommand_message_members = {
  "kuka_lwr4plus_msgs::msg",  // message namespace
  "ArmCommand",  // message name
  5,  // number of fields
  sizeof(kuka_lwr4plus_msgs::msg::ArmCommand),
  ArmCommand_message_member_array,  // message members
  ArmCommand_init_function,  // function to initialize message memory (memory has to be allocated)
  ArmCommand_fini_function  // function to terminate message instance (will not free memory)
};

static const rosidl_message_type_support_t ArmCommand_message_type_support_handle = {
  ::rosidl_typesupport_introspection_cpp::typesupport_identifier,
  &ArmCommand_message_members,
  get_message_typesupport_handle_function,
};

}  // namespace rosidl_typesupport_introspection_cpp

}  // namespace msg

}  // namespace kuka_lwr4plus_msgs


namespace rosidl_typesupport_introspection_cpp
{

template<>
ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
get_message_type_support_handle<kuka_lwr4plus_msgs::msg::ArmCommand>()
{
  return &::kuka_lwr4plus_msgs::msg::rosidl_typesupport_introspection_cpp::ArmCommand_message_type_support_handle;
}

}  // namespace rosidl_typesupport_introspection_cpp

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_INTROSPECTION_CPP_PUBLIC
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_cpp, kuka_lwr4plus_msgs, msg, ArmCommand)() {
  return &::kuka_lwr4plus_msgs::msg::rosidl_typesupport_introspection_cpp::ArmCommand_message_type_support_handle;
}

#ifdef __cplusplus
}
#endif
