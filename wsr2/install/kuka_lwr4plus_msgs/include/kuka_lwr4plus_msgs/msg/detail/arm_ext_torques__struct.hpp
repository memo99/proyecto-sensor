// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from kuka_lwr4plus_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__STRUCT_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__kuka_lwr4plus_msgs__msg__ArmExtTorques __attribute__((deprecated))
#else
# define DEPRECATED__kuka_lwr4plus_msgs__msg__ArmExtTorques __declspec(deprecated)
#endif

namespace kuka_lwr4plus_msgs
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct ArmExtTorques_
{
  using Type = ArmExtTorques_<ContainerAllocator>;

  explicit ArmExtTorques_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit ArmExtTorques_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _torque_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _torque_type torque;

  // setters for named parameter idiom
  Type & set__torque(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->torque = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator> *;
  using ConstRawPtr =
    const kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__kuka_lwr4plus_msgs__msg__ArmExtTorques
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__kuka_lwr4plus_msgs__msg__ArmExtTorques
    std::shared_ptr<kuka_lwr4plus_msgs::msg::ArmExtTorques_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const ArmExtTorques_ & other) const
  {
    if (this->torque != other.torque) {
      return false;
    }
    return true;
  }
  bool operator!=(const ArmExtTorques_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct ArmExtTorques_

// alias to use template instance with default allocator
using ArmExtTorques =
  kuka_lwr4plus_msgs::msg::ArmExtTorques_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace kuka_lwr4plus_msgs

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_EXT_TORQUES__STRUCT_HPP_
