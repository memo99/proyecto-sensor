// generated from rosidl_generator_c/resource/idl__functions.h.em
// with input from kuka_lwr4plus_msgs:msg/ArmStatus.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__FUNCTIONS_H_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_runtime_c/visibility_control.h"
#include "kuka_lwr4plus_msgs/msg/rosidl_generator_c__visibility_control.h"

#include "kuka_lwr4plus_msgs/msg/detail/arm_status__struct.h"

/// Initialize msg/ArmStatus message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * kuka_lwr4plus_msgs__msg__ArmStatus
 * )) before or use
 * kuka_lwr4plus_msgs__msg__ArmStatus__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
bool
kuka_lwr4plus_msgs__msg__ArmStatus__init(kuka_lwr4plus_msgs__msg__ArmStatus * msg);

/// Finalize msg/ArmStatus message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__msg__ArmStatus__fini(kuka_lwr4plus_msgs__msg__ArmStatus * msg);

/// Create msg/ArmStatus message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * kuka_lwr4plus_msgs__msg__ArmStatus__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
kuka_lwr4plus_msgs__msg__ArmStatus *
kuka_lwr4plus_msgs__msg__ArmStatus__create();

/// Destroy msg/ArmStatus message.
/**
 * It calls
 * kuka_lwr4plus_msgs__msg__ArmStatus__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__msg__ArmStatus__destroy(kuka_lwr4plus_msgs__msg__ArmStatus * msg);


/// Initialize array of msg/ArmStatus messages.
/**
 * It allocates the memory for the number of elements and calls
 * kuka_lwr4plus_msgs__msg__ArmStatus__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
bool
kuka_lwr4plus_msgs__msg__ArmStatus__Sequence__init(kuka_lwr4plus_msgs__msg__ArmStatus__Sequence * array, size_t size);

/// Finalize array of msg/ArmStatus messages.
/**
 * It calls
 * kuka_lwr4plus_msgs__msg__ArmStatus__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__msg__ArmStatus__Sequence__fini(kuka_lwr4plus_msgs__msg__ArmStatus__Sequence * array);

/// Create array of msg/ArmStatus messages.
/**
 * It allocates the memory for the array and calls
 * kuka_lwr4plus_msgs__msg__ArmStatus__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
kuka_lwr4plus_msgs__msg__ArmStatus__Sequence *
kuka_lwr4plus_msgs__msg__ArmStatus__Sequence__create(size_t size);

/// Destroy array of msg/ArmStatus messages.
/**
 * It calls
 * kuka_lwr4plus_msgs__msg__ArmStatus__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__msg__ArmStatus__Sequence__destroy(kuka_lwr4plus_msgs__msg__ArmStatus__Sequence * array);

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_STATUS__FUNCTIONS_H_
