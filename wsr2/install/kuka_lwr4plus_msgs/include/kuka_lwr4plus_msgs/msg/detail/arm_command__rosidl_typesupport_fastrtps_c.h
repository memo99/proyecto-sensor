// generated from rosidl_typesupport_fastrtps_c/resource/idl__rosidl_typesupport_fastrtps_c.h.em
// with input from kuka_lwr4plus_msgs:msg/ArmCommand.idl
// generated code does not contain a copyright notice
#ifndef KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
#define KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_


#include <stddef.h>
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "kuka_lwr4plus_msgs/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_kuka_lwr4plus_msgs
size_t get_serialized_size_kuka_lwr4plus_msgs__msg__ArmCommand(
  const void * untyped_ros_message,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_kuka_lwr4plus_msgs
size_t max_serialized_size_kuka_lwr4plus_msgs__msg__ArmCommand(
  bool & full_bounded,
  size_t current_alignment);

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_kuka_lwr4plus_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, kuka_lwr4plus_msgs, msg, ArmCommand)();

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__MSG__DETAIL__ARM_COMMAND__ROSIDL_TYPESUPPORT_FASTRTPS_C_H_
