// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__ARM_STATUS_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__ARM_STATUS_HPP_

#include "kuka_lwr4plus_msgs/msg/detail/arm_status__struct.hpp"
#include "kuka_lwr4plus_msgs/msg/detail/arm_status__builder.hpp"
#include "kuka_lwr4plus_msgs/msg/detail/arm_status__traits.hpp"

#endif  // KUKA_LWR4PLUS_MSGS__MSG__ARM_STATUS_HPP_
