// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__ARM_EXT_TORQUES_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__ARM_EXT_TORQUES_HPP_

#include "kuka_lwr4plus_msgs/msg/detail/arm_ext_torques__struct.hpp"
#include "kuka_lwr4plus_msgs/msg/detail/arm_ext_torques__builder.hpp"
#include "kuka_lwr4plus_msgs/msg/detail/arm_ext_torques__traits.hpp"

#endif  // KUKA_LWR4PLUS_MSGS__MSG__ARM_EXT_TORQUES_HPP_
