// generated from rosidl_generator_c/resource/idl.h.em
// with input from kuka_lwr4plus_msgs:msg/ArmExtTorques.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__ARM_EXT_TORQUES_H_
#define KUKA_LWR4PLUS_MSGS__MSG__ARM_EXT_TORQUES_H_

#include "kuka_lwr4plus_msgs/msg/detail/arm_ext_torques__struct.h"
#include "kuka_lwr4plus_msgs/msg/detail/arm_ext_torques__functions.h"
#include "kuka_lwr4plus_msgs/msg/detail/arm_ext_torques__type_support.h"

#endif  // KUKA_LWR4PLUS_MSGS__MSG__ARM_EXT_TORQUES_H_
