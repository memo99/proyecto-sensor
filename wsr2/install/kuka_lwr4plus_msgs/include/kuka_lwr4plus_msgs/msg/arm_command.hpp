// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__MSG__ARM_COMMAND_HPP_
#define KUKA_LWR4PLUS_MSGS__MSG__ARM_COMMAND_HPP_

#include "kuka_lwr4plus_msgs/msg/detail/arm_command__struct.hpp"
#include "kuka_lwr4plus_msgs/msg/detail/arm_command__builder.hpp"
#include "kuka_lwr4plus_msgs/msg/detail/arm_command__traits.hpp"

#endif  // KUKA_LWR4PLUS_MSGS__MSG__ARM_COMMAND_HPP_
