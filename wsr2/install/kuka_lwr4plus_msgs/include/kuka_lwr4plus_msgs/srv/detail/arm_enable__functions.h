// generated from rosidl_generator_c/resource/idl__functions.h.em
// with input from kuka_lwr4plus_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__FUNCTIONS_H_
#define KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__FUNCTIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "rosidl_runtime_c/visibility_control.h"
#include "kuka_lwr4plus_msgs/msg/rosidl_generator_c__visibility_control.h"

#include "kuka_lwr4plus_msgs/srv/detail/arm_enable__struct.h"

/// Initialize srv/ArmEnable message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request
 * )) before or use
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
bool
kuka_lwr4plus_msgs__srv__ArmEnable_Request__init(kuka_lwr4plus_msgs__srv__ArmEnable_Request * msg);

/// Finalize srv/ArmEnable message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Request__fini(kuka_lwr4plus_msgs__srv__ArmEnable_Request * msg);

/// Create srv/ArmEnable message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
kuka_lwr4plus_msgs__srv__ArmEnable_Request *
kuka_lwr4plus_msgs__srv__ArmEnable_Request__create();

/// Destroy srv/ArmEnable message.
/**
 * It calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Request__destroy(kuka_lwr4plus_msgs__srv__ArmEnable_Request * msg);


/// Initialize array of srv/ArmEnable messages.
/**
 * It allocates the memory for the number of elements and calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
bool
kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence__init(kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence * array, size_t size);

/// Finalize array of srv/ArmEnable messages.
/**
 * It calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence__fini(kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence * array);

/// Create array of srv/ArmEnable messages.
/**
 * It allocates the memory for the array and calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence *
kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence__create(size_t size);

/// Destroy array of srv/ArmEnable messages.
/**
 * It calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence__destroy(kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence * array);

/// Initialize srv/ArmEnable message.
/**
 * If the init function is called twice for the same message without
 * calling fini inbetween previously allocated memory will be leaked.
 * \param[in,out] msg The previously allocated message pointer.
 * Fields without a default value will not be initialized by this function.
 * You might want to call memset(msg, 0, sizeof(
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response
 * )) before or use
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response__create()
 * to allocate and initialize the message.
 * \return true if initialization was successful, otherwise false
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
bool
kuka_lwr4plus_msgs__srv__ArmEnable_Response__init(kuka_lwr4plus_msgs__srv__ArmEnable_Response * msg);

/// Finalize srv/ArmEnable message.
/**
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Response__fini(kuka_lwr4plus_msgs__srv__ArmEnable_Response * msg);

/// Create srv/ArmEnable message.
/**
 * It allocates the memory for the message, sets the memory to zero, and
 * calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response__init().
 * \return The pointer to the initialized message if successful,
 * otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
kuka_lwr4plus_msgs__srv__ArmEnable_Response *
kuka_lwr4plus_msgs__srv__ArmEnable_Response__create();

/// Destroy srv/ArmEnable message.
/**
 * It calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response__fini()
 * and frees the memory of the message.
 * \param[in,out] msg The allocated message pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Response__destroy(kuka_lwr4plus_msgs__srv__ArmEnable_Response * msg);


/// Initialize array of srv/ArmEnable messages.
/**
 * It allocates the memory for the number of elements and calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response__init()
 * for each element of the array.
 * \param[in,out] array The allocated array pointer.
 * \param[in] size The size / capacity of the array.
 * \return true if initialization was successful, otherwise false
 * If the array pointer is valid and the size is zero it is guaranteed
 # to return true.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
bool
kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence__init(kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence * array, size_t size);

/// Finalize array of srv/ArmEnable messages.
/**
 * It calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response__fini()
 * for each element of the array and frees the memory for the number of
 * elements.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence__fini(kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence * array);

/// Create array of srv/ArmEnable messages.
/**
 * It allocates the memory for the array and calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence__init().
 * \param[in] size The size / capacity of the array.
 * \return The pointer to the initialized array if successful, otherwise NULL
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence *
kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence__create(size_t size);

/// Destroy array of srv/ArmEnable messages.
/**
 * It calls
 * kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence__fini()
 * on the array,
 * and frees the memory of the array.
 * \param[in,out] array The initialized array pointer.
 */
ROSIDL_GENERATOR_C_PUBLIC_kuka_lwr4plus_msgs
void
kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence__destroy(kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence * array);

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__FUNCTIONS_H_
