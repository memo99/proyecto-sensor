// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from kuka_lwr4plus_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_HPP_
#define KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_HPP_

#include <rosidl_runtime_cpp/bounded_vector.hpp>
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>


#ifndef _WIN32
# define DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Request __attribute__((deprecated))
#else
# define DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Request __declspec(deprecated)
#endif

namespace kuka_lwr4plus_msgs
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct ArmEnable_Request_
{
  using Type = ArmEnable_Request_<ContainerAllocator>;

  explicit ArmEnable_Request_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit ArmEnable_Request_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _enable_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _enable_type enable;

  // setters for named parameter idiom
  Type & set__enable(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->enable = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator> *;
  using ConstRawPtr =
    const kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Request
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Request
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Request_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const ArmEnable_Request_ & other) const
  {
    if (this->enable != other.enable) {
      return false;
    }
    return true;
  }
  bool operator!=(const ArmEnable_Request_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct ArmEnable_Request_

// alias to use template instance with default allocator
using ArmEnable_Request =
  kuka_lwr4plus_msgs::srv::ArmEnable_Request_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace kuka_lwr4plus_msgs


#ifndef _WIN32
# define DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Response __attribute__((deprecated))
#else
# define DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Response __declspec(deprecated)
#endif

namespace kuka_lwr4plus_msgs
{

namespace srv
{

// message struct
template<class ContainerAllocator>
struct ArmEnable_Response_
{
  using Type = ArmEnable_Response_<ContainerAllocator>;

  explicit ArmEnable_Response_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
  }

  explicit ArmEnable_Response_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    (void)_init;
    (void)_alloc;
  }

  // field types and members
  using _enabled_type =
    std::vector<float, typename ContainerAllocator::template rebind<float>::other>;
  _enabled_type enabled;

  // setters for named parameter idiom
  Type & set__enabled(
    const std::vector<float, typename ContainerAllocator::template rebind<float>::other> & _arg)
  {
    this->enabled = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator> *;
  using ConstRawPtr =
    const kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Response
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__kuka_lwr4plus_msgs__srv__ArmEnable_Response
    std::shared_ptr<kuka_lwr4plus_msgs::srv::ArmEnable_Response_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const ArmEnable_Response_ & other) const
  {
    if (this->enabled != other.enabled) {
      return false;
    }
    return true;
  }
  bool operator!=(const ArmEnable_Response_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct ArmEnable_Response_

// alias to use template instance with default allocator
using ArmEnable_Response =
  kuka_lwr4plus_msgs::srv::ArmEnable_Response_<std::allocator<void>>;

// constant definitions

}  // namespace srv

}  // namespace kuka_lwr4plus_msgs

namespace kuka_lwr4plus_msgs
{

namespace srv
{

struct ArmEnable
{
  using Request = kuka_lwr4plus_msgs::srv::ArmEnable_Request;
  using Response = kuka_lwr4plus_msgs::srv::ArmEnable_Response;
};

}  // namespace srv

}  // namespace kuka_lwr4plus_msgs

#endif  // KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_HPP_
