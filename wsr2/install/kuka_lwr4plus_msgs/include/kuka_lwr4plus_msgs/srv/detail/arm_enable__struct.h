// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from kuka_lwr4plus_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_H_
#define KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'enable'
#include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/ArmEnable in the package kuka_lwr4plus_msgs.
typedef struct kuka_lwr4plus_msgs__srv__ArmEnable_Request
{
  rosidl_runtime_c__float__Sequence enable;
} kuka_lwr4plus_msgs__srv__ArmEnable_Request;

// Struct for a sequence of kuka_lwr4plus_msgs__srv__ArmEnable_Request.
typedef struct kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence
{
  kuka_lwr4plus_msgs__srv__ArmEnable_Request * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} kuka_lwr4plus_msgs__srv__ArmEnable_Request__Sequence;


// Constants defined in the message

// Include directives for member types
// Member 'enabled'
// already included above
// #include "rosidl_runtime_c/primitives_sequence.h"

// Struct defined in srv/ArmEnable in the package kuka_lwr4plus_msgs.
typedef struct kuka_lwr4plus_msgs__srv__ArmEnable_Response
{
  rosidl_runtime_c__float__Sequence enabled;
} kuka_lwr4plus_msgs__srv__ArmEnable_Response;

// Struct for a sequence of kuka_lwr4plus_msgs__srv__ArmEnable_Response.
typedef struct kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence
{
  kuka_lwr4plus_msgs__srv__ArmEnable_Response * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} kuka_lwr4plus_msgs__srv__ArmEnable_Response__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__STRUCT_H_
