// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from kuka_lwr4plus_msgs:srv/ArmEnable.idl
// generated code does not contain a copyright notice

#ifndef KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__TRAITS_HPP_
#define KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__TRAITS_HPP_

#include "kuka_lwr4plus_msgs/srv/detail/arm_enable__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<kuka_lwr4plus_msgs::srv::ArmEnable_Request>()
{
  return "kuka_lwr4plus_msgs::srv::ArmEnable_Request";
}

template<>
inline const char * name<kuka_lwr4plus_msgs::srv::ArmEnable_Request>()
{
  return "kuka_lwr4plus_msgs/srv/ArmEnable_Request";
}

template<>
struct has_fixed_size<kuka_lwr4plus_msgs::srv::ArmEnable_Request>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<kuka_lwr4plus_msgs::srv::ArmEnable_Request>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<kuka_lwr4plus_msgs::srv::ArmEnable_Request>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<kuka_lwr4plus_msgs::srv::ArmEnable_Response>()
{
  return "kuka_lwr4plus_msgs::srv::ArmEnable_Response";
}

template<>
inline const char * name<kuka_lwr4plus_msgs::srv::ArmEnable_Response>()
{
  return "kuka_lwr4plus_msgs/srv/ArmEnable_Response";
}

template<>
struct has_fixed_size<kuka_lwr4plus_msgs::srv::ArmEnable_Response>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<kuka_lwr4plus_msgs::srv::ArmEnable_Response>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<kuka_lwr4plus_msgs::srv::ArmEnable_Response>
  : std::true_type {};

}  // namespace rosidl_generator_traits

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<kuka_lwr4plus_msgs::srv::ArmEnable>()
{
  return "kuka_lwr4plus_msgs::srv::ArmEnable";
}

template<>
inline const char * name<kuka_lwr4plus_msgs::srv::ArmEnable>()
{
  return "kuka_lwr4plus_msgs/srv/ArmEnable";
}

template<>
struct has_fixed_size<kuka_lwr4plus_msgs::srv::ArmEnable>
  : std::integral_constant<
    bool,
    has_fixed_size<kuka_lwr4plus_msgs::srv::ArmEnable_Request>::value &&
    has_fixed_size<kuka_lwr4plus_msgs::srv::ArmEnable_Response>::value
  >
{
};

template<>
struct has_bounded_size<kuka_lwr4plus_msgs::srv::ArmEnable>
  : std::integral_constant<
    bool,
    has_bounded_size<kuka_lwr4plus_msgs::srv::ArmEnable_Request>::value &&
    has_bounded_size<kuka_lwr4plus_msgs::srv::ArmEnable_Response>::value
  >
{
};

template<>
struct is_service<kuka_lwr4plus_msgs::srv::ArmEnable>
  : std::true_type
{
};

template<>
struct is_service_request<kuka_lwr4plus_msgs::srv::ArmEnable_Request>
  : std::true_type
{
};

template<>
struct is_service_response<kuka_lwr4plus_msgs::srv::ArmEnable_Response>
  : std::true_type
{
};

}  // namespace rosidl_generator_traits

#endif  // KUKA_LWR4PLUS_MSGS__SRV__DETAIL__ARM_ENABLE__TRAITS_HPP_
