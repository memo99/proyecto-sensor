from setuptools import setup
import os
from glob import glob

package_name = 'robot_example_sim'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Federico Ruiz Ugalde',
    maintainer_email='memeruiz@gmail.com',
    description='robot example sim',
    license='GPLv3',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'robot_example_sim = robot_example_sim.robot_example_sim:main',
            'send_ext_torques = robot_example_sim.send_ext_torques:main',
        ],
    },
)
