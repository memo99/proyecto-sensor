import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
import subprocess

robot_example_urdf_dir = get_package_share_directory('robot_example_urdf')

def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default= 'false')
    robot_desc = subprocess.run(
        ["xacro", robot_example_urdf_dir+"/robot_example.xacro"], text=True, capture_output=True)

    return LaunchDescription([
        Node(
            package='robot_example_sim',
            executable='robot_example_sim',
            
            remappings=[
                ("/test/robot_example_sim/joint_states", "/joint_states")
            ],
            name='robot_example_sim',
            namespace='test',
            output='screen',
            parameters=[
                {'arm_instance' : 'example'}
            ],
        ),
        Node(
            package= 'robot_state_publisher',
            executable = 'robot_state_publisher',
            name = 'robot_state_publisher',
            output='screen',
            parameters= [{'use_sim_time': use_sim_time,
                        'robot_description': robot_desc.stdout}],

        ),
    ])
